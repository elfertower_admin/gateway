package com.eat.gateway;

import com.eat.gateway.crawler.browser.Browser;
import com.eat.gateway.dao.*;
import com.eat.gateway.entity.*;
import com.eat.gateway.service.IDaiaAchieveSrevice;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieSession;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GatewayApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(GatewayApplicationTests.class);

    @Autowired(required = true)
    private Browser browser;

    @Autowired(required = true)
    private IDaiaAchieveSrevice service;

    @Resource
    private ClOperationStepDAO stepDAO;

    @Resource
    private ClOperationStepArgsDAO stepArgsDAO;

    @Resource
    private ClExcelTitleDAO clExcelTitleDAO;

    @Resource
    private ClStandardWordDAO standardWordDAO;

    @Resource
    private ClExcelDataBatchDAO excelDataBatchDAO;

    @Resource
    private ClExcelDataDetailDAO excelDataDetailDAO;

    @Resource
    private ClBreakdownBillDAO breakdownBillDAO;

    @Resource
    private ClPowerGenBillDAO clPowerGenBillDAO;

    @Resource
    private KieSession kieSession;

    @Test
    public void insertStarndardWord() {
        String base_dir = "E:\\words";
        File dir = new File(base_dir + File.separator);
        if (dir.isDirectory()) {
            String result[] = dir.list();
            for (int x = 0; x < result.length; x++) {
                logger.debug(result[x]);
                File file = new File(base_dir + File.separator + result[x]);
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    int len = fis.available();
                    logger.debug(String.valueOf(len));
                    byte[] imgBytes = new byte[len];
                    fis.read(imgBytes);
                    ClStandardWord record = new ClStandardWord();
                    String id = UUID.randomUUID().toString().replaceAll("-","");
                    record.setId(id);
                    record.setStandardPicture(imgBytes);
                    record.setStandardWord(result[x].substring(0,1));
                    record.setDelfag("0");
                    standardWordDAO.insertSelective(record);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        long count = standardWordDAO.countByExample(null);
        logger.debug(String.valueOf(count));
    }
    @Test
    public void contextLoads() {
        try {
            DesiredCapabilities cap = browser.setOptions();
            browser.initializeDriver(cap);
            //得到操作步骤
            ClOperationStepExample stepExample = new ClOperationStepExample();
            stepExample.setOrderByClause(" step_no asc ");
            List<ClOperationStep> stepList = stepDAO.selectByExample(stepExample);
            //所有步骤参数
            ClOperationStepArgsExample argsExample = new ClOperationStepArgsExample();
            argsExample.setOrderByClause(" step_id asc, arg_order asc ");
            List<ClOperationStepArgs> argList = stepArgsDAO.selectByExample(argsExample);
            Class browserClazz = browser.getClass();
            for (ClOperationStep step : stepList) {
                logger.info(step.getOperationName());
                argsExample.clear();
                argsExample.createCriteria().andStepIdEqualTo(step.getStepId());
                int argCount = (int)stepArgsDAO.countByExample(argsExample);
                logger.info("参数：{}个",argCount);
                //存放参数类型
                Class[] classes = new Class[argCount];
                //存放参数值
                Object[] argObjects = new Object[argCount];
                int i = 0;
                for (ClOperationStepArgs arg : argList) {
                    if (step.getStepId().equals(arg.getStepId())){
                        classes[i] = Class.forName(arg.getArgType());
                        argObjects[i] = classes[i].getConstructor(String.class).newInstance(arg.getArgVal());
                        i ++;
                    }
                }
                Method m = browserClazz.getDeclaredMethod(step.getOperationName(),classes);
                Object obj = m.invoke(browser,argObjects);

                if(null != obj){
                    logger.debug("返回值类型：{}",obj.getClass().getName());
                    logger.debug("返回值：{}",obj.toString());
                    TimeUnit.SECONDS.sleep(1);
                } else {
                    logger.debug("无返回值");
                }
                ////*[@id="verifyArea"]/p/input
            }
            TimeUnit.SECONDS.sleep(1);

            browser.destroy(true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e){

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        }catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void simpleReadList() throws IOException {
        ClBreakdownBillExample example = new ClBreakdownBillExample();
        //去掉已经闭环的故障工单数据
        example.createCriteria().andAcceptTimeIsNull().andBackTimeIsNull();
        example.or().andBackTimeIsNull();
        List<ClBreakdownBill> billList = breakdownBillDAO.selectFullByExample(example);
    }

    @Test
//    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void testExcel(){
        /*
        FactHandle f = kieSession.insert(row);
        TestCheckResult result = new TestCheckResult();
        kieSession.insert(result);
        int ruleFiredCount = kieSession.fireAllRules(new AgendaFilter() {
                                                         @Override
                                                         public boolean accept(Match match) {
                                                             boolean isAccept = match.getRule().getPackageName().trim().equals("com.rules.breakdown");
                                                             isAccept = isAccept && match.getRule().getName().trim().equals("is_breakdown");
                                                             return isAccept;
                                                         }
                                                     }
        );
        logger.debug("触发了 {} 条规则",ruleFiredCount);
        if (result.isResult()){
            logger.debug("{} 不是故障工单，放弃保存，名称：{}",row.getBillCode(),row.getWarnningName());
            return;
        }
        */
        String fileName = "E:" + File.separator +"停电监控.xls";
        String fileNameKeyword = fileName.substring(fileName.indexOf(File.separator) + 1,fileName.indexOf("."));
        ClExcelTitleExample ex = new ClExcelTitleExample();
        ex.createCriteria().andFileNameEqualTo(fileNameKeyword);
        this.clExcelTitleDAO.deleteByExample(ex);
        File file = new File(fileName);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            HSSFWorkbook wb = new HSSFWorkbook(fis);

            HSSFSheet sheet = wb.getSheetAt(0);
            int first = sheet.getFirstRowNum();

            HSSFRow row = sheet.getRow(first);
            first = row.getFirstCellNum();
            int last = row.getLastCellNum();
            Date createDate = new Date();
            for (int i = first; i < last; i++){
                HSSFCell cell = row.getCell(i);
                int columnIndex = cell.getColumnIndex();
                String cellVal = cell.getStringCellValue();
                cellVal = cellVal.trim();
                String id = UUID.randomUUID().toString().replaceAll("-","");
                ClExcelTitle record = new ClExcelTitle();
                record.setTitledId(id);
                record.setColumnIndex(columnIndex);
                record.setColumnTitle(cellVal);
                record.setFileName(fileNameKeyword);
                record.setCreateDate(createDate);
                record.setUpdateDate(createDate);
                record.setStatus("0");
                this.clExcelTitleDAO.insertSelective(record);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (null != fis){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
