package com.eat.gateway;

import com.eat.gateway.mail.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MailServiceTest {
    @Autowired
    private MailService mailService;

    @Test
    public void sendSimpleMail() {
       mailService.sendSimpleMail("7614911@qq.com", "这是一个测试邮件", "这是一个测试邮件");
    }
}
