package com.eat.gateway.wx.handler;

import com.eat.gateway.dao.JsSysUserDAO;
import com.eat.gateway.entity.JsSysUser;
import com.eat.gateway.entity.JsSysUserExample;
import com.eat.gateway.utils.DateUtils;
import me.chanjar.weixin.common.api.WxConsts.MenuButtonType;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.WxCpXmlMessage;
import me.chanjar.weixin.cp.bean.WxCpXmlOutMessage;
import me.chanjar.weixin.cp.bean.WxCpXmlOutNewsMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class MenuHandler extends AbstractHandler {

  @Resource
  private JsSysUserDAO userDAO;

  @Value("${pub-host}")
  private String host;

  @Override
  public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage, Map<String, Object> context, WxCpService cpService,
                                  WxSessionManager sessionManager) {

    String msg = String.format("type:%s, event:%s, key:%s",
        wxMessage.getMsgType(), wxMessage.getEvent(),
        wxMessage.getEventKey());
    logger.info(msg);
    ///type:event, event:click, key:10101
    if (MenuButtonType.VIEW.equals(wxMessage.getEvent())) {
      return null;
    }
    WxCpXmlOutMessage message = null;

    String fromUser = wxMessage.getFromUserName();
    JsSysUserExample example = new JsSysUserExample();
    example.createCriteria().andLoginCodeEqualTo(fromUser);
    example.setOrderByClause(" create_date desc ");
    List<JsSysUser> userList = userDAO.selectWithOfficeByExample(example);

    if (null == userList || userList.size() == 0){
      logger.debug("未查询到登录名为：{}的用户",fromUser);
      message = WxCpXmlOutMessage.TEXT().content("您暂时没有数据查询权限，请等待铁塔工单监控系统自动同步微信数据后再试。")
              .fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName())
              .build();
      return message;
    }
    JsSysUser user = userList.get(0);

    String userParentOffice = user.getParentCode();
    String officeName = user.getOfficeName();
    String today = DateUtils.getCurrentDateAsString();
    WxCpXmlOutNewsMessage.Item item = new WxCpXmlOutNewsMessage.Item();
    String menuKey = wxMessage.getEventKey();
    //218.26.6.42:9090
    switch (menuKey){
      case "10101":
        //全省故障工单统计
        if (StringUtils.isBlank(userParentOffice) || !userParentOffice.equals("1")){
          message = WxCpXmlOutMessage.TEXT().content("您没有全省数据查询权限，请联系管理员。")
                  .fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName())
                  .build();
          return message;
        }
        item.setTitle("全省故障工单统计");
        item.setDescription("当天全省故障工单统计");
        item.setPicUrl("http://" + this.host +"/breakdown.png");
        item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-province_breakdown.ureport.xml&queryDate="+today);
        break;
      case "10102":
        //地市故障工单统计  mysql-city_breakdown.ureport.xml
        item.setTitle("地市故障工单统计");
        item.setDescription("当天地市故障工单统计");
        item.setPicUrl("http://" + this.host +"/breakdown.png");
        logger.debug("用户所在部门：{}",officeName);
        if(StringUtils.isNotBlank(officeName) && (officeName.contains("地市级") || officeName.contains("网络维护管理部"))) {
          item.setUrl("http://" + this.host + "/ureport/preview?_u=mysql-city_breakdown.ureport.xml&parentCode=" + userParentOffice + "&queryDate=" + today);
        } else {
          message = WxCpXmlOutMessage.TEXT().content("您没有地市级数据查询权限，请联系管理员。")
                  .fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName())
                  .build();
          return message;
        }
        break;
      case "10103":
        //区县工单统计 兼容两种情况
        item.setTitle("区县故障工单统计");
        item.setDescription("当天区县故障工单统计");
        item.setPicUrl("http://" + this.host +"/breakdown.png");
        logger.debug("用户所在部门：{}",officeName);
        if(StringUtils.isNotBlank(officeName) && (officeName.contains("地市级") || officeName.contains("网络维护管理部"))){
          //管理部门
          item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-city_breakdown.ureport.xml&parentCode=" + userParentOffice +"&queryDate="+today);
        } else {
          //生产部门
          item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-counties_breakdown.ureport.xml&officeCode=" + user.getOfficeCode() +"&queryDate="+today);
        }
        break;
      case "10104":
        //故障工单明细
        item.setTitle("故障工单明细");
        item.setDescription("当天故障工单明细");
        item.setPicUrl("http://" + this.host +"/breakdown.png");
        logger.debug("用户所在部门：{}",officeName);
        if(StringUtils.isNotBlank(officeName) && (officeName.contains("地市级") || officeName.contains("网络维护管理部"))) {
          //管理部门
          item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-detail_manager.ureport.xml&parentCode=" + userParentOffice +"&queryDate="+today);
        } else {
          //生产部门
          item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-detail.ureport.xml&officeCode=" + user.getOfficeCode() +"&queryDate="+today);
        }
        break;
      case "20101":
        //全省发电情况
        if (StringUtils.isBlank(userParentOffice) || !userParentOffice.equals("1")){
          message = WxCpXmlOutMessage.TEXT().content("您没有全省数据查询权限，请联系管理员。")
                  .fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName())
                  .build();
          return message;
        }
        item.setTitle("全省发电统计");
        item.setDescription("当天全省发电统计");
        item.setPicUrl("http://" + this.host +"/power.png");
        item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-province_powergen.ureport.xml&queryDate="+today);
        break;
      case "20102":
        //地市发电
        item.setTitle("地市发电统计");
        item.setDescription("当天地市发电统计");
        item.setPicUrl("http://" + this.host +"/power.png");
        logger.debug("用户所在部门：{}",officeName);
        if(StringUtils.isNotBlank(officeName) &&
                (officeName.contains("地市级") || officeName.contains("网络维护管理部") || officeName.contains("发电组"))) {
          item.setUrl("http://" + this.host + "/ureport/preview?_u=mysql-city_powergen.ureport.xml&parentCode=" + userParentOffice + "&queryDate=" + today);
        } else {
          message = WxCpXmlOutMessage.TEXT().content("您没有地市级数据查询权限，请联系管理员。")
                  .fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName())
                  .build();
          return message;
        }
        break;
      case "20103":
        //区县发电
        item.setTitle("区县发电统计");
        item.setDescription("当天区县发电统计");
        item.setPicUrl("http://" + this.host +"/power.png");
        if(StringUtils.isNotBlank(officeName) && (officeName.contains("地市级") || officeName.contains("网络维护管理部"))){
          //管理部门
          item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-city_powergen.ureport.xml&parentCode=" + userParentOffice +"&queryDate="+today);
        } else {
          //生产部门
          item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-counties_powergen.ureport.xml&officeCode=" + user.getOfficeCode() +"&queryDate="+today);
        }
        break;
      case "20104":
        //发电明细
        item.setTitle("发电明细");
        item.setDescription("当天发电明细");
        item.setPicUrl("http://" + this.host +"/power.png");
        logger.debug("用户所在部门：{}",officeName);
        if(StringUtils.isNotBlank(officeName) && (officeName.contains("地市级") || officeName.contains("网络维护管理部"))) {
          //管理部门
          item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-powergen_detail_manager.ureport.xml&parentCode=" + userParentOffice +"&queryDate="+today);
        } else {
          //生产部门
          item.setUrl("http://"+this.host+"/ureport/preview?_u=mysql-powergen_detail.ureport.xml&officeCode=" + user.getOfficeCode() +"&queryDate="+today);
        }
        break;
    }


    message = WxCpXmlOutMessage.NEWS().addArticle(item)
        .fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName())
        .build();
    return message;
  }

}
