package com.eat.gateway.drools.fact;

import lombok.Data;

@Data
public class TestCheckResult {
    private boolean result = false; // true:通过校验；false：未通过校验
}
