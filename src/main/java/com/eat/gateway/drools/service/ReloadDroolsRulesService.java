package com.eat.gateway.drools.service;

import java.io.UnsupportedEncodingException;

public interface ReloadDroolsRulesService {
    public void reload() throws UnsupportedEncodingException;
}
