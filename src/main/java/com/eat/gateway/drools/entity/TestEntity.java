package com.eat.gateway.drools.entity;

import lombok.Data;

@Data
public class TestEntity {
    private String postcode;

    private String street;

    private String state;
}
