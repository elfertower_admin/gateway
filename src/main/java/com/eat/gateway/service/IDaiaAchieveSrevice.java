package com.eat.gateway.service;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.bean.WxCpDepart;
import me.chanjar.weixin.cp.bean.WxCpUser;

import java.io.IOException;
import java.util.List;

@Deprecated
public interface IDaiaAchieveSrevice {

    /**
     * 下载数据文件
     * */
    public abstract void achieveDataFile() throws Exception;

    /**
     * 解析存储数据文件
     * */
    public abstract String parseFile(String fileName) throws IOException;

    /**
     * 生成并生成通知
     * */
    public abstract void executePrepareSend(String batchId) throws Exception;

    /**
     * 发送通知
     * */
    public abstract void sendMsg() throws Exception;

    /**
     * 从微信获得部门信息列表
     * */
    public abstract List<WxCpDepart> getAllDeptments() throws WxErrorException;

    /**
     * 从微信获得部门下人员信息列表
     * */
    public abstract List<WxCpUser> getUsersByDepartment(Long deptId)  throws WxErrorException;
}
