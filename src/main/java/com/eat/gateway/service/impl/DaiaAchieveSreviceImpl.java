package com.eat.gateway.service.impl;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.eat.gateway.crawler.browser.Browser;
import com.eat.gateway.crawler.excel.model.ExcelReadModel;
import com.eat.gateway.dao.*;
import com.eat.gateway.entity.*;
import com.eat.gateway.mail.MailService;
import com.eat.gateway.service.IDaiaAchieveSrevice;
import com.eat.gateway.utils.ApplicationContextProvider;
import com.eat.gateway.utils.DateUtils;
import com.eat.gateway.utils.ThreadPoolUtils;
import com.eat.gateway.wx.config.WxCpConfiguration;
import com.eat.gateway.wx.config.WxCpProperties;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.WxCpDepart;
import me.chanjar.weixin.cp.bean.WxCpMessage;
import me.chanjar.weixin.cp.bean.WxCpUser;
import me.chanjar.weixin.cp.bean.messagebuilder.TextBuilder;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Deprecated
@Service
@EnableConfigurationProperties(WxCpProperties.class)
public class DaiaAchieveSreviceImpl implements IDaiaAchieveSrevice {

    private  static final Logger logger = LoggerFactory.getLogger(DaiaAchieveSreviceImpl.class);

    @Autowired(required = true)
    private Browser browser;

    @Autowired
    private WxCpProperties properties;

    @Resource
    private ClOperationStepDAO stepDAO;

    @Resource
    private ClOperationStepArgsDAO stepArgsDAO;

    @Resource
    private ClExcelDataBatchDAO excelDataBatchDAO;

    @Resource
    private ClExcelDataDetailDAO excelDataDetailDAO;

    @Resource
    private ClBreakdownBillDAO breakdownBillDAO;

    @Resource
    private ClPowerGenBillDAO powerGenBillDAO;

    @Resource
    private ClPowerGenDataDetailDAO powerGenDataDetailDAO;

    @Resource
    private JsMonitorStationTypeDAO stationTypeDAO;

    @Resource
    private JsMonitorStationDAO stationDAO;

    @Resource
    private JsMonitorNoticeDAO noticeDAO;

    @Resource
    private JsSysUserDAO userDAO;

    @Resource
    private JsSysOfficeDAO officeDAO;

    private static List<ClOperationStep> stepList = null;

    private static long recordCount = 0L;

    private static DesiredCapabilities cap = null;

    /**
     * 下载数据文件，返回文件路径
     * */
    @Override
    public void achieveDataFile() throws Exception{
        boolean is_finish = false;
        try {
            if (null == cap) {
                cap = browser.setOptions();
            }
            //初始化浏览器
            browser.initializeDriver(cap);
            //得到操作步骤
            ClOperationStepExample stepExample = new ClOperationStepExample();
            stepExample.setOrderByClause(" step_no asc ");
            stepExample.createCriteria().andStatusEqualTo("0");
            List<ClOperationStep> stepList = stepDAO.selectByExample(stepExample);
            //所有步骤参数
            ClOperationStepArgsExample argsExample = new ClOperationStepArgsExample();
            argsExample.setOrderByClause(" step_id asc, arg_order asc ");
            argsExample.createCriteria().andStatusEqualTo("0");
            List<ClOperationStepArgs> argList = stepArgsDAO.selectByExample(argsExample);

            Class browserClazz = browser.getClass();

            for (ClOperationStep step : stepList) {
                logger.info("操作步骤名称：{}",step.getOperationName());
                logger.info("操作功能：{}",step.getRemarks());
                int argCount = this.argsCount(step.getStepId(),argList);
                logger.info("参数：{}个",argCount);
                //存放参数类型
                Class[] classes = new Class[argCount];
                //存放参数值
                Object[] argObjects = new Object[argCount];
                int i = 0;
                for (ClOperationStepArgs arg : argList) {
                    if (step.getStepId().equals(arg.getStepId())){
                        classes[i] = Class.forName(arg.getArgType());
                        argObjects[i] = classes[i].getConstructor(String.class).newInstance(arg.getArgVal());
                        i ++;
                    }
                }
                Method m = browserClazz.getDeclaredMethod(step.getOperationName(),classes);
                Object obj = m.invoke(browser,argObjects);

                if(null != obj){
                    logger.debug("返回值类型：{}",obj.getClass().getName());

                    TimeUnit.SECONDS.sleep(1);
                } else {
                    logger.debug("无返回值");
                }

            }
            is_finish = true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e);
        } finally {
            browser.destroy(is_finish);
        }
    }

    @Override
    @Transactional(readOnly=false,propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String parseFile(String fileName) throws IOException {
        String batchId = null;
        File file = new File(fileName);
        if (file.exists() && !file.isDirectory()){
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
            List<Object> data = EasyExcelFactory.read(inputStream, new Sheet(1, 1, ExcelReadModel.class));
            inputStream.close();
            logger.debug("获得数据：{}条",data.size());
            ClExcelDataBatch batch= new ClExcelDataBatch();
            batch.setIsExeced("0");
            batchId = UUID.randomUUID().toString().replaceAll("-","");
            batch.setId(batchId);
            batch.setImportTime(DateUtils.getCurrentDateTimeAsString());
            batch.setStatus("0");
            Date creatDate = new Date();
            batch.setCreateDate(creatDate);
            batch.setUpdateDate(creatDate);
            this.excelDataBatchDAO.insertSelective(batch);
            for (Object datum : data) {
                ExcelReadModel row = (ExcelReadModel)datum;
                String acceptTime = row.getAcceptTime();
                if (StringUtils.isNotBlank(acceptTime)){
                    continue;//放弃已经接单的数据
                }
                ClExcelDataDetail detail = new ClExcelDataDetail();
                String id = UUID.randomUUID().toString().replaceAll("-","");
                detail.setId(id);
                detail.setBatchId(batchId);
                try {
                    BeanUtils.copyProperties(detail,row);
                } catch (IllegalAccessException e) {
                    logger.error(e.getMessage());
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    logger.error(e.getMessage());
                    e.printStackTrace();
                }
                detail.setCreateDate(creatDate);
                detail.setUpdateDate(creatDate);
                detail.setStatus("0");
                this.excelDataDetailDAO.insertSelective(detail);
            }
            file.delete();
        }
        return batchId;
    }

    @Override
    @Transactional(readOnly=false,propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void executePrepareSend(String batchId) throws Exception {
        JsMonitorStationExample example = new JsMonitorStationExample();
        example.setOrderByClause(" create_date asc ");
        example.createCriteria().andStatusEqualTo("0");
        //加载站点
        List<JsMonitorStation> stationList = stationDAO.selectByExample(example);

        JsMonitorStationTypeExample typeExample = new JsMonitorStationTypeExample();
        typeExample.createCriteria().andStatusEqualTo("0");
        //加载配置
        List<JsMonitorStationType> typeList = stationTypeDAO.selectByExample(typeExample);
        //用户
        List<JsSysUser> userList = userDAO.selectWithOfficeByExample(null);
        //机构
        JsSysOfficeExample officeExample = new JsSysOfficeExample();
        officeExample.createCriteria().andStatusEqualTo("0");
        officeExample.setOrderByClause(" office_code asc ");
        List<JsSysOffice> officeList = this.officeDAO.selectByExample(officeExample);
        //抓取的数据
        ClExcelDataDetailExample excelDataDetailExample = new ClExcelDataDetailExample();
        excelDataDetailExample.createCriteria().andBatchIdEqualTo(batchId).andStatusEqualTo("0");
        List<ClExcelDataDetail> dataList = excelDataDetailDAO.selectByExample(excelDataDetailExample);
        for (ClExcelDataDetail detail : dataList) {
            String servicingId = detail.getStationServicingId();
            JsMonitorStation station = getStationByServicingId(stationList, servicingId);
            if (null != station) {
                logger.debug("匹配站址运维ID：{}", servicingId);
                String managers = station.getStationManager();
                logger.debug("包站人IDs：{}",managers);
                String[] managerArray = managers.split(",");
                String billCode = detail.getBillCode();
                logger.debug("故障单编码：{}",billCode);

                JsMonitorNoticeExample noticeExample = new JsMonitorNoticeExample();
                noticeExample.createCriteria().andStatusEqualTo("0")
                        .andBillCodeEqualTo(billCode).andSendeeCodeEqualTo(managerArray[0]);
                long noticedTimes = noticeDAO.countByExample(noticeExample);
                int prepareNotice = (int)noticedTimes + 1;
                logger.debug("已经通知次数：{}次",noticedTimes);
                for (String manager : managerArray) {
                    JsMonitorNotice record = new JsMonitorNotice();
                    String noticeId = UUID.randomUUID().toString().replaceAll("-","");
                    record.setNoticeId(noticeId);
                    record.setBillCode(billCode);
                    Date createDate = new Date();
                    record.setCreateDate(createDate);
                    record.setIsSend("0");
                    record.setNoticeTimes(prepareNotice);
                    record.setSendeeCode(manager);
                    JsSysUser user = getUserByCode(userList,manager);
                    if (null == user){
                        logger.error("未匹配到包站人：{}",manager);
                        throw new NullPointerException("包站人：" + manager + "匹配错误");
                    }
                    record.setOfficeCode(user.getOfficeCode());
                    record.setStatus("0");
                    record.setUpdateDate(createDate);
                    //生成通知内容
                    String content = this.getContent(detail,prepareNotice,user);
                    logger.debug(content);
                    record.setContent(content);
                    record.setUpReport(0);//非上报信息
                    String power = detail.getPowerGenerationBill();
                    if (null == power || StringUtils.isBlank(power) || power.trim().equals("否")){
                        record.setIsPower("0");
                    } else {
                        record.setIsPower("1");
                    }
                    noticeDAO.insertSelective(record);
                }
                int noticeTimesConfig = getNoticeTimesConfig(typeList,station);
                if (prepareNotice > noticeTimesConfig){
                    //是否通知上级
                    JsSysUser user = getUserByCode(userList,managerArray[0]);
                    if (null == user){
                        logger.error("未匹配到包站人：{}",managerArray[0]);
                        throw new NullPointerException("包站人：" + managerArray[0] + "匹配错误");
                    }
                    String parentCodes = user.getParentCodes();//得到所有上级单位
                    logger.debug("获得所有上级单位编码：{}",parentCodes);
                    int step = prepareNotice / noticeTimesConfig - 1;
                    if(prepareNotice % noticeTimesConfig != 0){
                        ++step;//通知几层上级
                    }
                    String[] parentCodeArr = parentCodes.split(",");
                    step = Math.min(step , parentCodeArr.length);
                    logger.debug("通知 {} 级上级单位",step);
                    int count = 0;
                    for (int i = parentCodeArr.length - 1; i > 0; i--){
                        if (count >= step){
                            break;
                        }

                        String parentOfficeCode = parentCodeArr[i];
                        logger.debug("通知上级组织机构编码：{}",parentOfficeCode);
                        //寻找该机构的管理部门
                        JsSysOffice manageOffice = getMamageOffice(officeList,parentOfficeCode);
                        String manageOfficeCode = "";
                        if (null == manageOffice){
                            logger.error("未找到组织机构编码为：{}的管理部门，跳过本级上报操作",parentOfficeCode);
                            continue;
                        } else {
                            manageOfficeCode = manageOffice.getOfficeCode();
                        }
                        JsMonitorNotice record = new JsMonitorNotice();
                        String noticeId = UUID.randomUUID().toString().replaceAll("-","");
                        record.setNoticeId(noticeId);
                        record.setBillCode(billCode);
                        Date createDate = new Date();
                        record.setCreateDate(createDate);
                        record.setIsSend("0");
                        record.setNoticeTimes(prepareNotice);
                        record.setOfficeCode(manageOfficeCode);
                        record.setStatus("0");
                        record.setUpdateDate(createDate);
                        //生成通知内容
                        String content = this.getContent(detail,prepareNotice,user);
                        record.setContent(content);
                        record.setUpReport(1);//上报信息
                        String power = detail.getPowerGenerationBill();
                        if (null == power || StringUtils.isBlank(power) || power.trim().equals("否")){
                            record.setIsPower("0");
                        } else {
                            record.setIsPower("1");
                        }
                        noticeDAO.insertSelective(record);
                        //获得该机构下用户
                        /*
                        List<JsSysUser> users = getUsersByOffice(userList,manageOfficeCode);
                        for (JsSysUser jsSysUser : users) {
                            JsMonitorNotice record = new JsMonitorNotice();
                            String noticeId = UUID.randomUUID().toString().replaceAll("-","");
                            record.setNoticeId(noticeId);
                            record.setBillCode(billCode);
                            Date createDate = new Date();
                            record.setCreateDate(createDate);
                            record.setIsSend("0");
                            record.setNoticeTimes(prepareNotice);
                            record.setSendeeCode(jsSysUser.getUserCode());
                         //   record.setLoginCode(jsSysUser.getLoginCode());
                            record.setOfficeCode(manageOfficeCode);
                            record.setStatus("0");
                            record.setUpdateDate(createDate);
                            //生成通知内容
                            String content = this.getContent(detail,prepareNotice,user);
                            record.setContent(content);
                            record.setUpReport(1);//上报信息
                            noticeDAO.insertSelective(record);
                        }
                        */
                        count ++;
                    }
                }
            } else {
                logger.debug("未能匹配站址运维ID：{}", servicingId);
                continue;
            }
        }
        ClExcelDataBatch b = new ClExcelDataBatch();
        b.setId(batchId);
        b.setIsExeced("1");
        this.excelDataBatchDAO.updateByPrimaryKeySelective(b);
    }

    private JsSysOffice getMamageOffice(List<JsSysOffice> officeList, String parentCode){
        JsSysOffice manageOffice = null;
        for (JsSysOffice office : officeList) {
            if (null != office && office.getParentCode().equals(parentCode) &&
                    (office.getOfficeName().indexOf("地市级") != -1 || office.getOfficeName().indexOf("网络维护") != -1)){
                manageOffice = office;
                break;
            }
        }
        return manageOffice;
    }

    private List<JsSysUser> getUsersByOffice(List<JsSysUser> userList , String officeCode){
        List<JsSysUser> users = new ArrayList<JsSysUser>();
        for (JsSysUser jsSysUser : userList) {
            String strOffice = jsSysUser.getOfficeCode();
            if (StringUtils.isNotBlank(strOffice) && strOffice.equals(officeCode)){
                users.add(jsSysUser);
            }
        }
        return users;
    }

    @Scheduled(cron="0 0/1 * * * ? ")//每分钟执行
    public void countData(){
        Date d = new Date();
        ClExcelDataDetailExample example = new ClExcelDataDetailExample();
        example.createCriteria().andStatusEqualTo("0");
        long count = this.excelDataDetailDAO.countByExample(example);
        logger.debug("此时共抓取到数据：{}条",count);

        Calendar c = Calendar.getInstance();
        c.setTime(d);
        int hour = c.get(Calendar.HOUR);
        int min = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        logger.debug("此时时间：{}点{}分{}秒",hour,min,second);
        if (min == 0){
            long diff = 0L;
            if (recordCount != 0L){
                diff = count - recordCount;
            }
            if (diff == 0L && recordCount != 0L ){
                String now = DateUtils.formatDate(d,DateUtils.DATETIME_FORMAT_PATTERN);
                logger.debug("时间：{}，过去1小时没有抓取到数据",now);

				Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        logger.debug("启动多线程发送邮件");
                        MailService mailService = ApplicationContextProvider.getBean(MailService.class);
                        StringBuilder sb = new StringBuilder();
                        sb.append("时间：");
                        sb.append(now);
                        sb.append("，");
                        sb.append("过去1小时没有抓取到数据。");
                        mailService.sendSimpleMail("7614911@qq.com","铁塔工单监控系统提醒",sb.toString());
                    }
                };
                Thread thread = new Thread(runnable);
                ThreadPoolUtils.getThreadPoolExecutor().execute(thread);

            }
            recordCount = count;
        }
    }

//    @Scheduled(cron="0 5 0-8 * * ? ")//0-8点5分执行
    @Transactional(readOnly=false,propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void openFullInfoDownLoad(){
        //打开全量下载
        Date now=new Date();
        ClOperationStepExample example = new ClOperationStepExample();
        example.createCriteria().andStatusEqualTo("1");
        example.setOrderByClause(" create_date asc ");
        stepList = stepDAO.selectByExample(example);
        for (ClOperationStep clOperationStep : stepList) {
            logger.info("打开功能：{}",clOperationStep.getRemarks());
            clOperationStep.setStatus("0");
            clOperationStep.setUpdateDate(now);
            stepDAO.updateByPrimaryKey(clOperationStep);
        }
    }

//    @Scheduled(cron="0 25 0-8 * * ? ")//0-8点25分执行
    @Transactional(readOnly=false,propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void closeFullInfoDownLoad(){
        //关闭全量下载
        if(null != stepList && stepList.size() > 0){
            Date now=new Date();
            for (ClOperationStep clOperationStep : stepList) {
                logger.info("关闭功能：{}",clOperationStep.getRemarks());
                clOperationStep.setUpdateDate(now);
                clOperationStep.setStatus("1");
                this.stepDAO.updateByPrimaryKey(clOperationStep);
            }
            stepList = null;
        }
    }

    @Scheduled(cron="0 30 0 * * ? ")//0点30执行
    @Transactional(readOnly=false,propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void cleanPassedData(){
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(new Date());
        gc.set(Calendar.HOUR_OF_DAY,0);
        gc.set(Calendar.MINUTE,0);
        gc.set(Calendar.SECOND,0);
//        gc.add(Calendar.DATE,-1);

        Date deadLine = gc.getTime();
        ClBreakdownBillExample breakdownBillExample = new ClBreakdownBillExample();
        breakdownBillExample.createCriteria().andCreateDateLessThan(deadLine);
        List<ClBreakdownBill> breakdownBills = breakdownBillDAO.selectByExample(breakdownBillExample);
        for (ClBreakdownBill breakdownBill : breakdownBills) {
            String detailId = breakdownBill.getDetailId();
            this.excelDataDetailDAO.deleteByPrimaryKey(detailId);
            breakdownBillDAO.deleteByPrimaryKey(breakdownBill.getBreakdownId());
        }
        ClPowerGenBillExample powerGenBillExample = new ClPowerGenBillExample();
        powerGenBillExample.createCriteria().andCreateDateLessThan(deadLine);
        List<ClPowerGenBill> powerGenBills = this.powerGenBillDAO.selectByExample(powerGenBillExample);
        for (ClPowerGenBill powerGenBill : powerGenBills) {
            String shadowId = powerGenBill.getShadowId();
            if(StringUtils.isNotBlank(shadowId)) {
                this.excelDataDetailDAO.deleteByPrimaryKey(shadowId);
            }
            this.powerGenDataDetailDAO.deleteByPrimaryKey(powerGenBill.getDetailId());
            this.powerGenBillDAO.deleteByPrimaryKey(powerGenBill.getPowerGenId());
        }
        JsMonitorNoticeExample jsMonitorNoticeExample = new JsMonitorNoticeExample();
        jsMonitorNoticeExample.createCriteria().andCreateDateLessThan(deadLine);
        this.noticeDAO.deleteByExample(jsMonitorNoticeExample);

    }

    @Override
    @Scheduled(cron="5 0/5 * * * ?")
    @Transactional(readOnly=false,propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void sendMsg() throws Exception {
        logger.debug("开始集中发送等待发送的信息，时间是：{}",DateUtils.getCurrentDateTimeAsString());
        JsMonitorNoticeExample example = new JsMonitorNoticeExample();
        example.createCriteria().andIsSendEqualTo("0").andSendTimeIsNull();
        List<JsMonitorNotice> list = this.noticeDAO.selectWithUserByExample(example);
        int size = list.size();
        logger.debug("待发送信息：{}条",size);
        int i = 1;
        if (size < 100){
            i = 5;
        } else if (size >= 100 && size < 500){
            i = 2;
        }
        WxCpService wxCpService = getWxService();
        for (JsMonitorNotice notice : list) {
            WxCpMessage message = null;
            TextBuilder build = WxCpMessage.TEXT();
            build = build.agentId(1000017);
            String sendee = notice.getSendeeCode();
            String loginCode = notice.getLoginCode();
            if (StringUtils.isBlank(loginCode)){
                logger.error("userCode：{}的站点错误",sendee);
            }
            if(StringUtils.isNotBlank(sendee) && StringUtils.isNotBlank(loginCode)){
                build = build.toUser(loginCode);
            } else if (StringUtils.isNotBlank(notice.getOfficeCode())){
                build = build.toParty(notice.getOfficeCode());

            } else {
                noticeDAO.deleteByPrimaryKey(notice.getNoticeId());
                continue;
            }
            message = build.content(notice.getContent()).build();
            try {
                wxCpService.messageSend(message);
            } catch (Exception ex){
                ex.printStackTrace();
                logger.error(ex.getMessage());
            }
            notice.setIsSend("1");
            notice.setSendTime(new Date());
            this.noticeDAO.updateByPrimaryKeySelective(notice);
            logger.debug("发送{}完成",notice.getNoticeId());
            TimeUnit.SECONDS.sleep(i);
        }
    }

    private String getContent(ClExcelDataDetail detail,int times,JsSysUser user){
        StringBuilder sb = new StringBuilder();
        sb.append(detail.getCounties());
        sb.append(" ");
        sb.append(detail.getVillages());
        sb.append(" 第 【");
        sb.append(times);
        sb.append("】 次通知，需要接单！");
        sb.append("是否发电工单：");
        sb.append(detail.getPowerGenerationBill());
        sb.append("。");
        sb.append(detail.getStationName());
        sb.append("，故障单编码：");
        sb.append(detail.getBillCode());
        sb.append("，告警内容：");
        sb.append(detail.getWarnningDescription());
        sb.append("，告警发生时间：");
        sb.append(detail.getWarnningTime());
        sb.append("，区域经理：");
        sb.append(detail.getManager());
        sb.append("，实际维护人员：");
        sb.append(user.getUserName());
        sb.append("，手机：");
        sb.append(user.getMobile());
        return sb.toString();
    }

    private JsSysUser getUserByCode(List<JsSysUser> userList,String userCode){
        JsSysUser user = null;
        for (JsSysUser jsSysUser : userList) {
            String code = jsSysUser.getUserCode();
            if (code.equals(userCode)){
                user = jsSysUser;
                break;
            }
        }
        return user;
    }

    private int getNoticeTimesConfig( List<JsMonitorStationType> typeList,JsMonitorStation station){
        int noticeTimesConfig = 2;//设置通知阀值的默认值
        String stationType = station.getStationType();
        for (JsMonitorStationType type : typeList) {
            String typeConfig = type.getStationType();
            if (typeConfig.equals(stationType)){
                noticeTimesConfig = type.getNoticeTimes();
                break;
            }
        }
        return noticeTimesConfig;
    }

    private JsMonitorStation getStationByServicingId(List<JsMonitorStation> stationList, String servicingId){
        JsMonitorStation station = null;
        for (JsMonitorStation jsMonitorStation : stationList) {
            String stationCode = jsMonitorStation.getStationCode();
            if (stationCode.equals(servicingId)){
                station = jsMonitorStation;
                break;
            }
        }
        return station;
    }
    
    @Override
    public List<WxCpDepart> getAllDeptments() throws WxErrorException {
        WxCpService wxCpService = this.getWxService();
        if (null != wxCpService) {
            //从根目录加载
            List<WxCpDepart> departList = wxCpService.getDepartmentService().list(new Long(0));
            return departList;
        }
        return null;
    }

    @Override
    public List<WxCpUser> getUsersByDepartment(Long deptId)  throws WxErrorException{
        WxCpService wxCpService = this.getWxService();
        if (null != wxCpService) {
            List<WxCpUser> list = wxCpService.getUserService().listByDepartment(deptId, false, 0);
            return list;
        }
        return null;
    }

    private WxCpService getWxService(){
        //logger.debug(properties.getCorpId());
        WxCpService service = null;
        List<WxCpProperties.AppConfig> list = properties.getAppConfigs();
        Integer appAgentId = 0;
        for (WxCpProperties.AppConfig config : list) {
            Integer agentId = config.getAgentId();
         //   logger.debug("应用ID：{}",agentId);
            if (null != agentId && agentId != 1000000){//排除同步通讯录的应用号码
                appAgentId = agentId;
                break;
            }
        }
        if(null != appAgentId && appAgentId != 0) {
            service = WxCpConfiguration.getCpService(appAgentId);
        }
        return service;
    }

    private int argsCount(String stepId, List<ClOperationStepArgs> argList){
        if (null != argList && argList.size() > 0){
            int argCount = 0;
            for (ClOperationStepArgs arg : argList) {
                String setp = arg.getStepId();
                if (setp.equals(stepId)){
                    ++argCount;
                }
            }
            return argCount;
        } else {
            return 0;
        }
    }
}
