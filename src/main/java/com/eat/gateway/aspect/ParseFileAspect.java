package com.eat.gateway.aspect;

import com.eat.gateway.dao.ClExcelTitleDAO;
import com.eat.gateway.entity.ClExcelTitle;
import com.eat.gateway.entity.ClExcelTitleExample;
import com.eat.gateway.mail.MailService;
import com.eat.gateway.utils.ApplicationContextProvider;
import com.eat.gateway.utils.ThreadPoolUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
public class ParseFileAspect {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private List<ClExcelTitle> titleList;

    @Resource
    private ClExcelTitleDAO excelTitleDAO;

    @PostConstruct()
    public void init() {
        logger.debug("文件格式检查切面被初始化...");
        ClExcelTitleExample example = new ClExcelTitleExample();
        example.createCriteria().andStatusEqualTo("0");
        example.setOrderByClause(" file_name DESC, column_index ASC ");
        titleList = this.excelTitleDAO.selectByExample(example);
    }

    @Pointcut("execution(public * com.eat.gateway.crawler.browser.ChromeBrowser.parseFile(..))")
    public void parseFile(){//切点映射，命名不规定
        logger.debug("parseFile aspect");
    }

    @Before("parseFile()")
    public void doBefore(JoinPoint joinPoint){
        logger.info("parseFile 方法执行前...");
        Object[] args = joinPoint.getArgs();
        if (null != args && args.length > 0){
            Object arg = args[0];
            String fullFileName = (String)arg;
            if(StringUtils.isNotBlank(fullFileName)){
                Map<Integer,String> map = getResultMapByFileName(fullFileName);
                File file = new File(fullFileName);
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    HSSFWorkbook wb = new HSSFWorkbook(fis);
                    int sheetCount = wb.getNumberOfSheets();
                    if(sheetCount > 0){
                        HSSFSheet sheet = wb.getSheetAt(0);
                        int titleRowNum = sheet.getFirstRowNum();
                        HSSFRow titleRow = sheet.getRow(titleRowNum);
                        int first = titleRow.getFirstCellNum();
                        int last = titleRow.getLastCellNum();
                        for (int i = first; i < last; i++){
                            HSSFCell cell = titleRow.getCell(i);
                            int columnIndex = cell.getColumnIndex();
                            /*
                            if(columnIndex == 2){
                                continue;//忽略第三列
                            }
                            */
                            String cellValue = cell.getStringCellValue() == null ? "" : cell.getStringCellValue().trim();
                            String title = map.get(columnIndex);
                            // map.size() != (last + 1)
                            if (!cellValue.equals(title) &&  columnIndex < 67){//忽略68列之后的列标题发生改变，原标题：签到时间，现标题：故障修复时间
                                logger.debug("文件：{}，第 {} 列标题发生改变，原标题：{}，现标题：{}",fullFileName,(columnIndex + 1),title,cellValue);
                                Runnable runnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        logger.debug("启动多线程发送邮件");
                                        MailService mailService = ApplicationContextProvider.getBean(MailService.class);
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("文件：");
                                        sb.append(fullFileName);
                                        sb.append(" 第【");
                                        sb.append(columnIndex + 1);
                                        sb.append("】");
                                        sb.append("列标题发生改变，原标题：【");
                                        sb.append(title);
                                        sb.append("】，现标题：【");
                                        sb.append(cellValue);
                                        sb.append("】。");
                                        sb.append("原列数：");
                                        sb.append(map.size());
                                        sb.append("现有列数：");
                                        sb.append(last + 1);
                                        mailService.sendSimpleMail("7614911@qq.com","铁塔工单监控系统提醒",sb.toString());
                                    }
                                };
                                Thread thread = new Thread(runnable);
                                ThreadPoolUtils.getThreadPoolExecutor().execute(thread);
                                break;
                            }
                        }
                    }
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    e.printStackTrace();
                } finally {
                    if (null != fis){
                        try {
                            fis.close();
                        } catch (IOException e) {
                            logger.error(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }



    private Map<Integer,String> getResultMapByFileName(String fileName){
        Map<Integer,String> map = new HashMap<Integer,String>();
        if(null != this.titleList){
            for (ClExcelTitle excelTitle : titleList) {
                String keyword = excelTitle.getFileName();
                if(fileName.contains(keyword)){
                    map.put(excelTitle.getColumnIndex(),excelTitle.getColumnTitle());
                }
            }
        }
        return map;
    }
}
