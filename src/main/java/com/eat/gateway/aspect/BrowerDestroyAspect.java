package com.eat.gateway.aspect;

import com.eat.gateway.schedules.BreakdownBillExecutor;
import com.eat.gateway.schedules.PowerGenerateBillExecutor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BrowerDestroyAspect {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = true)
    private BreakdownBillExecutor breakdownBillExecutor;

    @Autowired(required = true)
    private PowerGenerateBillExecutor powerGenerateBillExecutor;

    @Pointcut("execution(public * com.eat.gateway.crawler.browser.ChromeBrowser.destroy(..))")
    public void destroy(){//切点映射，命名不规定
        logger.debug("destroy aspect");
    }

    @Before("destroy()")
    public void doBefore(JoinPoint joinPoint){
        logger.info("destroy方法执行前...");
    }

    @After("destroy()")
    public void doAfter(JoinPoint joinPoint){
        logger.info("destroy方法执行后...");
        Object[] args = joinPoint.getArgs();
        if (null != args && args.length > 0){
            Object arg = args[0];
            boolean finished = (Boolean)arg;
            if (finished){
                logger.debug("已经成功完成所有抓取步骤");
                try{
                    //故障工单生成通知
                    this.breakdownBillExecutor.dataExecutor();
                } catch(Exception e){
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
                try{
                    //发电工单生成通知
                    this.powerGenerateBillExecutor.dataExecutor();
                }catch(Exception e){
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }
        }

    }


}
