package com.eat.gateway.aspect;

import com.eat.gateway.crawler.excel.model.ExcelReadModel;
import com.eat.gateway.crawler.excel.model.PowerExcelReadModel;
import com.eat.gateway.dao.*;
import com.eat.gateway.entity.*;
import com.eat.gateway.schedules.BreakdownBillExecutor;
import com.eat.gateway.schedules.PowerGenerateBillExecutor;
import com.eat.gateway.utils.DateUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * 切面类
 */
@Aspect
@Component
public class BroswerAspect {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private JsMonitorStationTypeDAO stationTypeDAO;

    @Resource
    private JsMonitorStationDAO stationDAO;

    @Resource
    private ClExcelDataDetailDAO excelDataDetailDAO;

    @Resource
    private ClPowerGenDataDetailDAO powerGenDataDetailDAO;

    @Resource
    private JsSysUserDAO userDAO;

    @Resource
    private JsSysOfficeDAO officeDAO;

    @Resource
    private ClBreakdownBillDAO breakdownBillDAO;

    @Resource
    private ClPowerGenBillDAO powerGenBillDAO;

    /*
    @Resource
    private KieSession kieSession;
*/
    private List<JsMonitorStation> stationList = null;

    private List<JsMonitorStationType> typeList = null;

    private List<JsSysUser> userList = null;

    private List<JsSysOffice> officeList = null;

    private GregorianCalendar gc = null;

    private void loadDatas(){
        JsMonitorStationExample example = new JsMonitorStationExample();
        example.setOrderByClause(" create_date asc ");
        example.createCriteria().andStatusEqualTo("0");
        //加载站点
        this.stationList = stationDAO.selectByExample(example);
        logger.debug("成功加载站点 {} 个",stationList == null?0:stationList.size());

        JsMonitorStationTypeExample typeExample = new JsMonitorStationTypeExample();
        typeExample.createCriteria().andStatusEqualTo("0");
        //加载配置
        this.typeList = stationTypeDAO.selectByExample(typeExample);
        logger.debug("成功加载站点类型 {} 个",typeList == null?0:typeList.size());

        //用户
        this.userList = userDAO.selectWithOfficeByExample(null);
        logger.debug("成功加载用户 {} 个",userList == null?0:userList.size());
        //机构
        JsSysOfficeExample officeExample = new JsSysOfficeExample();
        officeExample.createCriteria().andStatusEqualTo("0");
        officeExample.setOrderByClause(" office_code asc ");
        officeList = this.officeDAO.selectByExample(officeExample);//加载全部可见机构
        logger.debug("成功加载机构 {} 个",officeList == null?0:officeList.size());

        gc = new GregorianCalendar();
        gc.setTime(new Date());
        gc.set(Calendar.HOUR_OF_DAY,0);
        gc.set(Calendar.MINUTE,0);
        gc.set(Calendar.SECOND,0);
//        gc.add(Calendar.DATE,-1);//1天前
        logger.debug("时间设置完成，为：{}",DateUtils.format(gc.getTime(),DateUtils.DATETIME_FORMAT_PATTERN));
    }

    @Pointcut("execution(public * com.eat.gateway.crawler.browser.ChromeBrowser.downloadExcelAndAnalyze(..))")
    public void downloadExcelAndAnalyze(){//切点映射，命名不规定
        logger.debug("aspect");
    }

    @Before("downloadExcelAndAnalyze()")
    public void doBefore(JoinPoint joinPoint){
        logger.info("方法执行前...");
    }

    @After("downloadExcelAndAnalyze()")
    public void doAfter(JoinPoint joinPoint){
        logger.info("方法执行后...");
    }

    @AfterReturning(returning="result", pointcut="downloadExcelAndAnalyze()")
    public void doAfterReturning(Object result) throws Exception{
        loadDatas();
        logger.debug("返回数据类型为：{}",result.getClass().getName());
        if (null != result && result instanceof List){
            List<Object> dataList = (List<Object>)result;
            logger.debug("切面获得下载的数据：{} 条",dataList.size());
            Date createDate = new Date();

            for (Object object : dataList) {
                logger.debug("Row数据类型为：{}",object.getClass().getSimpleName());
                if (object instanceof ExcelReadModel){
                    //故障工单
                    ExcelReadModel row = (ExcelReadModel)object;
                    if (row == null || StringUtils.isBlank(row.getBillCode())){
                        logger.debug("忽略未派单的数据");
                        continue;
                    }
                    logger.debug("获得数据，工单编码：{}",row.getBillCode());
                    try {
                        saveBreakdowns(row, createDate);
                    }catch (Exception e ){
                        logger.error("切面捕获异常类型：{}",e.getClass().getSimpleName());
                        logger.error("切面捕获异常信息：{}",e.getMessage());
                        e.printStackTrace();
                    }
                } else if (object instanceof PowerExcelReadModel){
                    //发电工单
                    PowerExcelReadModel row = (PowerExcelReadModel)object;
                    if (StringUtils.isBlank(row.getBillCode())){
                        logger.debug("忽略未派单的数据");
                        continue;
                    }
                    logger.debug("获得停电工单数据，工单编码：{}",row.getBillCode());
                    savePowerGens(row , createDate);
                } else {
                    logger.error("工单数据类型错误");
                    continue;
                }
            }

        } else {
            logger.error("切面未能获得下载的数据");
        }
    }

    /*
    * 保存停电工单
    * */
    @Transactional(readOnly=false,propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void savePowerGens(PowerExcelReadModel row, Date creatDate) throws Exception{
        JsMonitorStation station = findOurStation(this.stationList,row.getStationServicingId());
        if(null == station){
            logger.debug("{} 可能不是我方代维站点，放弃数据",row.getStationServicingId());
            return;
        }
        //只处理2天以内的数据
        String startTime = row.getStartTime();
        if(StringUtils.isNotBlank(startTime)){
            logger.debug("停电开始时间：{}",startTime);
            Date start = DateUtils.parseDate(startTime);
            if(start.before(gc.getTime())){
                logger.debug("{} 停电工单，停电开始时间超过时限，放弃数据",row.getBillCode());
                return;
            }
        } else {
            logger.debug("{} 停电工单，停电开始时间为空，放弃数据",row.getBillCode());
            return;
        }
        /*
        * 保存抓取明细
        * */
        ClPowerGenDataDetail detail = new ClPowerGenDataDetail();
        try {
            BeanUtils.copyProperties(detail,row);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        String detailId = createID();
        detail.setPowerGenId(detailId);
        detail.setCreateDate(creatDate);
        detail.setUpdateDate(creatDate);
        detail.setStatus("0");
        this.powerGenDataDetailDAO.insertSelective(detail);
        /*
        * 保存停电工单
        * */
        //清除错误保存为故障工单的数据
        ClBreakdownBillExample clBreakdownBillExample = new ClBreakdownBillExample();
        clBreakdownBillExample.createCriteria().andBillCodeEqualTo(row.getBillCode());
        int deleteCount = this.breakdownBillDAO.deleteByExample(clBreakdownBillExample);
        logger.info("工单编码：{}，清除被错误保存为故障工单的数据{}条",row.getBillCode(),deleteCount);

        ClPowerGenBillExample example = new ClPowerGenBillExample();
        example.createCriteria().andStatusEqualTo("0").andBillCodeEqualTo(row.getBillCode());
        long billCount = this.powerGenBillDAO.countByExample(example);
        //找到综合查询里的投影信息
        ClExcelDataDetailExample clExcelDataDetailExample = new ClExcelDataDetailExample();
        clExcelDataDetailExample.createCriteria().andStatusEqualTo("0").andBillCodeEqualTo(row.getBillCode());
        clExcelDataDetailExample.setOrderByClause(" create_date desc ");
        List<ClExcelDataDetail> shadowList = this.excelDataDetailDAO.selectByExample(clExcelDataDetailExample);
        logger.debug("找到综合查询里的投影信息：{}条",shadowList.size());
        ClExcelDataDetail shadowDetail = null;
        if (shadowList != null && shadowList.size() >0){
            shadowDetail = shadowList.get(0);
        }
        if (billCount == 0L){
            logger.debug("第一次抓取到停电工单：{} ，新建停电工单",row.getBillCode());
            ClPowerGenBill powerGenBill = new ClPowerGenBill();
            powerGenBill.setPowerGenId(createID());
            powerGenBill.setShadowId(shadowDetail == null || StringUtils.isBlank(shadowDetail.getId())? null : shadowDetail.getId());
            powerGenBill.setIsTopLevel(shadowDetail == null || StringUtils.isBlank(shadowDetail.getWarnningName()) || !shadowDetail.getWarnningName().contains("一级低压脱离") ? "0":"1");
            powerGenBill.setBillCode(row.getBillCode());
            powerGenBill.setDetailId(detailId);
            powerGenBill.setStationId(station.getStationId());
            String userCode = StringUtils.isBlank(station.getPowerGenerator())?station.getStationManager():station.getPowerGenerator();
            powerGenBill.setUserCode(userCode);
            String cityName = detail.getCity();
            logger.debug("工单所在地市：{}",cityName);
            JsSysOffice city = this.getOfficeByName("1",cityName);
            if(null != city){
                powerGenBill.setParentOfficeCode(city.getOfficeCode());
            } else {
                logger.debug("未找到名称为：{}的机构",cityName);
                return;
            }
            String countiesName = detail.getCounties();
            logger.debug("工单所在区县：{}",countiesName);
            JsSysOffice counties = this.getOfficeByName(city.getOfficeCode(),countiesName);
            if(null != counties){
                powerGenBill.setOfficeCode(counties.getOfficeCode());
            }  else {
                logger.debug("未找到名称为：{}的机构",countiesName);
                return;
            }
            /*
            JsSysUser user = getUserByCode(this.userList,userCode);
            if(null != user){
                powerGenBill.setOfficeCode(user.getOfficeCode());
                powerGenBill.setParentOfficeCode(user.getParentCode());
            } else {
                logger.error("未找到发电人数据：{}",userCode);
                return;
            }
            */
            powerGenBill.setCreateDate(creatDate);
            powerGenBill.setUpdateDate(creatDate);
            powerGenBill.setStatus("0");
            powerGenBill = makePowerGenResults(powerGenBill,detail);
            this.powerGenBillDAO.insertSelective(powerGenBill);
        } else {
            logger.debug("再次抓取到发电工单：{} ，更新发电工单",row.getBillCode());
            List<ClPowerGenBill> list = powerGenBillDAO.selectByExample(example);
            ClPowerGenBill powerGenBill = list.get(0);
            String stationId = station.getStationId();
            if (powerGenBill.getStationId().equals(stationId)){
                String originalDetailId = powerGenBill.getDetailId();
                logger.debug("开始更新停电工单：{}",row.getBillCode());
                powerGenBill.setDetailId(detailId);

                //防止中途更换了包站人
                String userCode = StringUtils.isBlank(station.getPowerGenerator())?station.getStationManager():station.getPowerGenerator();
                powerGenBill.setUserCode(userCode);
                String cityName = detail.getCity();
                logger.debug("工单所在地市：{}",cityName);
                JsSysOffice city = this.getOfficeByName("1",cityName);
                if(null != city){
                    powerGenBill.setParentOfficeCode(city.getOfficeCode());
                } else {
                    logger.debug("未找到名称为：{}的机构",cityName);
                    return;
                }
                String countiesName = detail.getCounties();
                logger.debug("工单所在区县：{}",countiesName);
                JsSysOffice counties = this.getOfficeByName(city.getOfficeCode(),countiesName);
                if(null != counties){
                    powerGenBill.setOfficeCode(counties.getOfficeCode());
                }  else {
                    logger.debug("未找到名称为：{}的机构",countiesName);
                    return;
                }
                /*
                JsSysUser user = getUserByCode(this.userList,userCode);
                if(null != user){
                    powerGenBill.setUserCode(user.getUserCode());
                    powerGenBill.setOfficeCode(user.getOfficeCode());
                    powerGenBill.setParentOfficeCode(user.getParentCode());
                } else {
                    logger.error("未找到包站人数据：{}",station.getStationManager());
                    return;
                }
                */
                //更新接单状态
                powerGenBill = makePowerGenResults(powerGenBill,detail);
                powerGenBill.setUpdateDate(creatDate);
                //更新投影记录
                powerGenBill.setShadowId(shadowDetail == null || StringUtils.isBlank(shadowDetail.getId())? null : shadowDetail.getId());
                powerGenBill.setIsTopLevel(shadowDetail == null || StringUtils.isBlank(shadowDetail.getWarnningName()) || !shadowDetail.getWarnningName().contains("一级低压脱离") ? "0":"1");

                powerGenBillDAO.updateByPrimaryKeySelective(powerGenBill);
                logger.debug("删除过期的抓取明细：{}",originalDetailId);
                powerGenDataDetailDAO.deleteByPrimaryKey(originalDetailId);
            } else {
                logger.error("相同工单编码：{}中，抓取到的站点信息不一致，退出更新",row.getBillCode());
                return;
            }
        }

    }

    private ClPowerGenBill makePowerGenResults(ClPowerGenBill powerGenBill,ClPowerGenDataDetail detail ){
        String generationStart = detail.getGenerationStart();//发电开始时间
        if (StringUtils.isBlank(generationStart)){
            logger.debug("没有发电开始时间");
            powerGenBill.setIsPowerGen("0");
        } else {
            logger.debug("发电开始时间：{}",generationStart);
            powerGenBill.setIsPowerGen("1");
        }
        String breakdownTime = detail.getBreakdownTime();
        if (StringUtils.isBlank(breakdownTime)){
            logger.debug("没有断站时间");
            powerGenBill.setIsOffLine("0");
        } else {
            logger.debug("断站时间：{}",breakdownTime);
            powerGenBill.setIsOffLine("1");
        }

        String mobileLevel = detail.getMobileLevel();
        boolean isMobile = StringUtils.isNotBlank(mobileLevel) && mobileLevel.contains("高级");
        String unicomLevel = detail.getUnicomLevel();
        boolean isUnicom = StringUtils.isNotBlank(unicomLevel) && unicomLevel.contains("高级");
        String telecomLevel = detail.getTelecomLevel();
        boolean isTelecom = StringUtils.isNotBlank(telecomLevel) && telecomLevel.contains("高级");
        if (isMobile || isUnicom || isTelecom){
            powerGenBill.setStationLevel("高级站");
        } else {
            powerGenBill.setStationLevel("标准站");
        }
        return powerGenBill;
    }

    /**
     * 保存故障工单
     */
    @Transactional(readOnly=false,propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveBreakdowns(ExcelReadModel row, Date creatDate) throws Exception{
        JsMonitorStation station = findOurStation(this.stationList,row.getStationServicingId());
        if(null == station){
            logger.debug("{} 可能不是我方代维站点，放弃数据",row.getStationServicingId());
            return;
        }
        String warning = row.getWarnningName();
        if (StringUtils.isBlank(warning)){
            logger.debug("告警名称为空，取告警描述");
            warning = row.getWarnningDescription() == null?"":row.getWarnningDescription();
        }

        //只处理2天以内的数据
        String str_warnningTime = row.getWarnningTime();
        if(StringUtils.isNotBlank(str_warnningTime)){
            logger.debug("告警时间：{}",str_warnningTime);
            Date warnningTime = DateUtils.parseDate(str_warnningTime);
            if(warnningTime.before(gc.getTime())){
                logger.debug("{} 故障工单，告警时间超过时限，放弃数据",row.getBillCode());
                return;
            }
        } else {
            logger.debug("{} 故障工单，告警时间为空，放弃数据",row.getBillCode());
            return;
        }
        /*
         保存抓取明细
        */
        ClExcelDataDetail detail = new ClExcelDataDetail();
        try {
            BeanUtils.copyProperties(detail,row);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        String detailId = createID();
        detail.setId(detailId);
        detail.setCreateDate(creatDate);
        detail.setUpdateDate(creatDate);
        detail.setStatus("0");
        this.excelDataDetailDAO.insertSelective(detail);//保存抓取明细

        boolean isnot_breakdown = warning.contains("停电") || warning.contains("断电");
        if (isnot_breakdown){
            logger.debug("{} 不是故障工单，放弃关联，删除旧数据，仅保存最新的抓取明细，告警名称：{}",row.getBillCode(),row.getWarnningName());
            ClExcelDataDetailExample detailExample = new ClExcelDataDetailExample();
            detailExample.createCriteria().andBillCodeEqualTo(row.getBillCode()).andIdNotEqualTo(detailId);
            this.excelDataDetailDAO.deleteByExample(detailExample);
            return;
        }

        /*
        * 保存故障工单
        * */
        ClBreakdownBillExample breakdownBillExample = new ClBreakdownBillExample();
        breakdownBillExample.createCriteria().andStatusEqualTo("0").andBillCodeEqualTo(row.getBillCode());
        long billCount = breakdownBillDAO.countByExample(breakdownBillExample);
        if(billCount == 0L){
            logger.debug("第一次抓取到故障工单：{} ，新建故障工单",row.getBillCode());
            ClBreakdownBill breakdownBill = new ClBreakdownBill();
            breakdownBill.setDetailId(detailId);
            String breakdownBillId = this.createID();
            breakdownBill.setBreakdownId(breakdownBillId);
            breakdownBill.setBillCode(row.getBillCode());
            breakdownBill.setStationId(station.getStationId());
            breakdownBill.setUserCode(station.getStationManager());
            String cityName = detail.getCity();
            logger.debug("工单所在地市：{}",cityName);
            JsSysOffice city = this.getOfficeByName("1",cityName);
            if(null != city){
                breakdownBill.setParentOfficeCode(city.getOfficeCode());
            } else {
                logger.debug("未找到名称为：{}的机构",cityName);
                return;
            }
            String countiesName = detail.getCounties();
            logger.debug("工单所在区县：{}",countiesName);
            JsSysOffice counties = this.getOfficeByName(city.getOfficeCode(), countiesName);
            if(null != counties){
                breakdownBill.setOfficeCode(counties.getOfficeCode());
            }  else {
                logger.debug("未找到名称为：{}的机构",countiesName);
                return;
            }
            /*
            JsSysUser user = getUserByCode(this.userList,station.getStationManager());
            if(null != user){
                breakdownBill.setOfficeCode(user.getOfficeCode());
                breakdownBill.setParentOfficeCode(user.getParentCode());
            } else {
                logger.error("未找到包站人数据：{}",station.getStationManager());
                return;
            }
            */
            breakdownBill = makeOverTimes(breakdownBill,detail);
            breakdownBill.setCreateDate(creatDate);
            breakdownBill.setUpdateDate(creatDate);
            breakdownBill.setStatus("0");
            breakdownBillDAO.insertSelective(breakdownBill);
        } else {
            logger.debug("再次抓取到故障工单：{} ，更新故障工单",row.getBillCode());
            List<ClBreakdownBill> billList = breakdownBillDAO.selectByExample(breakdownBillExample);
            ClBreakdownBill breakdownBill = billList.get(0);
            String stationId = station.getStationId();
            if (breakdownBill.getStationId().equals(stationId)){
                String originalDetailId = breakdownBill.getDetailId();
                logger.debug("开始更新故障工单：{}",row.getBillCode());
                breakdownBill.setDetailId(detailId);
                //防止中途更换了包站人
                breakdownBill.setUserCode(station.getStationManager());
                String cityName = detail.getCity();
                logger.debug("工单所在地市：{}",cityName);
                JsSysOffice city = this.getOfficeByName("1",cityName);
                if(null != city){
                    breakdownBill.setParentOfficeCode(city.getOfficeCode());
                } else {
                    logger.debug("未找到名称为：{}的机构",cityName);
                    return;
                }
                String countiesName = detail.getCounties();
                logger.debug("工单所在区县：{}",countiesName);
                JsSysOffice counties = this.getOfficeByName(city.getOfficeCode(),countiesName);
                if(null != counties){
                    breakdownBill.setOfficeCode(counties.getOfficeCode());
                }  else {
                    logger.debug("未找到名称为：{}的机构",countiesName);
                    return;
                }
                /*
                JsSysUser user = getUserByCode(this.userList,station.getStationManager());
                if(null != user){
                    breakdownBill.setUserCode(user.getUserCode());
                    breakdownBill.setOfficeCode(user.getOfficeCode());
                    breakdownBill.setParentOfficeCode(user.getParentCode());
                } else {
                    logger.error("未找到包站人数据：{}",station.getStationManager());
                    return;
                }
                */
                //更新接单状态
                breakdownBill = makeOverTimes(breakdownBill,detail);
                breakdownBill.setUpdateDate(creatDate);
                breakdownBillDAO.updateByPrimaryKeySelective(breakdownBill);
                logger.debug("删除过期的抓取明细：{}",originalDetailId);
                excelDataDetailDAO.deleteByPrimaryKey(originalDetailId);
            } else {
                logger.error("相同工单编码：{}中，抓取到的站点信息不一致，退出更新",row.getBillCode());
                return;
            }
        }
    }

    private ClBreakdownBill makeOverTimes(ClBreakdownBill breakdownBill, ClExcelDataDetail detail){
        String overtime = detail.getOvertime();
        logger.debug("接单是否超时：{}", overtime);
        if (StringUtils.isBlank(overtime) || overtime.trim().equals("否")){
            breakdownBill.setAcceptOvertime("0");
        } else {
            breakdownBill.setAcceptOvertime("1");
        }
        String timeout = detail.getTimeout();
        logger.debug("回单是否超时：{}", timeout);
        if (StringUtils.isBlank(timeout) || timeout.trim().equals("否")){
            breakdownBill.setBackOvertime("0");
        } else {
            breakdownBill.setBackOvertime("1");
        }
        return breakdownBill;
    }

    private JsMonitorStation findOurStation(List<JsMonitorStation> stationList, String stationCode){
        JsMonitorStation station = null;
        if(null != stationList && stationList.size() > 0 && StringUtils.isNotBlank(stationCode)) {
            for (JsMonitorStation jsMonitorStation : stationList) {
                String code = jsMonitorStation.getStationCode();
                if (code.equals(stationCode)) {
                    station = jsMonitorStation;
                    break;
                }
            }
        }
        return station;
    }

    private String createID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    private JsSysOffice getMamageOffice(List<JsSysOffice> officeList, String parentCode){
        JsSysOffice manageOffice = null;
        for (JsSysOffice office : officeList) {
            if (null != office && office.getParentCode().equals(parentCode) &&
                    (office.getOfficeName().indexOf("地市级") != -1 || office.getOfficeName().indexOf("网络维护") != -1)){
                manageOffice = office;
                break;
            }
        }
        return manageOffice;
    }

    private List<JsSysUser> getUsersByOffice(List<JsSysUser> userList , String officeCode){
        List<JsSysUser> users = new ArrayList<JsSysUser>();
        for (JsSysUser jsSysUser : userList) {
            String strOffice = jsSysUser.getOfficeCode();
            if (StringUtils.isNotBlank(strOffice) && strOffice.equals(officeCode)){
                users.add(jsSysUser);
            }
        }
        return users;
    }

    private JsSysUser getUserByCode(List<JsSysUser> userList,String userCode){
        JsSysUser user = null;
        for (JsSysUser jsSysUser : userList) {
            String code = jsSysUser.getUserCode();
            if (code.equals(userCode)){
                user = jsSysUser;
                break;
            }
        }
        return user;
    }

    private JsSysOffice getOfficeByName(String parentCode, String officeName){
        JsSysOffice target = null;
        if (officeList != null && officeList.size() > 0){
            for (JsSysOffice office : officeList) {
                String name = office.getOfficeName();
                if (name.contains(officeName) && office.getParentCode().equals(parentCode)){
                    target = office;
                    break;
                }
            }
        }
        return target;
    }
}
