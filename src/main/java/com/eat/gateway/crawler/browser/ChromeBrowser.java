package com.eat.gateway.crawler.browser;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.eat.gateway.dao.ClExcelModeDAO;
import com.eat.gateway.dao.ClStandardWordDAO;
import com.eat.gateway.dao.ClSysConfigDAO;
import com.eat.gateway.entity.ClExcelMode;
import com.eat.gateway.entity.ClStandardWord;
import com.eat.gateway.entity.ClStandardWordExample;
import com.eat.gateway.entity.ClSysConfig;
import com.eat.gateway.utils.ApplicationContextProvider;
import com.eat.gateway.utils.ValidateCodeUtil;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
public class ChromeBrowser implements Browser {
	
	private static final Logger logger = LoggerFactory.getLogger(ChromeBrowser.class);

	private static List<ClStandardWord> starndardWordList = null;

	private ChromeDriverService service = null;
	
	private WebDriver driver = null;

	private ClSysConfig config = null;

	@Resource
	private ClSysConfigDAO configDao;

	@Resource
	private ClStandardWordDAO standardWordDAO;

	@Resource
	private ClExcelModeDAO excelModeDAO;

	@Override
	public WebDriver initializeDriver(DesiredCapabilities cap) throws IOException {

		logger.debug("Browser Home: {}", config.getBrowserHome());
		logger.debug("Browser Driver: {}", config.getBrowserDriver());
		System.setProperty("webdriver.chrome.driver",config.getBrowserHome());
		// 创建一个 ChromeDriver 的接口，用于连接 Chrome（chromedriver.exe 的路径可以任意放置，只要在newFile（）的时候写入你放的路径即可）
        this.service = new ChromeDriverService.Builder().usingDriverExecutable(new File(config.getBrowserDriver())) .usingAnyFreePort().build();
        service.start();
        // 创建一个 Chrome 的浏览器实例
        this.driver = new RemoteWebDriver(service.getUrl(),cap);
        this.driver.manage().window().maximize();//最大化
        return this.driver ;
	}

	@Override
	public byte[] takeScreenshot(WebDriver driver) throws IOException {
		byte[] screenshot = null;
        screenshot = ((TakesScreenshot) driver)
                .getScreenshotAs(OutputType.BYTES);//得到截图
        return screenshot;
	}

	@Override
	public DesiredCapabilities setOptions() throws IOException{
		config = configDao.selectByPrimaryKey("chrome");
		if (null==config){
			String errMsg = "缺少浏览器配置";
			logger.error(errMsg);
			throw new IOException(errMsg);
		}
		String downloadFilepath = this.config.getBrowserDownloadPath();//临时目录
		logger.debug("下载路径：{}",downloadFilepath);
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0); //设置为禁止弹出下载窗口
        chromePrefs.put("download.default_directory", downloadFilepath); //设置为文件下载路径
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
        options.setExperimentalOption("prefs",chromePrefs);
        options.addArguments("--test-type");
        //设置 chrome 的无头模式（有BUG）
//        options.addArguments("headless");
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        return cap;
	}

	@Override
	public Boolean browse(String url) {
		if (null != this.driver){
			String orignTitle = this.driver.getTitle() == null? "":this.driver.getTitle();
			driver.navigate().to(url);

			WebDriverWait wait = new WebDriverWait(driver, 10);
			Boolean result = wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					//标题发生变化
			        return (!driver.getTitle().equals(orignTitle) || driver.getTitle().contains("综合查询")) ;
				}
			});
			 return result;
		}
		return false;
	}

	@Override
	public void fillBlankByXPath(String xPath, String blankValue) throws Throwable{
		if (null != this.driver){
			WebElement element = this.driver.findElement(By.xpath(xPath));
			if (element.isEnabled()){
				element.clear();
				element.sendKeys(blankValue);
			}
		}
	}

	@Override
	public void fillBlank(String blankId, String blankValue) throws Throwable{
		if (null != this.driver){
			WebElement element = this.driver.findElement(By.id(blankId));
			if (element.isEnabled()){
				element.clear();
				element.sendKeys(blankValue);
			}
		}
	}

	@Override
	public void distinguishVerifyCode(String xpath, String fillXPath) throws Throwable {
		//截屏
		byte[] byteArray = takeScreenshot(driver);
		ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
		BufferedImage screenShotImage = ImageIO.read(bis);
//		ImageIO.write(screenShotImage, "PNG", new File("e://screenShotImage.png"));

		//验证码图片
		WebElement verifyImgElement =driver.findElement(By.xpath(xpath));
		Point point = verifyImgElement.getLocation();
		BufferedImage verifyImg = ValidateCodeUtil.cropImage(screenShotImage,
				point.getX(),point.getY(),
				point.getX() + verifyImgElement.getSize().getWidth(),point.getY() + verifyImgElement.getSize().getHeight());
//		ImageIO.write(verifyImg, "PNG", new File("e://verifyImg.png"));

		verifyImg = ValidateCodeUtil.removeBackground(verifyImg);//降噪
//		ImageIO.write(verifyImg, "PNG", new File("e://removeBackground.png"));

		verifyImg = ValidateCodeUtil.cuttingImg(verifyImg);//切边
//		ImageIO.write(verifyImg, "PNG", new File("e://cut.png"));

		verifyImg = ValidateCodeUtil.cropImage(verifyImg,
				2,0,verifyImg.getWidth() ,verifyImg.getHeight());
//		ImageIO.write(verifyImg, "PNG", new File("e://oneImg.png"));

		//等分
		int oneW = verifyImg.getWidth() / 4;
		List<BufferedImage> verifyImgList = new ArrayList<BufferedImage>();
		for (int j = 0; j < 4; j++) {
			BufferedImage oneImg = ValidateCodeUtil.cropImage(verifyImg,
					oneW * j,0,oneW * (j + 1),verifyImg.getHeight());
			verifyImgList.add(oneImg);
//			String id = UUID.randomUUID().toString().replaceAll("-","");
//			ImageIO.write(oneImg, "PNG", new File("e://words//"+id+".png"));
		}
		if (null == starndardWordList){
			ClStandardWordExample clStandardWordExample = new ClStandardWordExample();
			clStandardWordExample.createCriteria().andDelfagEqualTo("0");
			starndardWordList = standardWordDAO.selectByExampleWithBLOBs(clStandardWordExample);
		}
		HashMap<String, BufferedImage> standardMap = new HashMap<String, BufferedImage>();
		for (ClStandardWord standardWord : starndardWordList) {
			ByteArrayInputStream sbis = new ByteArrayInputStream(standardWord.getStandardPicture());
			BufferedImage standardImg = ImageIO.read(sbis);
//				ImageIO.write(standardImg, "PNG", new File("e://test//"+ standardWord.getStandardWord() + "_" + standardWord.getId() + ".png"));
			standardMap.put(standardWord.getStandardWord() + "_" + standardWord.getId(),standardImg);
		}
		//比较
		StringBuffer sb = new StringBuffer();
		for (BufferedImage image : verifyImgList) {
			String result = ValidateCodeUtil.decideImgNumber(image,standardMap);
			sb.append(result);
		}
		logger.debug("识别验证码：{}",sb.toString());
		this.fillBlankByXPath(fillXPath,sb.toString());
		return ;
	}

	@Override
	public void recoVerifyCode(String xpath, String fillId) throws Throwable {
		//截图
		byte[] byteArray = takeScreenshot(driver);
		ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
		BufferedImage screenShotImage = ImageIO.read(bis);

		//验证码图片
		WebElement verifyImgElement =driver.findElement(By.xpath(xpath));
		Point point = verifyImgElement.getLocation();

//		ImageIO.write(screenShotImage, "PNG", new File("e://screenShotImage.png"));

		BufferedImage verifyImg = ValidateCodeUtil.cropImage(screenShotImage,
				point.getX(),point.getY(),
				point.getX() + verifyImgElement.getSize().getWidth(),point.getY() + verifyImgElement.getSize().getHeight());

//		ImageIO.write(verifyImg, "PNG", new File("e://verifyImg.png"));

		verifyImg = ValidateCodeUtil.removeBackground(verifyImg);//降噪

//		ImageIO.write(verifyImg, "PNG", new File("e://removeBackground.png"));

		verifyImg = ValidateCodeUtil.cuttingImg(verifyImg);//切边

//		ImageIO.write(verifyImg, "PNG", new File("e://cut.png"));

		//等分
		int oneW = verifyImg.getWidth() / 4;
		List<BufferedImage> verifyImgList = new ArrayList<BufferedImage>();
		for (int j = 0; j < 4; j++) {
			BufferedImage oneImg = ValidateCodeUtil.cropImage(verifyImg,
					oneW * j,0,oneW * (j + 1),verifyImg.getHeight());
			verifyImgList.add(oneImg);

//			ImageIO.write(oneImg, "PNG", new File("e://"+j+".png"));

		}
		if (null == starndardWordList){
			ClStandardWordExample clStandardWordExample = new ClStandardWordExample();
			clStandardWordExample.createCriteria().andDelfagEqualTo("1");
			starndardWordList = standardWordDAO.selectByExampleWithBLOBs(clStandardWordExample);
		}
		HashMap<String, BufferedImage> standardMap = new HashMap<String, BufferedImage>();
		for (ClStandardWord standardWord : starndardWordList) {
			ByteArrayInputStream sbis = new ByteArrayInputStream(standardWord.getStandardPicture());
			BufferedImage standardImg = ImageIO.read(sbis);
		//	ImageIO.write(standardImg, "PNG", new File("e://test//"+ standardWord.getStandardWord() + "_" + standardWord.getId() + ".png"));
			standardMap.put(standardWord.getStandardWord() + "_" + standardWord.getId(),standardImg);
		}
		//比较
		StringBuffer sb = new StringBuffer();
		for (BufferedImage image : verifyImgList) {
			String result = ValidateCodeUtil.decideImgNumber(image,standardMap);
			sb.append(result);
		}
		logger.debug("识别验证码：{}",sb.toString());
		this.fillBlank(fillId,sb.toString());
		return ;
	}

	@Deprecated
	@Override
	public Boolean clickButton(String btnId) throws Throwable {
		if (null != this.driver){
			WebElement element = this.driver.findElement(By.id(btnId));
			boolean enables = element.isEnabled();
			logger.debug("按钮可用：{}", enables);
			boolean displayed = element.isDisplayed();
			logger.debug("按钮可见：{}", displayed);
			if (displayed && enables) {
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
				element.click();
			}
			return (displayed && enables);
		}
		return false;
	}

	@Override
	public Boolean clickLoginButtonXPath(String xPath) throws Throwable {
		try {
			if (null != this.driver) {
				WebElement element = this.driver.findElement(By.xpath(xPath));
				TimeUnit.SECONDS.sleep(1);
				boolean enables = element.isEnabled();
				logger.debug("XPath按钮可用：{}", enables);
				boolean displayed = element.isDisplayed();
				logger.debug("XPath按钮可见：{}", displayed);
				/*
				WebDriverWait wait = new WebDriverWait(driver, 3);
				Boolean result = wait.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver driver) {
						return displayed && enables;
					}
				});
				*/
				if (displayed && enables) {
					logger.debug("允许点击：{}",xPath);
					element.click();
				} else {
					logger.debug("不允许点击：{}",xPath);
				}
				return displayed && enables;
			}
		}catch (NoSuchElementException e){
			throw new NoSuchElementException(e.getMessage());
		} catch (Exception e){
			logger.error("点击登录按钮捕获异常类型：{}",e.getClass().getSimpleName());
			logger.error("点击登录按钮捕获异常信息：{}",e.getMessage());
			e.printStackTrace();
			TimeUnit.SECONDS.sleep(1);
			throw new Exception(e);
		}
		return false;
	}

	@Override
	public Boolean clickButtonXPath(String xpath) throws Throwable {
		int count = 0;
		while(true) {
			try {
				if (null != this.driver) {
					WebElement element = this.driver.findElement(By.xpath(xpath));
					boolean enables = element.isEnabled();
					logger.debug("XPath按钮可用：{}", enables);
					boolean displayed = element.isDisplayed();
					logger.debug("XPath按钮可见：{}", displayed);
					if (displayed && enables) {
						logger.debug("允许点击：{}",xpath);
					//	TimeUnit.MINUTES.sleep(1);//休眠，提高成功率
						element.click();
					} else {
						logger.debug("不允许点击：{}",xpath);
					}
					return (displayed && enables);
				}
			}catch (NoSuchElementException e){
				throw new NoSuchElementException(e.getMessage());
			} catch (Exception e){
				count ++;
				logger.error("Browser捕获异常类型：{}",e.getClass().getSimpleName());
				logger.error("Browser捕获异常信息：{}",e.getMessage());
				e.printStackTrace();
				TimeUnit.SECONDS.sleep(1);
				logger.debug("第{}次尝试点击按钮：{}",count + 1,xpath);
				if (count > 180){
					throw e;
				}
			}
		}
	}

	@Deprecated
	@Override
	public String clickToDownload(String btnId) throws Throwable {
		this.clickButton(btnId);//&#20840;&#37096;
		WebDriverWait downloadWait = new WebDriverWait(driver, 300);
		String result = downloadWait.until(new ExpectedCondition<String>() {
			@Override
			public String apply(WebDriver driver) {

				String downloadPath = config.getBrowserDownloadPath();
				File downloadPathDir = new File(downloadPath);
				StringBuilder sb = new StringBuilder();
				int count = 0;
				here:
				while (true) {
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
					if (downloadPathDir.exists()){
						String[] fileNames = downloadPathDir.list();
						if (fileNames == null || fileNames.length == 0){
							logger.debug("文件未开始下载");
							count ++;
							if (count < 60){//等待一分钟
								continue;
							} else {
								break;
							}

						} else {
							for (String fileName : fileNames) {
								logger.info("文件名：{}",fileName);
								if(fileName.indexOf("crdownload") != -1){
									logger.debug("文件未下载完");
									continue here;
								}
							}
							sb.append(downloadPath);
							sb.append(File.separator);
							sb.append(fileNames[0]);
						}
					} else {
						logger.debug("下载目录未创建");
						continue;
					}
					break;
				}
				logger.debug("下载文件名为：{}",sb.toString());
				return sb.toString();
			}
		});
		return result;
	}

	@Override
	public List<Object> downloadExcelAndAnalyze(String xpath) throws Throwable {

	//	this.clickButton(btnId);
		this.clickButtonXPath(xpath);
		WebDriverWait downloadWait = new WebDriverWait(driver, 480);
		String result = downloadWait.until(new ExpectedCondition<String>() {
			@Override
			public String apply(WebDriver driver) {

				String downloadPath = config.getBrowserDownloadPath();
				logger.info("下载目录设置是：{}",downloadPath);
				File downloadPathDir = new File(downloadPath);
				StringBuilder sb = new StringBuilder();
				int count = 0;
				here:
				while (true) {
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
					if (downloadPathDir.exists()){
						String[] fileNames = downloadPathDir.list();
						if (fileNames == null || fileNames.length == 0){
							logger.debug("文件未开始下载");
							count ++;
							if (count < 300){//等待5分钟
								continue;
							} else {
								break;
							}
						} else {
							for (String fileName : fileNames) {
								logger.info("文件名：{}",fileName);
								if(fileName.indexOf("download") != -1){
									logger.debug("文件未下载完");
									continue here;
								}
							}
							sb.append(downloadPath);
							sb.append(File.separator);
							sb.append(fileNames[0]);
						}
					} else {
						logger.debug("下载目录未创建");
						continue;
					}
					break;
				}
				logger.debug("下载文件名为：{}",sb.toString());
				return sb.toString();
			}
		});

		/*
		* 使切面生效，需要调用Spring生成的代理对象 temp
		* */
		ChromeBrowser temp = ApplicationContextProvider.getBean(this.getClass());
		List<Object> dataList = temp.parseFile(result);

		File file = new File(result);
		file.delete();//把文件删除
		return dataList;
	}

	@Override
	public List<Object> parseFile(String result) throws Exception {
		List<Object> dataList = null;
		if (StringUtils.isNotBlank(result)){
			logger.debug("解析文件：{}",result);
			List<ClExcelMode> modeList = excelModeDAO.selectByExample(null);
			if(null == modeList || modeList.size() == 0) {
				String msg = "未找到数据解析模型配置";
				logger.error(msg);
				throw new Exception(msg);
			}
			String modeClassName = null;
			for (ClExcelMode mode : modeList) {
				String modeName = mode.getModeName();
				if (result.contains(modeName)){
					modeClassName = mode.getModeClass();
					break;
				}
			}
			if (StringUtils.isNotBlank(modeClassName)){
				logger.debug("匹配文件：{} 的解析模型：{}",result,modeClassName);
				Class modeClass = Class.forName(modeClassName);
				File file = new File(result);
				if (file.exists() && !file.isDirectory()) {
					InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
					dataList = EasyExcelFactory.read(inputStream, new Sheet(1, 1, modeClass));
					inputStream.close();
					logger.debug("获得数据：{}条", dataList.size());
				//	file.delete();//把文件删除
				} else {
					String msg = "文件：" + result + " 不存在";
					logger.error(msg);
					throw new FileNotFoundException(msg);
				}
			} else {
				String msg = "未能匹配文件：" + result + " 的解析模型，请检查配置";
				logger.error(msg);
				throw new Exception(msg);
			}
		} else {
			String msg = "文件未下载成功，退出后等待下次调用";
			logger.error(msg);
			throw new Exception(msg);
		}
		return dataList;
	}

	@Override
	public void destroy(boolean finished) {
		if (null != this.driver) {
			this.driver.quit();//关闭浏览器
			this.driver = null;
		}
		if (null != this.service && this.service.isRunning()){
			this.service.stop();// 关闭 ChromeDriver 接口
			this.service = null;
		}
	}

	@Override
	public Object javascriptExecute(String js) throws Exception {
		Object object = null;
		if(StringUtils.isNotBlank(js) && null != this.driver){
			logger.debug("Javascript脚本：{}",js);
			try {
				JavascriptExecutor executor = (JavascriptExecutor) this.driver;
				object = executor.executeScript(js);
			} catch (Exception e){
				logger.error("执行JS脚本捕获异常类型：{}",e.getClass().getSimpleName());
				logger.error("执行JS脚本捕获异常信息：{}",e.getMessage());
				throw new Exception(e);
			}

		}
		return object;
	}
}
