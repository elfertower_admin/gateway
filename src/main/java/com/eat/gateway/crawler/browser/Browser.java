package com.eat.gateway.crawler.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.util.List;

public interface Browser {
	
	public abstract WebDriver initializeDriver(DesiredCapabilities cap) throws IOException ;
	
	public abstract  byte[] takeScreenshot(WebDriver driver) throws IOException; 
	
	public abstract DesiredCapabilities setOptions() throws IOException;
	
	public abstract Boolean browse(String url);

	public abstract void fillBlank(String blankId,String blankValue) throws Throwable;

	public abstract void fillBlankByXPath(String xPath,String blankValue) throws Throwable;

	public abstract void recoVerifyCode(String xpath,String fillId)  throws Throwable;

	public abstract void distinguishVerifyCode(String xpath,String fillXPath)  throws Throwable;

	@Deprecated
	public abstract Boolean clickButton(String btnId)  throws Throwable;

	public abstract Boolean clickLoginButtonXPath(String xPath) throws Throwable;

	public abstract Boolean clickButtonXPath(String xpath) throws Throwable;

	@Deprecated
	public abstract String clickToDownload(String btnId)  throws Throwable;

	public abstract List<Object> downloadExcelAndAnalyze(String xpath)  throws Throwable;

	public abstract List<Object> parseFile(String result) throws Exception;

	public abstract Object javascriptExecute(String js) throws Exception;

	public abstract void destroy(boolean finished);
}
