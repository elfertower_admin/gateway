package com.eat.gateway.crawler.excel.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

public class PowerExcelReadModel extends BaseRowModel {

    @ExcelProperty(index = 0)
    private String billCode;//工单编码

    @ExcelProperty(index = 1)
    private String billState;//工单状态

    @ExcelProperty(index = 2)
    private String billType;//工单类型

    @ExcelProperty(index = 3)
    private String province;//所属省份

    @ExcelProperty(index = 4)
    private String city;//所属地市

    @ExcelProperty(index = 5)
    private String counties; //区县
    /*2020-03-15修改*/
    @ExcelProperty(index = 20)
    private String resourceCode;//资源编码

    @ExcelProperty(index = 11)
    private String stationName;//站址名称

    @ExcelProperty(index = 7)
    private String stationServicingId;//运维ID

    @ExcelProperty(index = 9)
    private String voltage;//电池电压(V)

    @ExcelProperty(index = 10)
    private String electricCurrent;//负载电流(A)

    @ExcelProperty(index = 12)
    private String startTime;//停电开始时间

    @ExcelProperty(index = 13)
    private String endTime;//停电结束时间

    @ExcelProperty(index = 14)
    private String remainMin;//蓄电池剩余续航时长(分钟)

    @ExcelProperty(index = 15)
    private String generationStart;//发电开始时间

    @ExcelProperty(index = 16)
    private String generationEnd;//发电结束时间

    @ExcelProperty(index = 17)
    private String generationMin;//发电时长(分钟)

    @ExcelProperty(index = 18)
    private String breakdownTime;//断站时间

    @ExcelProperty(index = 19)
    private String breakdownLong;//断站时长(分钟)

    @ExcelProperty(index = 29)
    private String dispatchTime;//派单时间

    @ExcelProperty(index = 30)
    private String billCost;//工单历时(分钟)

    @ExcelProperty(index = 31)
    private String operatorsName;//运营商名称

    @ExcelProperty(index = 21)
    private String mobileLevel;//移动维护服务等级

    @ExcelProperty(index = 22)
    private String unicomLevel;//联通维护服务等级

    @ExcelProperty(index = 23)
    private String telecomLevel;//电信维护服务等级

    @ExcelProperty(index = 24)
    private String buyPowerGen;//是否购买发电服务

    @ExcelProperty(index = 32)
    private String operatorsAccept;//运营商是否同意发电

    @ExcelProperty(index = 33)
    private String servicingCom;//代维公司

    @ExcelProperty(index = 34)
    private String isGetVal;//是否取信

    @ExcelProperty(index = 35)
    private String requirement;//派单条件

    @ExcelProperty(index = 36)
    private String dispatchLong;//派单判断历时（分钟）

    @ExcelProperty(index = 37)
    private String cormLong;//运营商确认历时（分钟）

    @ExcelProperty(index = 38)
    private String getin;//上站时长（分钟）

    @ExcelProperty(index = 39)
    private String operator;//发电人

    public String getUnicomLevel() {
        return unicomLevel;
    }

    public void setUnicomLevel(String unicomLevel) {
        this.unicomLevel = unicomLevel;
    }

    public String getTelecomLevel() {
        return telecomLevel;
    }

    public void setTelecomLevel(String telecomLevel) {
        this.telecomLevel = telecomLevel;
    }

    public String getBuyPowerGen() {
        return buyPowerGen;
    }

    public void setBuyPowerGen(String buyPowerGen) {
        this.buyPowerGen = buyPowerGen;
    }

    public String getOperatorsAccept() {
        return operatorsAccept;
    }

    public void setOperatorsAccept(String operatorsAccept) {
        this.operatorsAccept = operatorsAccept;
    }

    public String getServicingCom() {
        return servicingCom;
    }

    public void setServicingCom(String servicingCom) {
        this.servicingCom = servicingCom;
    }

    public String getIsGetVal() {
        return isGetVal;
    }

    public void setIsGetVal(String isGetVal) {
        this.isGetVal = isGetVal;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getDispatchLong() {
        return dispatchLong;
    }

    public void setDispatchLong(String dispatchLong) {
        this.dispatchLong = dispatchLong;
    }

    public String getCormLong() {
        return cormLong;
    }

    public void setCormLong(String cormLong) {
        this.cormLong = cormLong;
    }

    public String getGetin() {
        return getin;
    }

    public void setGetin(String getin) {
        this.getin = getin;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getElectricCurrent() {
        return electricCurrent;
    }

    public void setElectricCurrent(String electricCurrent) {
        this.electricCurrent = electricCurrent;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRemainMin() {
        return remainMin;
    }

    public void setRemainMin(String remainMin) {
        this.remainMin = remainMin;
    }

    public String getGenerationStart() {
        return generationStart;
    }

    public void setGenerationStart(String generationStart) {
        this.generationStart = generationStart;
    }

    public String getGenerationEnd() {
        return generationEnd;
    }

    public void setGenerationEnd(String generationEnd) {
        this.generationEnd = generationEnd;
    }

    public String getGenerationMin() {
        return generationMin;
    }

    public void setGenerationMin(String generationMin) {
        this.generationMin = generationMin;
    }

    public String getBreakdownTime() {
        return breakdownTime;
    }

    public void setBreakdownTime(String breakdownTime) {
        this.breakdownTime = breakdownTime;
    }

    public String getBreakdownLong() {
        return breakdownLong;
    }

    public void setBreakdownLong(String breakdownLong) {
        this.breakdownLong = breakdownLong;
    }

    public String getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(String dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public String getBillCost() {
        return billCost;
    }

    public void setBillCost(String billCost) {
        this.billCost = billCost;
    }

    public String getOperatorsName() {
        return operatorsName;
    }

    public void setOperatorsName(String operatorsName) {
        this.operatorsName = operatorsName;
    }

    public String getMobileLevel() {
        return mobileLevel;
    }

    public void setMobileLevel(String mobileLevel) {
        this.mobileLevel = mobileLevel;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getBillState() {
        return billState;
    }

    public void setBillState(String billState) {
        this.billState = billState;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounties() {
        return counties;
    }

    public void setCounties(String counties) {
        this.counties = counties;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationServicingId() {
        return stationServicingId;
    }

    public void setStationServicingId(String stationServicingId) {
        this.stationServicingId = stationServicingId;
    }
}
