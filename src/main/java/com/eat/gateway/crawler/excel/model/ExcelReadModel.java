package com.eat.gateway.crawler.excel.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

public class  ExcelReadModel extends BaseRowModel {

    @ExcelProperty(index = 0)
    private String billCode;//故障单编码

    @ExcelProperty(index = 1)
    private String billState;//工单状态

    @ExcelProperty(index = 3)
    private String takeupTime;//受理时间

    @ExcelProperty(index = 4)
    private String dispatchTime;//派单时间

    @ExcelProperty(index = 5)
    private String acceptTime;//接单时间

    @ExcelProperty(index = 6)
    private String warnningTime;//告警时间

    @ExcelProperty(index = 7)
    private String timeLimit;//时限

    @ExcelProperty(index = 8)
    private String backTime;//回单时间

    @ExcelProperty(index = 9)
    private String pigeonholeTime;//归档时间

    @ExcelProperty(index = 10)
    private String breakdownOriginate;//故障来源

    @ExcelProperty(index = 11)
    private String warnningName;//告警名称

    @ExcelProperty(index = 12)
    private String warnningState;//告警状态

    @ExcelProperty(index = 13)
    private String warnningDescription;//告警描述

    @ExcelProperty(index = 14)
    private String warnningLevel;//告警等级

    @ExcelProperty(index = 15)
    private String breakdownTitle;//故障标题

    @ExcelProperty(index = 16)
    private String breakdownDescription;//故障描述

    @ExcelProperty(index = 17)
    private String stationServicingId;//站点维护ID

    @ExcelProperty(index = 18)
    private String stationName;//站址名称

    @ExcelProperty(index = 19)
    private String province;//所属省份

    @ExcelProperty(index = 20)
    private String city;//所属地市

    @ExcelProperty(index = 21)
    private String counties; //区县

    @ExcelProperty(index = 22)
    private String villages; //乡镇

    @ExcelProperty(index = 23)
    private String getinStation;//上站

    @ExcelProperty(index = 24)
    private String breakdownReason;//故障原因

    @ExcelProperty(index = 25)
    private String exonerative;//是否免责

    @ExcelProperty(index = 26)
    private String exonerationClause;//免责条款

    @ExcelProperty(index = 27)
    private String specificReasons;//具体原因

    @ExcelProperty(index = 28)
    private String currentVoltage;//回单时获取的实时电压值

    @ExcelProperty(index = 29)
    private String reductionMin;//减免分钟

    @ExcelProperty(index = 30)
    private String isTowerReason;//申告工单是否铁塔原因

    @ExcelProperty(index = 31)
    private String breakdownClassify;//申告工单故障分类

    @ExcelProperty(index = 32)
    private String contactsMan;//运营商联系人

    @ExcelProperty(index = 33)
    private String contactsWay;//运营商联系方式

    @ExcelProperty(index = 34)
    private String restrain;//抑制工单

    @ExcelProperty(index = 35)
    private String restrainReason;//抑制原因

    @ExcelProperty(index = 36)
    private String warnningClearTime;//告警清除时间

    @ExcelProperty(index = 37)
    private String reply;//回复内容

    @ExcelProperty(index = 38)
    private String deviceType;//故障设备类型

    @ExcelProperty(index = 39)
    private String prompt;//催办

    @ExcelProperty(index = 40)
    private String timeout;//回单是否超时

    @ExcelProperty(index = 41)
    private String backMan;//回单人

    @ExcelProperty(index = 42)
    private String servicingCom;//代维护公司

    @ExcelProperty(index = 43)
    private String servicingWay;//处理过程

    @ExcelProperty(index = 44)
    private String machineRoomType;//机房类型

    @ExcelProperty(index = 45)
    private String powerSupply;//供电方式

    @ExcelProperty(index = 46)
    private String fsu;//FSU上网卡类型

    @ExcelProperty(index = 47)
    private String fsuManufacturer;//FSU厂家

    @ExcelProperty(index = 48)
    private String airManufacturer;//空调厂家

    @ExcelProperty(index = 49)
    private String batteryManufacturer;//电池厂家

    @ExcelProperty(index = 50)
    private String powerManufacturer;//电源厂家

    @ExcelProperty(index = 51)
    private String generation;//是否具备发电条件

    @ExcelProperty(index = 52)
    private String batterysCount;//电池组数

    @ExcelProperty(index = 53)
    private String capacitance;//单组容量

    @ExcelProperty(index = 54)
    private String totalElectric;//直流负载总电流（A）

    @ExcelProperty(index = 55)
    private String duration;//蓄电池基础保障服务备电时长

    @ExcelProperty(index = 56)
    private String warnningDuration;//告警历时（分钟）

    @ExcelProperty(index = 57)
    private String billExecDuration;//工单处理历时（分钟）

    @ExcelProperty(index = 58)
    private String department;//处理人/部门

    @ExcelProperty(index = 59)
    private String backTerminal;//回单操作终端

    @ExcelProperty(index = 60)
    private String acceptBillLong;//接单时长（分钟）

    @ExcelProperty(index = 61)
    private String firstFeedbackTime;//首次反馈时间

    @ExcelProperty(index = 62)
    private String overtime ;//接单是否超时

    @ExcelProperty(index = 63)
    private String belongTo;//所属运营商

    @ExcelProperty(index = 64)
    private String confirmContent;//故障确认内容

    @ExcelProperty(index = 65)
    private String manager;//区域经理

    @ExcelProperty(index = 66)
    private String originate;//数据来源

    @ExcelProperty(index = 67)
    private String acceptTerminal;//接单操作终端

    @ExcelProperty(index = 68)
    private String signinTime;//签到时间

    @ExcelProperty(index = 69)
    private String signinLongitude;//签到经度

    @ExcelProperty(index = 70)
    private String signinLatitude;//签到纬度

    @ExcelProperty(index = 71)
    private String fsuLongitude;//FSU经度

    @ExcelProperty(index = 72)
    private String fsuLatitude;//FSU纬度

    @ExcelProperty(index = 73)
    private String powerGenerationBill;//是否是发电工单
    /*2020-03-15修改*/
    @ExcelProperty(index = 2)
    private String breakdownType;//故障类型

    @ExcelProperty(index = 74)
    private String limitVoltage;//停电工单最低电压

    @ExcelProperty(index = 75)
    private String operators;//真实发电运营商

    @ExcelProperty(index = 76)
    private String operatorsBill;//运营商工单号

    @ExcelProperty(index = 77)
    private String mobileName;//移动站址名称

    @ExcelProperty(index = 78)
    private String mobileCode;//移动站址编码

    @ExcelProperty(index = 79)
    private String unicomName;//联通站址名称

    @ExcelProperty(index = 80)
    private String unicomCode;//联通站址编码

    @ExcelProperty(index = 81)
    private String telecomName;//电信站址名称

    @ExcelProperty(index = 82)
    private String telecomCode;//电信站址编码

    public String getCurrentVoltage() {
        return currentVoltage;
    }

    public void setCurrentVoltage(String currentVoltage) {
        this.currentVoltage = currentVoltage;
    }

    public String getBreakdownDescription() {
        return breakdownDescription;
    }

    public void setBreakdownDescription(String breakdownDescription) {
        this.breakdownDescription = breakdownDescription;
    }

    public String getBreakdownTitle() {
        return breakdownTitle;
    }

    public void setBreakdownTitle(String breakdownTitle) {
        this.breakdownTitle = breakdownTitle;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getBillState() {
        return billState;
    }

    public void setBillState(String billState) {
        this.billState = billState;
    }

    public String getTakeupTime() {
        return takeupTime;
    }

    public void setTakeupTime(String takeupTime) {
        this.takeupTime = takeupTime;
    }

    public String getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(String dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public String getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(String acceptTime) {
        this.acceptTime = acceptTime;
    }

    public String getWarnningTime() {
        return warnningTime;
    }

    public void setWarnningTime(String warnningTime) {
        this.warnningTime = warnningTime;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getBackTime() {
        return backTime;
    }

    public void setBackTime(String backTime) {
        this.backTime = backTime;
    }

    public String getPigeonholeTime() {
        return pigeonholeTime;
    }

    public void setPigeonholeTime(String pigeonholeTime) {
        this.pigeonholeTime = pigeonholeTime;
    }

    public String getBreakdownOriginate() {
        return breakdownOriginate;
    }

    public void setBreakdownOriginate(String breakdownOriginate) {
        this.breakdownOriginate = breakdownOriginate;
    }

    public String getWarnningName() {
        return warnningName;
    }

    public void setWarnningName(String warnningName) {
        this.warnningName = warnningName;
    }

    public String getWarnningState() {
        return warnningState;
    }

    public void setWarnningState(String warnningState) {
        this.warnningState = warnningState;
    }

    public String getWarnningDescription() {
        return warnningDescription;
    }

    public void setWarnningDescription(String warnningDescription) {
        this.warnningDescription = warnningDescription;
    }

    public String getWarnningLevel() {
        return warnningLevel;
    }

    public void setWarnningLevel(String warnningLevel) {
        this.warnningLevel = warnningLevel;
    }

    public String getStationServicingId() {
        return stationServicingId;
    }

    public void setStationServicingId(String stationServicingId) {
        this.stationServicingId = stationServicingId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounties() {
        return counties;
    }

    public void setCounties(String counties) {
        this.counties = counties;
    }

    public String getVillages() {
        return villages;
    }

    public void setVillages(String villages) {
        this.villages = villages;
    }

    public String getGetinStation() {
        return getinStation;
    }

    public void setGetinStation(String getinStation) {
        this.getinStation = getinStation;
    }

    public String getBreakdownReason() {
        return breakdownReason;
    }

    public void setBreakdownReason(String breakdownReason) {
        this.breakdownReason = breakdownReason;
    }

    public String getExonerative() {
        return exonerative;
    }

    public void setExonerative(String exonerative) {
        this.exonerative = exonerative;
    }

    public String getExonerationClause() {
        return exonerationClause;
    }

    public void setExonerationClause(String exonerationClause) {
        this.exonerationClause = exonerationClause;
    }

    public String getSpecificReasons() {
        return specificReasons;
    }

    public void setSpecificReasons(String specificReasons) {
        this.specificReasons = specificReasons;
    }

    public String getReductionMin() {
        return reductionMin;
    }

    public void setReductionMin(String reductionMin) {
        this.reductionMin = reductionMin;
    }

    public String getIsTowerReason() {
        return isTowerReason;
    }

    public void setIsTowerReason(String isTowerReason) {
        this.isTowerReason = isTowerReason;
    }

    public String getBreakdownClassify() {
        return breakdownClassify;
    }

    public void setBreakdownClassify(String breakdownClassify) {
        this.breakdownClassify = breakdownClassify;
    }

    public String getContactsMan() {
        return contactsMan;
    }

    public void setContactsMan(String contactsMan) {
        this.contactsMan = contactsMan;
    }

    public String getContactsWay() {
        return contactsWay;
    }

    public void setContactsWay(String contactsWay) {
        this.contactsWay = contactsWay;
    }

    public String getRestrain() {
        return restrain;
    }

    public void setRestrain(String restrain) {
        this.restrain = restrain;
    }

    public String getRestrainReason() {
        return restrainReason;
    }

    public void setRestrainReason(String restrainReason) {
        this.restrainReason = restrainReason;
    }

    public String getWarnningClearTime() {
        return warnningClearTime;
    }

    public void setWarnningClearTime(String warnningClearTime) {
        this.warnningClearTime = warnningClearTime;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getBackMan() {
        return backMan;
    }

    public void setBackMan(String backMan) {
        this.backMan = backMan;
    }

    public String getServicingCom() {
        return servicingCom;
    }

    public void setServicingCom(String servicingCom) {
        this.servicingCom = servicingCom;
    }

    public String getServicingWay() {
        return servicingWay;
    }

    public void setServicingWay(String servicingWay) {
        this.servicingWay = servicingWay;
    }

    public String getMachineRoomType() {
        return machineRoomType;
    }

    public void setMachineRoomType(String machineRoomType) {
        this.machineRoomType = machineRoomType;
    }

    public String getPowerSupply() {
        return powerSupply;
    }

    public void setPowerSupply(String powerSupply) {
        this.powerSupply = powerSupply;
    }

    public String getFsu() {
        return fsu;
    }

    public void setFsu(String fsu) {
        this.fsu = fsu;
    }

    public String getFsuManufacturer() {
        return fsuManufacturer;
    }

    public void setFsuManufacturer(String fsuManufacturer) {
        this.fsuManufacturer = fsuManufacturer;
    }

    public String getAirManufacturer() {
        return airManufacturer;
    }

    public void setAirManufacturer(String airManufacturer) {
        this.airManufacturer = airManufacturer;
    }

    public String getBatteryManufacturer() {
        return batteryManufacturer;
    }

    public void setBatteryManufacturer(String batteryManufacturer) {
        this.batteryManufacturer = batteryManufacturer;
    }

    public String getPowerManufacturer() {
        return powerManufacturer;
    }

    public void setPowerManufacturer(String powerManufacturer) {
        this.powerManufacturer = powerManufacturer;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public String getBatterysCount() {
        return batterysCount;
    }

    public void setBatterysCount(String batterysCount) {
        this.batterysCount = batterysCount;
    }

    public String getCapacitance() {
        return capacitance;
    }

    public void setCapacitance(String capacitance) {
        this.capacitance = capacitance;
    }

    public String getTotalElectric() {
        return totalElectric;
    }

    public void setTotalElectric(String totalElectric) {
        this.totalElectric = totalElectric;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getWarnningDuration() {
        return warnningDuration;
    }

    public void setWarnningDuration(String warnningDuration) {
        this.warnningDuration = warnningDuration;
    }

    public String getBillExecDuration() {
        return billExecDuration;
    }

    public void setBillExecDuration(String billExecDuration) {
        this.billExecDuration = billExecDuration;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBackTerminal() {
        return backTerminal;
    }

    public void setBackTerminal(String backTerminal) {
        this.backTerminal = backTerminal;
    }

    public String getAcceptBillLong() {
        return acceptBillLong;
    }

    public void setAcceptBillLong(String acceptBillLong) {
        this.acceptBillLong = acceptBillLong;
    }

    public String getFirstFeedbackTime() {
        return firstFeedbackTime;
    }

    public void setFirstFeedbackTime(String firstFeedbackTime) {
        this.firstFeedbackTime = firstFeedbackTime;
    }

    public String getOvertime() {
        return overtime;
    }

    public void setOvertime(String overtime) {
        this.overtime = overtime;
    }

    public String getBelongTo() {
        return belongTo;
    }

    public void setBelongTo(String belongTo) {
        this.belongTo = belongTo;
    }

    public String getConfirmContent() {
        return confirmContent;
    }

    public void setConfirmContent(String confirmContent) {
        this.confirmContent = confirmContent;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getOriginate() {
        return originate;
    }

    public void setOriginate(String originate) {
        this.originate = originate;
    }

    public String getAcceptTerminal() {
        return acceptTerminal;
    }

    public void setAcceptTerminal(String acceptTerminal) {
        this.acceptTerminal = acceptTerminal;
    }

    public String getSigninTime() {
        return signinTime;
    }

    public void setSigninTime(String signinTime) {
        this.signinTime = signinTime;
    }

    public String getSigninLongitude() {
        return signinLongitude;
    }

    public void setSigninLongitude(String signinLongitude) {
        this.signinLongitude = signinLongitude;
    }

    public String getSigninLatitude() {
        return signinLatitude;
    }

    public void setSigninLatitude(String signinLatitude) {
        this.signinLatitude = signinLatitude;
    }

    public String getFsuLongitude() {
        return fsuLongitude;
    }

    public void setFsuLongitude(String fsuLongitude) {
        this.fsuLongitude = fsuLongitude;
    }

    public String getFsuLatitude() {
        return fsuLatitude;
    }

    public void setFsuLatitude(String fsuLatitude) {
        this.fsuLatitude = fsuLatitude;
    }

    public String getPowerGenerationBill() {
        return powerGenerationBill;
    }

    public void setPowerGenerationBill(String powerGenerationBill) {
        this.powerGenerationBill = powerGenerationBill;
    }

    public String getBreakdownType() {
        return breakdownType;
    }

    public void setBreakdownType(String breakdownType) {
        this.breakdownType = breakdownType;
    }

    public String getLimitVoltage() {
        return limitVoltage;
    }

    public void setLimitVoltage(String limitVoltage) {
        this.limitVoltage = limitVoltage;
    }

    public String getOperators() {
        return operators;
    }

    public void setOperators(String operators) {
        this.operators = operators;
    }

    public String getOperatorsBill() {
        return operatorsBill;
    }

    public void setOperatorsBill(String operatorsBill) {
        this.operatorsBill = operatorsBill;
    }

    public String getMobileName() {
        return mobileName;
    }

    public void setMobileName(String mobileName) {
        this.mobileName = mobileName;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public String getUnicomName() {
        return unicomName;
    }

    public void setUnicomName(String unicomName) {
        this.unicomName = unicomName;
    }

    public String getUnicomCode() {
        return unicomCode;
    }

    public void setUnicomCode(String unicomCode) {
        this.unicomCode = unicomCode;
    }

    public String getTelecomName() {
        return telecomName;
    }

    public void setTelecomName(String telecomName) {
        this.telecomName = telecomName;
    }

    public String getTelecomCode() {
        return telecomCode;
    }

    public void setTelecomCode(String telecomCode) {
        this.telecomCode = telecomCode;
    }
}
