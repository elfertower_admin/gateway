package com.eat.gateway.utils;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NameableThreadPoolExecutor extends ThreadPoolExecutor {
	
	private static final Logger logger = LoggerFactory.getLogger(NameableThreadPoolExecutor.class);
	// 保存线程的名字  
    private ConcurrentHashMap<String, Thread> threads;  
  
    public NameableThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ConcurrentHashMap<String, Thread> threads) {  
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);  
        this.threads = threads;  
    }  
  
    @Override  
    protected void afterExecute(Runnable r, Throwable t) {  
        super.afterExecute(r, t);
        if (null != r && r instanceof Thread && !((Thread)r).isAlive()) {
        	//将运行完的线程移除map，避免内存泄漏
        	 Thread thread = (Thread)r;
             this.remove(thread.getName());
             logger.debug("threads size is " + String.valueOf(this.threads.size()));
        }
       
    }  
  
    @Override  
    protected void beforeExecute(Thread t, Runnable r) {  
        super.beforeExecute(t, r);
    }  
  
    @Override  
    protected void terminated() {  
        super.terminated();    //To change body of overridden methods use File | Settings | File Templates.  
    }  
  
    @Override  
    public void execute(Runnable command) {  
        super.execute(command);  
        if (command instanceof Thread) {  
            Thread t = (Thread)command;  
            String key = t.getName();  
            this.threads.put(key, t);  
        }  
    }  
  
    public ConcurrentHashMap<String, Thread> getThreads() {  
        return this.threads;  
    }  
  
    @Override  
    public boolean remove(Runnable task) {  
        if (task instanceof Thread) {  
            Thread t = (Thread)task;  
            String key = t.getName();  
            this.threads.remove(key);  
        }  
        return super.remove(task);  
    }  
  
    public boolean remove(String name) {  
        Thread t = this.threads.get(name);  
        return this.remove(t);  
    }  
  
    public boolean contains(String name) {  
        return this.threads.containsKey(name);  
    }  
  
}
