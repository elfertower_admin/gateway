package com.eat.gateway.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

public class ThreadPoolUtils {

    private static final Logger logger = LoggerFactory.getLogger(ThreadPoolUtils.class);

    private static NameableThreadPoolExecutor threadPoolExecutor = null;

    public static NameableThreadPoolExecutor getThreadPoolExecutor(){
        if (null == threadPoolExecutor){
            threadPoolExecutor = new NameableThreadPoolExecutor(1, 2, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), new ConcurrentHashMap<String, Thread>());
        }
        return threadPoolExecutor;
    }

}
