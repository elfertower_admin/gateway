package com.eat.gateway.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class ValidateCodeUtil {

    private static final Logger logger = LoggerFactory.getLogger(ValidateCodeUtil.class);

    public static void removeBackground(String imgUrl, String resUrl) {
        //定义一个临界阈值上限
        int thresholdUp = 500;
        int thresholdDown = 50;
        try {
            BufferedImage img = ImageIO.read(new File(imgUrl));
            int width = img.getWidth();
            int height = img.getHeight();
            for (int i = 1; i < width; i++) {
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        Color color = new Color(img.getRGB(x, y));
                        //System.out.println("red:" + color.getRed() + " | green:" + color.getGreen() + " | blue:" + color.getBlue());
                        int num = color.getRed() + color.getGreen() + color.getBlue();
                        if (num >= thresholdUp || (num <=thresholdDown && y != 1)) {
                            img.setRGB(x, y, Color.WHITE.getRGB());
                        }
                    }
                }
            }
            for (int i = 1; i < width; i++) {
                Color color1 = new Color(img.getRGB(i, 1));
                int num1 = color1.getRed() + color1.getGreen() + color1.getBlue();
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        Color color = new Color(img.getRGB(x, y));

                        int num = color.getRed() + color.getGreen() + color.getBlue();
                        if (num == num1) {
                            img.setRGB(x, y, Color.BLACK.getRGB());
                        } else {
                            img.setRGB(x, y, Color.WHITE.getRGB());
                        }
                    }
                }
            }
            File file = new File(resUrl);
            if (!file.exists()) {
                File dir = file.getParentFile();
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ImageIO.write(img, "PNG", file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BufferedImage removeBackground(BufferedImage orginImage) {
        //定义一个临界阈值上限
        int thresholdUp = 500;
        int thresholdDown = 50;
        BufferedImage img = orginImage;
        try {
            int width = img.getWidth();
            int height = img.getHeight();
            for (int i = 1; i < width; i++) {
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        Color color = new Color(img.getRGB(x, y));
                        //System.out.println("red:" + color.getRed() + " | green:" + color.getGreen() + " | blue:" + color.getBlue());
                        int num = color.getRed() + color.getGreen() + color.getBlue();
                        if (num >= thresholdUp || (num <=thresholdDown && y != 1)) {
                            img.setRGB(x, y, Color.WHITE.getRGB());
                        }
                    }
                }
            }
            for (int i = 1; i < width; i++) {
                Color color1 = new Color(img.getRGB(i, 1));
                int num1 = color1.getRed() + color1.getGreen() + color1.getBlue();
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        Color color = new Color(img.getRGB(x, y));

                        int num = color.getRed() + color.getGreen() + color.getBlue();
                        if (num == num1) {
                            img.setRGB(x, y, Color.BLACK.getRGB());
                        } else {
                            img.setRGB(x, y, Color.WHITE.getRGB());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return img;
    }

    public static void cuttingImg(String imgUrl) {
        try {
            File newfile = new File(imgUrl);
            BufferedImage bufferedimage = ImageIO.read(newfile);
            int width = bufferedimage.getWidth();
            int height = bufferedimage.getHeight();
            if (width > 62) {
                bufferedimage = cropImage(bufferedimage, (int) ((width - 62) / 2), 0, (int) (width - (width - 62) / 2), (int) (height));
                if (height > 21) {
                    bufferedimage = cropImage(bufferedimage, 0, (int) ((height - 21) / 2), 62, (int) (height - (height - 21) / 2));
                }
            } else {
                if (height > 21) {
                    bufferedimage = cropImage(bufferedimage, 0, (int) ((height - 21) / 2), (int) (width), (int) (height - (height - 21) / 2));
                }
            }
            ImageIO.write(bufferedimage, "PNG", new File(imgUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static BufferedImage cuttingImg(BufferedImage bufferedimage) {
        int width = bufferedimage.getWidth();
        int height = bufferedimage.getHeight();
        if (width > 62) {
            bufferedimage = cropImage(bufferedimage, (int) ((width - 62) / 2), 0, (int) (width - (width - 62) / 2), (int) (height));
            if (height > 21) {
                bufferedimage = cropImage(bufferedimage, 0, (int) ((height - 21) / 2), 62, (int) (height - (height - 21) / 2));
            }
        } else {
            if (height > 21) {
                bufferedimage = cropImage(bufferedimage, 0, (int) ((height - 21) / 2), (int) (width), (int) (height - (height - 21) / 2));
            }
        }
        return bufferedimage;
     //   ImageIO.write(bufferedimage, "PNG", new File(imgUrl));

    }

    public static BufferedImage cropImage(BufferedImage bufferedImage, int startX, int startY, int endX, int endY) {
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        if (startX <= -1) {
            startX = 0;
        }
        if (startY <= -1) {
            startY = 0;
        }
        if (endX <= -1) {
            endX = width - 1;
        }
        if (endY <= -1) {
            endY = height - 1;
        }
        BufferedImage result = new BufferedImage(endX - startX, endY - startY, 4);
        for (int x = startX; x < endX; ++x) {
            for (int y = startY; y < endY; ++y) {
                int rgb = bufferedImage.getRGB(x, y);
                result.setRGB(x - startX, y - startY, rgb);
            }
        }
        return result;
    }


    /**
     * 图片与图片之间的黑色像素比较
     */
    public static int imgToImgCompare(BufferedImage bufImg, BufferedImage standardImg) {
        int width = bufImg.getWidth(); // 读取识别图片的高度与宽度
        int height = bufImg.getHeight();
        int count = 0;
        for (int x = 0; x < width; ++x) { // 循环每一个像素
            for (int y = 0; y < height; ++y) {
                if (bufImg.getRGB(x, y) == standardImg.getRGB(x, y)
                       && standardImg.getRGB(x, y) == Color.BLACK.getRGB()) {// 相等就加一
                    count++;
                }
            }
        }

        return count;
    }


    /**
     * 确认被分割的小图片的数字
     */
    public static String decideImgNumber(BufferedImage bufImg, HashMap<String, BufferedImage> standardMap ) {
        // 读取标准图片库
        logger.debug("标准图库：{}个",standardMap.size());
        Set<String> keys = standardMap.keySet();
        HashMap<String, Integer> resultMap = new HashMap<String, Integer>();
        for (String key : keys) {
         //   logger.debug("key：{}",key);
            BufferedImage standardImg = standardMap.get(key);
            int result = imgToImgCompare(bufImg, standardImg);//计算相似度得分
        //    logger.debug("得分：{}",result);
            resultMap.put(key,result);//保存得分
        }
        // 下面为取出比较结果的最大值
        int max = 0; // 保存最大值
        String site = null;
        Set<String> resultKeys = resultMap.keySet();
        for (String key : resultKeys) {
            int result = resultMap.get(key);
            if (result > max) {
                max = result;
                site = key.substring(0,1);
            }
        }
        return site;
        /*
        File file = new File(trainsDir);
        if (file.isDirectory()) {
            String[] filelist = file.list();
            HashMap<String, BufferedImage> map = new HashMap<String, BufferedImage>();
            for (int i = 0; i < filelist.length; i++) {
                //   System.out.println(filelist[i].substring(0,1));
                File readfile = new File(trainsDir + File.separator + filelist[i]);
                BufferedImage standardImg = ImageIO.read(readfile);
                map.put(filelist[i],standardImg);
            }
            int mapSize = map.size();
            Set<String> keys = map.keySet();
            HashMap<String, Integer> resultMap = new HashMap<String, Integer>();
            for (String key : keys) {
                BufferedImage standardImg = map.get(key);
                int result = imgToImgCompare(bufImg, standardImg);//计算相似度得分
                resultMap.put(key,result);//保存得分
            }
            // 下面为取出比较结果的最大值
            int max = 0; // 保存最大值
            String resultFileName = "";
            Set<String> resultKeys = resultMap.keySet();
            for (String key : resultKeys) {
                int result = resultMap.get(key);
                if (result > max) {
                    max = result;
                    resultFileName = key;
                }
            }
            return resultFileName.substring(0, 1);
        }
        return "";

        for (int i = 0; i < 10; i++) { // 比较大小
            if (result[i] > max) {
                max = result[i];
                site = i;
            }
        }
        return site; // 返回识别的数字
        */
    }

}
