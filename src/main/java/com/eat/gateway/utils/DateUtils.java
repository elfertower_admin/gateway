package com.eat.gateway.utils;

import java.util.Date;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.FastDateFormat;

public class DateUtils
{
  public static final long MILLIS_PER_SECOND = 1000L;
  public static final String COMPACT_DATE_FORMAT_PATTERN = "yyyyMMdd";
  public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";
  public static final String DATETIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";

  public static String getCurrentDateAsString()
  {
    FastDateFormat formatter = FastDateFormat.getInstance("yyyy-MM-dd");
    return formatter.format(new Date());
  }

  public static String getCurrentDateAsString(String pattern) {
    FastDateFormat formatter = FastDateFormat.getInstance(pattern);
    return formatter.format(new Date()); }

  public static String getCurrentDateTimeAsString() {
    FastDateFormat formatter = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");
    return formatter.format(new Date());
  }

  public static String format(Date date, String pattern)
  {
    return formatDate(date, pattern);
  }

  public static String format(Date date, String pattern, String defaultString)
  {
    return formatDate(date, pattern, defaultString);
  }

  public static String formatDate(Date date, String pattern)
  {
    return formatDate(date, pattern, "");
  }

  public static String formatDate(Date date, String pattern, String defaultString)
  {
    if (date == null)
      return defaultString;
    try {
      return DateFormatUtils.format(date, pattern);
    }
    catch (Exception localException) {
    }
    return defaultString;
  }

  public static boolean isSameDay(Date date1, Date date2)
  {
    if ((date1 == null) && (date2 == null))
      return true;
    if ((date1 != null) && (date2 != null))
      try {
        return org.apache.commons.lang3.time.DateUtils.isSameDay(date1, date2);
      }
      catch (Exception localException)
      {
      }
    return false;
  }

  public static Date parseDate(String str, String pattern) {
    return parseDate(str, new String[] { pattern });
  }

  public static Date parseDate(String str) {
    return parseDate(str, new String[] { DATETIME_FORMAT_PATTERN });
  }

  public static Date parseDate(String str, String[] patterns) {
    try {
      return org.apache.commons.lang3.time.DateUtils.parseDate(str, patterns);
    }
    catch (Exception localException) {
    }
    return null;
  }

  public static final int secondBetween(Date early, Date late) {
    return ((int)(late.getTime() / 1000L) - (int)(early.getTime() / 1000L));
  }

  public static final int minutesBetween(Date early, Date late) {
    return ((int)((late.getTime() / 1000L) - (int)(early.getTime() / 1000L))/ 60);
  }
}