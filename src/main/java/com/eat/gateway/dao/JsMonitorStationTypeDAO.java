package com.eat.gateway.dao;

import com.eat.gateway.entity.JsMonitorStationType;
import com.eat.gateway.entity.JsMonitorStationTypeExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface JsMonitorStationTypeDAO {
    long countByExample(JsMonitorStationTypeExample example);

    int deleteByExample(JsMonitorStationTypeExample example);

    int deleteByPrimaryKey(String stationTypeId);

    int insert(JsMonitorStationType record);

    int insertSelective(JsMonitorStationType record);

    List<JsMonitorStationType> selectByExample(JsMonitorStationTypeExample example);

    JsMonitorStationType selectByPrimaryKey(String stationTypeId);

    int updateByExampleSelective(@Param("record") JsMonitorStationType record, @Param("example") JsMonitorStationTypeExample example);

    int updateByExample(@Param("record") JsMonitorStationType record, @Param("example") JsMonitorStationTypeExample example);

    int updateByPrimaryKeySelective(JsMonitorStationType record);

    int updateByPrimaryKey(JsMonitorStationType record);
}