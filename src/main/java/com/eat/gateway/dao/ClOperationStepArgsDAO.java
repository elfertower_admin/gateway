package com.eat.gateway.dao;

import com.eat.gateway.entity.ClOperationStepArgs;
import com.eat.gateway.entity.ClOperationStepArgsExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClOperationStepArgsDAO {
    long countByExample(ClOperationStepArgsExample example);

    int deleteByExample(ClOperationStepArgsExample example);

    int deleteByPrimaryKey(String argId);

    int insert(ClOperationStepArgs record);

    int insertSelective(ClOperationStepArgs record);

    List<ClOperationStepArgs> selectByExample(ClOperationStepArgsExample example);

    ClOperationStepArgs selectByPrimaryKey(String argId);

    int updateByExampleSelective(@Param("record") ClOperationStepArgs record, @Param("example") ClOperationStepArgsExample example);

    int updateByExample(@Param("record") ClOperationStepArgs record, @Param("example") ClOperationStepArgsExample example);

    int updateByPrimaryKeySelective(ClOperationStepArgs record);

    int updateByPrimaryKey(ClOperationStepArgs record);
}