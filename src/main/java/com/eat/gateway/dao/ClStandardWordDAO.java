package com.eat.gateway.dao;

import com.eat.gateway.entity.ClStandardWord;
import com.eat.gateway.entity.ClStandardWordExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClStandardWordDAO {
    long countByExample(ClStandardWordExample example);

    int deleteByExample(ClStandardWordExample example);

    int deleteByPrimaryKey(String id);

    int insert(ClStandardWord record);

    int insertSelective(ClStandardWord record);

    List<ClStandardWord> selectByExampleWithBLOBs(ClStandardWordExample example);

    List<ClStandardWord> selectByExample(ClStandardWordExample example);

    ClStandardWord selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ClStandardWord record, @Param("example") ClStandardWordExample example);

    int updateByExampleWithBLOBs(@Param("record") ClStandardWord record, @Param("example") ClStandardWordExample example);

    int updateByExample(@Param("record") ClStandardWord record, @Param("example") ClStandardWordExample example);

    int updateByPrimaryKeySelective(ClStandardWord record);

    int updateByPrimaryKeyWithBLOBs(ClStandardWord record);

    int updateByPrimaryKey(ClStandardWord record);
}