package com.eat.gateway.dao;

import com.eat.gateway.entity.ClExcelDataDetail;
import com.eat.gateway.entity.ClExcelDataDetailExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClExcelDataDetailDAO {
    long countByExample(ClExcelDataDetailExample example);

    int deleteByExample(ClExcelDataDetailExample example);

    int deleteByPrimaryKey(String id);

    int insert(ClExcelDataDetail record);

    int insertSelective(ClExcelDataDetail record);

    List<ClExcelDataDetail> selectByExampleWithBLOBs(ClExcelDataDetailExample example);

    List<ClExcelDataDetail> selectByExample(ClExcelDataDetailExample example);

    ClExcelDataDetail selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ClExcelDataDetail record, @Param("example") ClExcelDataDetailExample example);

    int updateByExampleWithBLOBs(@Param("record") ClExcelDataDetail record, @Param("example") ClExcelDataDetailExample example);

    int updateByExample(@Param("record") ClExcelDataDetail record, @Param("example") ClExcelDataDetailExample example);

    int updateByPrimaryKeySelective(ClExcelDataDetail record);

    int updateByPrimaryKeyWithBLOBs(ClExcelDataDetail record);

    int updateByPrimaryKey(ClExcelDataDetail record);
}