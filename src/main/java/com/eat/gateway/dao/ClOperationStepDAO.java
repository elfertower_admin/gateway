package com.eat.gateway.dao;

import com.eat.gateway.entity.ClOperationStep;
import com.eat.gateway.entity.ClOperationStepExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClOperationStepDAO {
    long countByExample(ClOperationStepExample example);

    int deleteByExample(ClOperationStepExample example);

    int deleteByPrimaryKey(String stepId);

    int insert(ClOperationStep record);

    int insertSelective(ClOperationStep record);

    List<ClOperationStep> selectByExample(ClOperationStepExample example);

    ClOperationStep selectByPrimaryKey(String stepId);

    int updateByExampleSelective(@Param("record") ClOperationStep record, @Param("example") ClOperationStepExample example);

    int updateByExample(@Param("record") ClOperationStep record, @Param("example") ClOperationStepExample example);

    int updateByPrimaryKeySelective(ClOperationStep record);

    int updateByPrimaryKey(ClOperationStep record);
}