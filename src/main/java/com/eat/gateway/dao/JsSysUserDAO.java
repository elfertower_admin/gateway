package com.eat.gateway.dao;

import com.eat.gateway.entity.JsSysUser;
import com.eat.gateway.entity.JsSysUserExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface JsSysUserDAO {
    long countByExample(JsSysUserExample example);

    int deleteByExample(JsSysUserExample example);

    int deleteByPrimaryKey(String userCode);

    int insert(JsSysUser record);

    int insertSelective(JsSysUser record);

    List<JsSysUser> selectByExample(JsSysUserExample example);

    List<JsSysUser> selectWithOfficeByExample(JsSysUserExample example);

    JsSysUser selectByPrimaryKey(String userCode);

    int updateByExampleSelective(@Param("record") JsSysUser record, @Param("example") JsSysUserExample example);

    int updateByExample(@Param("record") JsSysUser record, @Param("example") JsSysUserExample example);

    int updateByPrimaryKeySelective(JsSysUser record);

    int updateByPrimaryKey(JsSysUser record);
}