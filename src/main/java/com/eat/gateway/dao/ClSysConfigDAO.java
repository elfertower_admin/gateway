package com.eat.gateway.dao;

import com.eat.gateway.entity.ClSysConfig;
import com.eat.gateway.entity.ClSysConfigExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ClSysConfigDAO {
    long countByExample(ClSysConfigExample example);

    int deleteByExample(ClSysConfigExample example);

    int deleteByPrimaryKey(String browserName);

    int insert(ClSysConfig record);

    int insertSelective(ClSysConfig record);

    List<ClSysConfig> selectByExample(ClSysConfigExample example);

    ClSysConfig selectByPrimaryKey(String browserName);

    int updateByExampleSelective(@Param("record") ClSysConfig record, @Param("example") ClSysConfigExample example);

    int updateByExample(@Param("record") ClSysConfig record, @Param("example") ClSysConfigExample example);

    int updateByPrimaryKeySelective(ClSysConfig record);

    int updateByPrimaryKey(ClSysConfig record);
}