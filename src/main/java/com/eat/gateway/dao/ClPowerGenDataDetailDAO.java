package com.eat.gateway.dao;

import com.eat.gateway.entity.ClPowerGenDataDetail;
import com.eat.gateway.entity.ClPowerGenDataDetailExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClPowerGenDataDetailDAO {
    long countByExample(ClPowerGenDataDetailExample example);

    int deleteByExample(ClPowerGenDataDetailExample example);

    int deleteByPrimaryKey(String powerGenId);

    int insert(ClPowerGenDataDetail record);

    int insertSelective(ClPowerGenDataDetail record);

    List<ClPowerGenDataDetail> selectByExample(ClPowerGenDataDetailExample example);

    ClPowerGenDataDetail selectByPrimaryKey(String powerGenId);

    int updateByExampleSelective(@Param("record") ClPowerGenDataDetail record, @Param("example") ClPowerGenDataDetailExample example);

    int updateByExample(@Param("record") ClPowerGenDataDetail record, @Param("example") ClPowerGenDataDetailExample example);

    int updateByPrimaryKeySelective(ClPowerGenDataDetail record);

    int updateByPrimaryKey(ClPowerGenDataDetail record);
}