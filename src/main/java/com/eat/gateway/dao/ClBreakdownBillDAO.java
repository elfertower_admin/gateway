package com.eat.gateway.dao;

import com.eat.gateway.entity.ClBreakdownBill;
import com.eat.gateway.entity.ClBreakdownBillExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClBreakdownBillDAO {
    long countByExample(ClBreakdownBillExample example);

    int deleteByExample(ClBreakdownBillExample example);

    int deleteByPrimaryKey(String breakdownId);

    int insert(ClBreakdownBill record);

    int insertSelective(ClBreakdownBill record);

    List<ClBreakdownBill> selectFullByExample(ClBreakdownBillExample example);

    List<ClBreakdownBill> selectByExample(ClBreakdownBillExample example);

    ClBreakdownBill selectByPrimaryKey(String breakdownId);

    int updateByExampleSelective(@Param("record") ClBreakdownBill record, @Param("example") ClBreakdownBillExample example);

    int updateByExample(@Param("record") ClBreakdownBill record, @Param("example") ClBreakdownBillExample example);

    int updateByPrimaryKeySelective(ClBreakdownBill record);

    int updateByPrimaryKey(ClBreakdownBill record);
}