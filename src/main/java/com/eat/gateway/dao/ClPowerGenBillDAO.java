package com.eat.gateway.dao;

import com.eat.gateway.entity.ClPowerGenBill;
import com.eat.gateway.entity.ClPowerGenBillExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClPowerGenBillDAO {
    long countByExample(ClPowerGenBillExample example);

    int deleteByExample(ClPowerGenBillExample example);

    int deleteByPrimaryKey(String powerGenId);

    int insert(ClPowerGenBill record);

    int insertSelective(ClPowerGenBill record);

    List<ClPowerGenBill> selectByExample(ClPowerGenBillExample example);

    public List<ClPowerGenBill> selectFullByExample(ClPowerGenBillExample example);

    ClPowerGenBill selectByPrimaryKey(String powerGenId);

    int updateByExampleSelective(@Param("record") ClPowerGenBill record, @Param("example") ClPowerGenBillExample example);

    int updateByExample(@Param("record") ClPowerGenBill record, @Param("example") ClPowerGenBillExample example);

    int updateByPrimaryKeySelective(ClPowerGenBill record);

    int updateByPrimaryKey(ClPowerGenBill record);
}