package com.eat.gateway.dao;

import com.eat.gateway.entity.ClExcelDataBatch;
import com.eat.gateway.entity.ClExcelDataBatchExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClExcelDataBatchDAO {
    long countByExample(ClExcelDataBatchExample example);

    int deleteByExample(ClExcelDataBatchExample example);

    int deleteByPrimaryKey(String id);

    int insert(ClExcelDataBatch record);

    int insertSelective(ClExcelDataBatch record);

    List<ClExcelDataBatch> selectByExample(ClExcelDataBatchExample example);

    ClExcelDataBatch selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ClExcelDataBatch record, @Param("example") ClExcelDataBatchExample example);

    int updateByExample(@Param("record") ClExcelDataBatch record, @Param("example") ClExcelDataBatchExample example);

    int updateByPrimaryKeySelective(ClExcelDataBatch record);

    int updateByPrimaryKey(ClExcelDataBatch record);
}