package com.eat.gateway.dao;

import com.eat.gateway.entity.JsMonitorNotice;
import com.eat.gateway.entity.JsMonitorNoticeExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface JsMonitorNoticeDAO {
    long countByExample(JsMonitorNoticeExample example);

    int deleteByExample(JsMonitorNoticeExample example);

    int deleteByPrimaryKey(String noticeId);

    int insert(JsMonitorNotice record);

    int insertSelective(JsMonitorNotice record);

    List<JsMonitorNotice> selectByExample(JsMonitorNoticeExample example);

    List<JsMonitorNotice> selectWithUserByExample(JsMonitorNoticeExample example);

    JsMonitorNotice selectByPrimaryKey(String noticeId);

    int updateByExampleSelective(@Param("record") JsMonitorNotice record, @Param("example") JsMonitorNoticeExample example);

    int updateByExample(@Param("record") JsMonitorNotice record, @Param("example") JsMonitorNoticeExample example);

    int updateByPrimaryKeySelective(JsMonitorNotice record);

    int updateByPrimaryKey(JsMonitorNotice record);
}