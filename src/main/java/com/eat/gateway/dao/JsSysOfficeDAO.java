package com.eat.gateway.dao;

import com.eat.gateway.entity.JsSysOffice;
import com.eat.gateway.entity.JsSysOfficeExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface JsSysOfficeDAO {
    long countByExample(JsSysOfficeExample example);

    int deleteByExample(JsSysOfficeExample example);

    int deleteByPrimaryKey(String officeCode);

    int insert(JsSysOffice record);

    int insertSelective(JsSysOffice record);

    List<JsSysOffice> selectByExample(JsSysOfficeExample example);

    JsSysOffice selectByPrimaryKey(String officeCode);

    int updateByExampleSelective(@Param("record") JsSysOffice record, @Param("example") JsSysOfficeExample example);

    int updateByExample(@Param("record") JsSysOffice record, @Param("example") JsSysOfficeExample example);

    int updateByPrimaryKeySelective(JsSysOffice record);

    int updateByPrimaryKey(JsSysOffice record);
}