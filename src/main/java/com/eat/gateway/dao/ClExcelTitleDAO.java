package com.eat.gateway.dao;

import com.eat.gateway.entity.ClExcelTitle;
import com.eat.gateway.entity.ClExcelTitleExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClExcelTitleDAO {
    long countByExample(ClExcelTitleExample example);

    int deleteByExample(ClExcelTitleExample example);

    int insert(ClExcelTitle record);

    int insertSelective(ClExcelTitle record);

    List<ClExcelTitle> selectByExample(ClExcelTitleExample example);

    int updateByExampleSelective(@Param("record") ClExcelTitle record, @Param("example") ClExcelTitleExample example);

    int updateByExample(@Param("record") ClExcelTitle record, @Param("example") ClExcelTitleExample example);
}