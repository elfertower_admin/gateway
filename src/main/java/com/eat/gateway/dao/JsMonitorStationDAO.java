package com.eat.gateway.dao;

import com.eat.gateway.entity.JsMonitorStation;
import com.eat.gateway.entity.JsMonitorStationExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface JsMonitorStationDAO {
    long countByExample(JsMonitorStationExample example);

    int deleteByExample(JsMonitorStationExample example);

    int deleteByPrimaryKey(String stationId);

    int insert(JsMonitorStation record);

    int insertSelective(JsMonitorStation record);

    List<JsMonitorStation> selectByExample(JsMonitorStationExample example);

    JsMonitorStation selectByPrimaryKey(String stationId);

    int updateByExampleSelective(@Param("record") JsMonitorStation record, @Param("example") JsMonitorStationExample example);

    int updateByExample(@Param("record") JsMonitorStation record, @Param("example") JsMonitorStationExample example);

    int updateByPrimaryKeySelective(JsMonitorStation record);

    int updateByPrimaryKey(JsMonitorStation record);
}