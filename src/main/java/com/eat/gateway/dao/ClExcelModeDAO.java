package com.eat.gateway.dao;

import com.eat.gateway.entity.ClExcelMode;
import com.eat.gateway.entity.ClExcelModeExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ClExcelModeDAO {
    long countByExample(ClExcelModeExample example);

    int deleteByExample(ClExcelModeExample example);

    int deleteByPrimaryKey(String modeId);

    int insert(ClExcelMode record);

    int insertSelective(ClExcelMode record);

    List<ClExcelMode> selectByExample(ClExcelModeExample example);

    ClExcelMode selectByPrimaryKey(String modeId);

    int updateByExampleSelective(@Param("record") ClExcelMode record, @Param("example") ClExcelModeExample example);

    int updateByExample(@Param("record") ClExcelMode record, @Param("example") ClExcelModeExample example);

    int updateByPrimaryKeySelective(ClExcelMode record);

    int updateByPrimaryKey(ClExcelMode record);
}