package com.eat.gateway.schedules;

import com.eat.gateway.dao.ClPowerGenBillDAO;
import com.eat.gateway.dao.JsMonitorNoticeDAO;
import com.eat.gateway.dao.JsSysOfficeDAO;
import com.eat.gateway.entity.*;
import com.eat.gateway.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@Order(1000)
public class PowerGenerateBillExecutor {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private JsMonitorNoticeDAO noticeDAO;

    @Resource
    private JsSysOfficeDAO officeDAO;

    @Resource
    private ClPowerGenBillDAO clPowerGenBillDAO;

    @PostConstruct()
    public void init() {
        logger.debug("发电工单处理任务被初始化...");
    }

    @PreDestroy
    public void destroy(){
        logger.debug("发电工单处理任务被销毁...");
    }

//    @Scheduled(cron="0 0/1 * * * ?")
    public void dataExecutor() {
        Date now = new Date();
        logger.debug("发电工单处理任务开始执行，时间：{}", DateUtils.formatDate(now,DateUtils.DATETIME_FORMAT_PATTERN));

        //机构
        JsSysOfficeExample officeExample = new JsSysOfficeExample();
        officeExample.createCriteria().andStatusEqualTo("0");
        officeExample.setOrderByClause(" office_code asc ");
        List<JsSysOffice> officeList = this.officeDAO.selectByExample(officeExample);

        ClPowerGenBillExample clPowerGenBillExample = new ClPowerGenBillExample();
        clPowerGenBillExample.createCriteria().andAcceptTimeIsNull();
        clPowerGenBillExample.or().andWarnningClearTimeIsNull();
        List<ClPowerGenBill> billList = this.clPowerGenBillDAO.selectFullByExample(clPowerGenBillExample);

        if(null != billList && billList.size() != 0){
            for (ClPowerGenBill bill : billList) {
                String billCode = bill.getBillCode();
                logger.debug("获得发电工单：{}",billCode);
                int minutes = DateUtils.minutesBetween(bill.getWarnningTime(),now);
                logger.debug("告警时间过去了：{}分钟",minutes);
                JsMonitorNoticeExample jsMonitorNoticeExample = new JsMonitorNoticeExample();
                jsMonitorNoticeExample.createCriteria().andNoticeTypeEqualTo("2")
                        .andBillCodeEqualTo(billCode).andSendeeCodeEqualTo(bill.getUserCode());
                long noticedTimes = noticeDAO.countByExample(jsMonitorNoticeExample);
                int prepareNotice = (int)noticedTimes + 1;
                logger.debug("发电工单：{}已经发送：{}次通知",billCode,noticedTimes);

                String stationType = bill.getStationType();
                Date acceptTime = bill.getAcceptTime();
                if (null == acceptTime) {
                    logger.debug("发电工单：{}没有接单时间", billCode);

                    if (minutes <=  10 && noticedTimes == 0L){
                        JsMonitorNotice record = new JsMonitorNotice();
                        record.setNoticeId(this.createId());
                        record.setBillCode(billCode);
                        record.setCreateDate(now);
                        record.setIsSend("0");//等待发送
                        record.setNoticeTimes(prepareNotice);
                        record.setSendeeCode(bill.getUserCode());
                        record.setOfficeCode(bill.getOfficeCode());
                        record.setStatus("0");
                        record.setUpdateDate(now);
                        record.setUpReport(0);//非上报信息
                        record.setIsPower("1");//发电通知
                        record.setNoticeType("2");//发电提醒
                        //生成通知内容
                        String content = this.getContent(bill,record,DateUtils.minutesBetween(bill.getStartTime(),now));
                        logger.debug("通知内容：{}",content);
                        record.setContent(content);
                        noticeDAO.insertSelective(record);
                    }
                } else {
                    logger.debug("发电工单：{}的接单时间是：{}",billCode,DateUtils.formatDate(acceptTime,DateUtils.DATETIME_FORMAT_PATTERN));
                }
                BigDecimal v = bill.getVoltage();
                logger.debug("发电工单：{}的电池电压：{}V",billCode,v);
                boolean up_report_city = false;
                if (stationType.equals("3")){
                    logger.debug("发电工单：{}的站点是标准站点",billCode);
                    Date lastNoticeTime = now;//上次通知时间
                    JsMonitorNotice lastNotice = this.getLastestPowerNotice(bill);
                    if (null != lastNotice){
                        lastNoticeTime = lastNotice.getCreateDate();
                    }
                    int noticePassed = DateUtils.minutesBetween(lastNoticeTime,now);
                    logger.debug("发电工单：{}，距上次发送回单提醒过去了：{}分钟",billCode,noticePassed);
                    if(v != null && v.doubleValue() < 50.00D && noticePassed >= 15){
                        JsMonitorNotice record = new JsMonitorNotice();
                        record.setNoticeId(this.createId());
                        record.setBillCode(billCode);
                        record.setCreateDate(now);
                        record.setIsSend("0");//等待发送
                        record.setNoticeTimes(prepareNotice);
                        record.setSendeeCode(bill.getUserCode());
                        record.setOfficeCode(bill.getOfficeCode());
                        record.setStatus("0");
                        record.setUpdateDate(now);
                        record.setUpReport(0);//非上报信息
                        record.setIsPower("1");//发电通知
                        record.setNoticeType("2");//发电提醒
                        //生成通知内容
                        String content = this.getContent(bill,record,DateUtils.minutesBetween(bill.getStartTime(),now));
                        logger.debug("通知内容：{}",content);
                        record.setContent(content);
                        noticeDAO.insertSelective(record);

                        if (v.doubleValue() < 48.00D){
                            up_report_city = true;
                        }
                    }
                } else {
                    logger.debug("发电工单：{}的站点是高级站点",billCode);
                    if (v != null && v.doubleValue() < 52.00D){
                        up_report_city = true;
                    }
                }
                if (up_report_city){
                    //向地市级发送通知
                    JsSysOffice office = getMamageOffice(officeList,bill.getParentOfficeCode());
                    if (null == office){
                        logger.error("未找到officeCode = {}的管理部门，请正确按约定命名管理部门",bill.getParentOfficeCode());
                        continue;
                    }
                    logger.debug("找到管理部门：{}",office.getOfficeName());
                    JsMonitorNotice city_record = new JsMonitorNotice();
                    city_record.setNoticeId(this.createId());
                    city_record.setBillCode(billCode);
                    city_record.setCreateDate(now);
                    city_record.setIsSend("0");//等待发送
                    city_record.setNoticeTimes(prepareNotice);
                    city_record.setOfficeCode(office.getOfficeCode());
                    city_record.setStatus("0");
                    city_record.setUpdateDate(now);
                    city_record.setUpReport(1);//上报信息
                    city_record.setIsPower("1");//发电通知
                    city_record.setNoticeType("2");//发电提醒
                    //生成通知内容
                    String content = this.getContent(bill,city_record,DateUtils.minutesBetween(bill.getStartTime(),now));
                    logger.debug("通知内容：{}",content);
                    city_record.setContent(content);
                    noticeDAO.insertSelective(city_record);

                    String breakdownLong = bill.getBreakdownLong();
                    if (StringUtils.isNotBlank(breakdownLong)){
                        logger.debug("发电工单：{}的告警内容是：{}",billCode,bill.getWarnningName());
                        JsMonitorNotice province_record = new JsMonitorNotice();
                        province_record.setNoticeId(this.createId());
                        province_record.setBillCode(billCode);
                        province_record.setCreateDate(now);
                        province_record.setIsSend("0");//等待发送
                        province_record.setNoticeTimes(prepareNotice);
                        JsSysOffice province_office = getMamageOffice(officeList,"1");
                        if (null == province_office){
                            logger.error("未找到officeCode = {}的管理部门，请正确按约定命名管理部门","1");
                            continue;
                        }
                        logger.debug("找到管理部门：{}",province_office.getOfficeName());
                        province_record.setOfficeCode(province_office.getOfficeCode());
                        province_record.setStatus("0");
                        province_record.setUpdateDate(now);
                        province_record.setUpReport(1);//上报信息
                        province_record.setIsPower("1");//发电通知
                        province_record.setNoticeType("2");//发电提醒
                        //生成通知内容
                        StringBuilder sb = new StringBuilder();
                        sb.append(content);
                        sb.append(" ");
                        sb.append("断站时长：");
                        sb.append(breakdownLong);
                        sb.append("分钟");
                        province_record.setContent(sb.toString());
                        noticeDAO.insertSelective(province_record);
                    }
                }

            }
        }
    }

    private JsSysOffice getMamageOffice(List<JsSysOffice> officeList, String parentCode){
        JsSysOffice manageOffice = null;
        for (JsSysOffice office : officeList) {
            if (null != office && office.getParentCode().equals(parentCode) &&
                    (office.getOfficeName().indexOf("地市级") != -1 || office.getOfficeName().indexOf("网络维护") != -1)){
                manageOffice = office;
                break;
            }
        }
        return manageOffice;
    }

    private JsMonitorNotice getLastestPowerNotice(ClPowerGenBill bill){
        //得到最后的发电通知
        JsMonitorNotice jsMonitorNotice = null;

        JsMonitorNoticeExample noticeExample = new JsMonitorNoticeExample();
        JsMonitorNoticeExample.Criteria criteria = noticeExample.createCriteria()
                .andNoticeTypeEqualTo("2");//发电提醒
        criteria = criteria.andStatusEqualTo("0");
        criteria = criteria.andBillCodeEqualTo(bill.getBillCode());
        criteria = criteria.andSendeeCodeEqualTo(bill.getUserCode());
        noticeExample.setOrderByClause(" create_date desc ");
        List<JsMonitorNotice> list = this.noticeDAO.selectByExample(noticeExample);
        if(null != list && list.size() > 0){
            jsMonitorNotice = list.get(0);
        }
        return jsMonitorNotice;
    }

    private String createId(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    private String getContent(ClPowerGenBill bill, JsMonitorNotice record,int minutes){
        StringBuilder sb = new StringBuilder();
        sb.append("【发电工单】：");
        sb.append(bill.getCity());
        sb.append(" ");
        sb.append(bill.getCounties());
        sb.append(" ");
        /*
        sb.append(bill.getVillages());
        sb.append(" ");
        */
        sb.append(bill.getStationName());
        sb.append("，第【");
        sb.append(record.getNoticeTimes());
        sb.append("】次通知。工单编码：");
        sb.append(bill.getBillCode());
        sb.append("，告警内容：");
        sb.append(bill.getWarnningName());
        sb.append("，告警发生时间：");
        sb.append(DateUtils.formatDate(bill.getWarnningTime(),DateUtils.DATETIME_FORMAT_PATTERN));
        sb.append("，已停电");
        sb.append(minutes);
        sb.append("分钟，当前电压");
        sb.append(bill.getVoltage());
        sb.append("V，实际维护人员：");
        sb.append(bill.getUserName());
        sb.append("，手机：");
        sb.append(bill.getMobile());
        return sb.toString();
    }
}
