package com.eat.gateway.schedules;

import com.eat.gateway.dao.ClBreakdownBillDAO;
import com.eat.gateway.dao.JsMonitorNoticeDAO;
import com.eat.gateway.dao.JsSysOfficeDAO;
import com.eat.gateway.entity.*;
import com.eat.gateway.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@Order(1000)
public class BreakdownBillExecutor {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ClBreakdownBillDAO breakdownBillDAO;

    @Resource
    private JsMonitorNoticeDAO noticeDAO;

    @Resource
    private JsSysOfficeDAO officeDAO;

    @Value("${bill.breakdown.accept.baseRemind}")
    private int baseRemind;

    @Value("${bill.breakdown.accept.cityRemind}")
    private int cityRemind;

    @Value("${bill.breakdown.accept.povinceRemind}")
    private int povinceRemind;

    @Value("${bill.breakdown.back.senior}")
    private int senior;

    @Value("${bill.breakdown.back.standard}")
    private int standard;

    @Value("${bill.breakdown.back.cityRemind}")
    private int backCityRemind;

    @Value("${bill.breakdown.back.povinceRemind}")
    private int backPovinceRemind;

    @PostConstruct()
    public void init() {
        logger.debug("故障工单处理任务被初始化...");
    }

    @PreDestroy
    public void destroy(){
        logger.debug("故障工单处理任务被销毁...");
    }

  //  @Scheduled(cron="0 0/1 * * * ?")
    public void dataExecutor() {
        Date now = new Date();
        logger.debug("故障工单处理任务开始执行，时间：{}", DateUtils.formatDate(now,DateUtils.DATETIME_FORMAT_PATTERN));

        //机构
        JsSysOfficeExample officeExample = new JsSysOfficeExample();
//        officeExample.createCriteria().andStatusEqualTo("0");
        officeExample.setOrderByClause(" office_code asc ");
        List<JsSysOffice> officeList = this.officeDAO.selectByExample(officeExample);

        ClBreakdownBillExample example = new ClBreakdownBillExample();
        //去掉已经闭环的故障工单数据
        example.createCriteria().andAcceptTimeIsNull().andBackTimeIsNull();
        example.or().andBackTimeIsNull();
        List<ClBreakdownBill> billList = breakdownBillDAO.selectFullByExample(example);
        if(null != billList && billList.size() != 0){
            for (ClBreakdownBill bill : billList) {
                String billCode = bill.getBillCode();
                logger.debug("获得故障工单：{}",billCode);
                Date acceptTime = bill.getAcceptTime();
                if (null == acceptTime){
                    logger.debug("故障工单：{}没有接单时间",billCode);
                    Date warnningTime = bill.getWarnningTime();
                    logger.debug("故障工单：{}的告警时间是：{}",billCode,DateUtils.formatDate(warnningTime,DateUtils.DATETIME_FORMAT_PATTERN));
                    int minutes = DateUtils.minutesBetween(warnningTime,now);
                    logger.debug("告警时间过去了：{}分钟",minutes);
                    if (minutes > this.baseRemind){
                        logger.debug("告警时间超过包站人提醒阈值，生成包站人提醒信息等待推送");
                        JsMonitorNoticeExample noticeExample = new JsMonitorNoticeExample();
                        noticeExample.createCriteria().andStatusEqualTo("0").andNoticeTypeEqualTo("0")
                                .andBillCodeEqualTo(billCode).andSendeeCodeEqualTo(bill.getUserCode());
                        long noticedTimes = noticeDAO.countByExample(noticeExample);
                        int prepareNotice = (int)noticedTimes + 1;
                        logger.debug("接单通知已经通知次数：{}次，准备通知：第{}次",noticedTimes,prepareNotice);

                        JsMonitorNotice record = new JsMonitorNotice();
                        record.setNoticeId(this.createId());
                        record.setBillCode(billCode);
                        record.setCreateDate(now);
                        record.setIsSend("0");//等待发送
                        record.setNoticeTimes(prepareNotice);
                        record.setSendeeCode(bill.getUserCode());
                        record.setOfficeCode(bill.getOfficeCode());
                        record.setStatus("0");
                        record.setUpdateDate(now);
                        record.setUpReport(0);//非上报信息
                        record.setIsPower("0");//非发电通知
                        record.setNoticeType("0");//接单提醒
                        //生成通知内容
                        String content = this.getContent(bill,record,minutes);
                        logger.debug("通知内容：{}",content);
                        record.setContent(content);
                        noticeDAO.insertSelective(record);
                        //通知负责人
                        noticeLeader(record,officeList);
                        if (minutes > this.cityRemind){
                            logger.debug("告警时间超过地市级提醒阈值，生成地市级提醒信息等待推送");
                            JsMonitorNotice city_record = new JsMonitorNotice();
                            city_record.setNoticeId(this.createId());
                            city_record.setBillCode(billCode);
                            city_record.setCreateDate(now);
                            city_record.setIsSend("0");//等待发送
                            city_record.setNoticeTimes(prepareNotice);
                            JsSysOffice office = getMamageOffice(officeList,bill.getParentOfficeCode());
                            if (null == office){
                                logger.error("未找到officeCode = {}的管理部门，请正确按约定命名管理部门",bill.getParentOfficeCode());
                                continue;
                            }
                            logger.debug("找到管理部门：{}",office.getOfficeName());
                            city_record.setOfficeCode(office.getOfficeCode());
                            city_record.setStatus("0");
                            city_record.setUpdateDate(now);
                            city_record.setUpReport(1);//上报信息
                            city_record.setIsPower("0");//非发电通知
                            city_record.setNoticeType("0");//接单提醒
                            //生成通知内容
                            city_record.setContent(content);
                            noticeDAO.insertSelective(city_record);

                            if(minutes > this.povinceRemind){
                                logger.debug("告警时间超过省级提醒阈值，生成省级提醒信息等待推送");
                                JsMonitorNotice province_record = new JsMonitorNotice();
                                province_record.setNoticeId(this.createId());
                                province_record.setBillCode(billCode);
                                province_record.setCreateDate(now);
                                province_record.setIsSend("0");//等待发送
                                province_record.setNoticeTimes(prepareNotice);
                                JsSysOffice province_office = getMamageOffice(officeList,"1");
                                if (null == province_office){
                                    logger.error("未找到officeCode = {}的管理部门，请正确按约定命名管理部门","1");
                                    continue;
                                }
                                logger.debug("找到管理部门：{}",province_office.getOfficeName());
                                province_record.setOfficeCode(province_office.getOfficeCode());
                                province_record.setStatus("0");
                                province_record.setUpdateDate(now);
                                province_record.setUpReport(1);//上报信息
                                province_record.setIsPower("0");//非发电通知
                                province_record.setNoticeType("0");//接单提醒
                                //生成通知内容
                                province_record.setContent(content);
                                noticeDAO.insertSelective(province_record);

                            } else {
                                logger.debug("故障工单：{}未超过省级提醒阈值，不生成省级提醒信息",billCode);
                            }
                        } else {
                            logger.debug("故障工单：{}未超过地市级提醒阈值，不生成地市级提醒信息",billCode);
                        }
                    } else {
                        logger.debug("故障工单：{}未超过包站人提醒阈值，不生成提醒信息",billCode);
                    }
                } else {
                    logger.debug("故障工单：{}的接单时间是：{}",billCode,DateUtils.formatDate(acceptTime,DateUtils.DATETIME_FORMAT_PATTERN));
                    String stationType = bill.getStationType();
                    int minutes = DateUtils.minutesBetween(acceptTime,now);
                    logger.debug("故障工单：{}，接单时间过去了：{}分钟",billCode,minutes);

                    if ((stationType.equals("3") && minutes > this.standard) ||//标准站
                            (stationType.equals("2") && minutes > this.senior)){//高级站
                        Date lastNoticeTime = now;//上次通知时间
                        JsMonitorNotice lastNotice = getLastestBackNotice(bill);
                        if (null != lastNotice){
                            lastNoticeTime = lastNotice.getCreateDate();
                        }
                        int noticePassed = DateUtils.minutesBetween(lastNoticeTime,now);
                        logger.debug("故障工单：{}，距上次发送回单提醒过去了：{}分钟",billCode,noticePassed);
                        JsMonitorNoticeExample noticeExample = new JsMonitorNoticeExample();
                        noticeExample.createCriteria().andStatusEqualTo("0").andNoticeTypeEqualTo("1")
                                .andBillCodeEqualTo(billCode).andSendeeCodeEqualTo(bill.getUserCode());
                        long noticedTimes = noticeDAO.countByExample(noticeExample);
                        int prepareNotice = (int)noticedTimes + 1;
                        logger.debug("故障工单：{}，回单通知已经通知次数：{}次，准备通知：第{}次",billCode,noticedTimes,prepareNotice);
                        //工单历时
                        String billExecDuration = bill.getBillExecDuration() == null ? String.valueOf(minutes):bill.getBillExecDuration();
                        logger.debug("故障工单：{}，工单历时：{}分钟",billCode,billExecDuration);
                        if (noticePassed == 0 || noticePassed >= 60){
                            JsMonitorNotice record = new JsMonitorNotice();
                            record.setNoticeId(this.createId());
                            record.setBillCode(billCode);
                            record.setCreateDate(now);
                            record.setIsSend("0");//等待发送
                            record.setNoticeTimes(prepareNotice);
                            record.setSendeeCode(bill.getUserCode());
                            record.setOfficeCode(bill.getOfficeCode());
                            record.setStatus("0");
                            record.setUpdateDate(now);
                            record.setUpReport(0);//非上报信息
                            record.setIsPower("0");//非发电通知
                            record.setNoticeType("1");//回单提醒
                            //生成通知内容
                            String content = this.getContent(bill,record,minutes);
                            content = content.replaceAll("接单","回单");
                            String warnningDuration = bill.getWarnningDuration() == null ? String.valueOf(minutes):bill.getWarnningDuration();
                            content = content.replaceAll("告警历时" + warnningDuration,"工单历时" + billExecDuration);
                            logger.debug("通知内容：{}",content);
                            record.setContent(content);
                            noticeDAO.insertSelective(record);
                            //通知负责人
                            noticeLeader(record,officeList);

                            int execDuration = Integer.valueOf(billExecDuration);
                            if (execDuration > backCityRemind){
                                logger.debug("故障工单：{}，达到地市级回单提醒阈值",billCode);
                                JsMonitorNotice city_record = new JsMonitorNotice();
                                city_record.setNoticeId(this.createId());
                                city_record.setBillCode(billCode);
                                city_record.setCreateDate(now);
                                city_record.setIsSend("0");//等待发送
                                city_record.setNoticeTimes(prepareNotice);
                                JsSysOffice office = getMamageOffice(officeList,bill.getParentOfficeCode());
                                if (null == office){
                                    logger.error("未找到officeCode = {}的管理部门，请正确按约定命名管理部门",bill.getParentOfficeCode());
                                    continue;
                                }
                                logger.debug("找到管理部门：{}",office.getOfficeName());
                                city_record.setOfficeCode(office.getOfficeCode());
                                city_record.setStatus("0");
                                city_record.setUpdateDate(now);
                                city_record.setUpReport(1);//上报信息
                                city_record.setIsPower("0");//非发电通知
                                city_record.setNoticeType("1");//回单提醒
                                //生成通知内容
                                content = this.getContent(bill,city_record,minutes);
                                content = content.replaceAll("接单","回单");
                                warnningDuration = StringUtils.isBlank(bill.getWarnningDuration()) ? String.valueOf(minutes):bill.getWarnningDuration();
                                content = content.replaceAll("告警历时" + warnningDuration,"工单历时" + billExecDuration);
                                logger.debug("通知内容：{}",content);
                                city_record.setContent(content);
                                noticeDAO.insertSelective(city_record);
                            }
                            if (execDuration > this.backPovinceRemind){
                                logger.debug("故障工单：{}，达到省级回单提醒阈值",billCode);
                                JsSysOffice province_office = getMamageOffice(officeList,"1");
                                if (null == province_office){
                                    logger.error("未找到officeCode = {}的管理部门，请正确按约定命名管理部门","1");
                                    continue;
                                }
                                logger.debug("找到管理部门：{}",province_office.getOfficeName());
                                JsMonitorNotice province_record = new JsMonitorNotice();
                                province_record.setNoticeId(this.createId());
                                province_record.setBillCode(billCode);
                                province_record.setCreateDate(now);
                                province_record.setIsSend("0");//等待发送
                                province_record.setNoticeTimes(prepareNotice);

                                province_record.setOfficeCode(province_office.getOfficeCode());
                                province_record.setStatus("0");
                                province_record.setUpdateDate(now);
                                province_record.setUpReport(1);//上报信息
                                province_record.setIsPower("0");//非发电通知
                                province_record.setNoticeType("1");//回单提醒
                                //生成通知内容
                                String province_record_content = this.getContent(bill,province_record,minutes);
                                province_record_content = province_record_content.replaceAll("接单","回单");
                                warnningDuration = StringUtils.isBlank(bill.getWarnningDuration()) ? String.valueOf(minutes):bill.getWarnningDuration();
                                province_record_content = province_record_content.replaceAll("告警历时" + warnningDuration,"工单历时" + billExecDuration);
                                logger.debug("通知内容：{}",province_record_content);
                                province_record.setContent(province_record_content);
                                noticeDAO.insertSelective(province_record);
                            }
                        }

                    }
                }
            }
        }
    }
    private void noticeLeader(JsMonitorNotice record ,List<JsSysOffice> officeList){
        record.setNoticeId(this.createId());
        record.setSendeeCode(null);
        for (JsSysOffice office : officeList) {
            String pc = office.getParentCode();
            if (pc.equals(record.getOfficeCode()) &&
                    office.getOfficeName().contains("负责人")){
                record.setOfficeCode(office.getOfficeCode());
                this.noticeDAO.insertSelective(record);
                break;
            }
        }
    }

    private JsMonitorNotice getLastestBackNotice(ClBreakdownBill bill){
        //得到最后的回单通知
        JsMonitorNotice jsMonitorNotice = null;

        JsMonitorNoticeExample noticeExample = new JsMonitorNoticeExample();
        JsMonitorNoticeExample.Criteria criteria = noticeExample.createCriteria().andNoticeTypeEqualTo("1");//回单提醒
        criteria = criteria.andStatusEqualTo("0");
        criteria = criteria.andBillCodeEqualTo(bill.getBillCode());
        criteria = criteria.andSendeeCodeEqualTo(bill.getUserCode());
        noticeExample.setOrderByClause(" create_date desc ");
        List<JsMonitorNotice> list = this.noticeDAO.selectByExample(noticeExample);
        if(null != list && list.size() > 0){
            jsMonitorNotice = list.get(0);
        }
        return jsMonitorNotice;
    }

    private String getContent(ClBreakdownBill bill, JsMonitorNotice record,int minutes) {
        StringBuilder sb = new StringBuilder();
        sb.append("【故障工单】：");
        sb.append(bill.getCity());
        sb.append(" ");
        sb.append(bill.getCounties());
        sb.append(" ");
       /* sb.append(bill.getVillages());
        sb.append(" ");
        */
        sb.append(bill.getStationName());
        sb.append("，第【");
        sb.append(record.getNoticeTimes());
        sb.append("】次通知接单。故障编码：");
        sb.append(bill.getBillCode());
        sb.append("，告警内容：");
        sb.append(bill.getWarnningName());
        sb.append("，告警发生时间：");
        sb.append(DateUtils.formatDate(bill.getWarnningTime(),DateUtils.DATETIME_FORMAT_PATTERN));
//        String warnningDuration = bill.getWarnningDuration() == null ? String.valueOf(minutes):bill.getWarnningDuration();

        String warnningDuration = String.valueOf(minutes);
        sb.append("，告警历时");
        sb.append(warnningDuration);
        sb.append("分钟，实际维护人员：");
        sb.append(bill.getUserName());
        sb.append("，手机：");
        sb.append(bill.getMobile());
        return sb.toString();
    }

    private String createId(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    private JsSysOffice getMamageOffice(List<JsSysOffice> officeList, String parentCode){
        JsSysOffice manageOffice = null;
        for (JsSysOffice office : officeList) {
            if (null != office && office.getParentCode().equals(parentCode) &&
                    (office.getOfficeName().indexOf("地市级") != -1 || office.getOfficeName().indexOf("网络维护") != -1)){
                manageOffice = office;
                break;
            }
        }
        return manageOffice;
    }

}
