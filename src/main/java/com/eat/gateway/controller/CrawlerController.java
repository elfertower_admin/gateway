package com.eat.gateway.controller;

import com.eat.gateway.mail.MailService;
import com.eat.gateway.service.IDaiaAchieveSrevice;
import com.eat.gateway.utils.ApplicationContextProvider;
import com.eat.gateway.utils.NameableThreadPoolExecutor;
import com.eat.gateway.utils.ThreadPoolUtils;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.bean.WxCpDepart;
import me.chanjar.weixin.cp.bean.WxCpUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping(value = "/api")
public class CrawlerController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IDaiaAchieveSrevice service;

    @RequestMapping(value = "/getAllDept")
    public List<WxCpDepart> getAllDept(){
        List<WxCpDepart> list = null;
        try {
            list = this.service.getAllDeptments();
        } catch (WxErrorException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    @RequestMapping(value = "/getUsersByDept")
    public List<WxCpUser> getUsersByDept(@RequestParam("dept_id") Long deptId){
        List<WxCpUser> list = null;
        try {
            logger.debug("获取部门：{}下的用户",deptId);
            list = this.service.getUsersByDepartment(deptId);
            logger.debug("获得用户数量：{}",list.size());
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return list;
    }

    @RequestMapping(value = "/crawleAndSend")
    public void crawleAndSend(){
        int count = 0;
        while(true){
            try {
                service.achieveDataFile();
                break;
            } catch (Exception e){
                logger.error(e.getMessage());
                e.printStackTrace();
            }
            count ++;
            logger.debug("第{}次尝试抓取数据，失败",count);

            if ( count >= 100){
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        logger.debug("启动多线程发送邮件");
                        MailService mailService = ApplicationContextProvider.getBean(MailService.class);
                        StringBuilder sb = new StringBuilder();
                        sb.append("数据抓取超过最大尝试次数，本次抓取任务失败，请检查。");
                        mailService.sendSimpleMail("7614911@qq.com","铁塔工单监控系统提醒",sb.toString());
                    }
                };
                Thread thread = new Thread(runnable);
                ThreadPoolUtils.getThreadPoolExecutor().execute(thread);
                return;
            }
        }
        /*
        try {
            String batchId = this.service.parseFile(fileName);
            logger.info("获取数据批次为：{}",batchId);
            this.service.executePrepareSend(batchId);

            this.service.sendMsg();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

    @PreDestroy
    public void destroy(){

    }
}
