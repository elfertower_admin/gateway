package com.eat.gateway.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClPowerGenDataDetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ClPowerGenDataDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPowerGenIdIsNull() {
            addCriterion("power_gen_id is null");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdIsNotNull() {
            addCriterion("power_gen_id is not null");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdEqualTo(String value) {
            addCriterion("power_gen_id =", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdNotEqualTo(String value) {
            addCriterion("power_gen_id <>", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdGreaterThan(String value) {
            addCriterion("power_gen_id >", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdGreaterThanOrEqualTo(String value) {
            addCriterion("power_gen_id >=", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdLessThan(String value) {
            addCriterion("power_gen_id <", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdLessThanOrEqualTo(String value) {
            addCriterion("power_gen_id <=", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdLike(String value) {
            addCriterion("power_gen_id like", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdNotLike(String value) {
            addCriterion("power_gen_id not like", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdIn(List<String> values) {
            addCriterion("power_gen_id in", values, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdNotIn(List<String> values) {
            addCriterion("power_gen_id not in", values, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdBetween(String value1, String value2) {
            addCriterion("power_gen_id between", value1, value2, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdNotBetween(String value1, String value2) {
            addCriterion("power_gen_id not between", value1, value2, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andBillCodeIsNull() {
            addCriterion("bill_code is null");
            return (Criteria) this;
        }

        public Criteria andBillCodeIsNotNull() {
            addCriterion("bill_code is not null");
            return (Criteria) this;
        }

        public Criteria andBillCodeEqualTo(String value) {
            addCriterion("bill_code =", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotEqualTo(String value) {
            addCriterion("bill_code <>", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeGreaterThan(String value) {
            addCriterion("bill_code >", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeGreaterThanOrEqualTo(String value) {
            addCriterion("bill_code >=", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLessThan(String value) {
            addCriterion("bill_code <", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLessThanOrEqualTo(String value) {
            addCriterion("bill_code <=", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLike(String value) {
            addCriterion("bill_code like", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotLike(String value) {
            addCriterion("bill_code not like", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeIn(List<String> values) {
            addCriterion("bill_code in", values, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotIn(List<String> values) {
            addCriterion("bill_code not in", values, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeBetween(String value1, String value2) {
            addCriterion("bill_code between", value1, value2, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotBetween(String value1, String value2) {
            addCriterion("bill_code not between", value1, value2, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillStateIsNull() {
            addCriterion("bill_state is null");
            return (Criteria) this;
        }

        public Criteria andBillStateIsNotNull() {
            addCriterion("bill_state is not null");
            return (Criteria) this;
        }

        public Criteria andBillStateEqualTo(String value) {
            addCriterion("bill_state =", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateNotEqualTo(String value) {
            addCriterion("bill_state <>", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateGreaterThan(String value) {
            addCriterion("bill_state >", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateGreaterThanOrEqualTo(String value) {
            addCriterion("bill_state >=", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateLessThan(String value) {
            addCriterion("bill_state <", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateLessThanOrEqualTo(String value) {
            addCriterion("bill_state <=", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateLike(String value) {
            addCriterion("bill_state like", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateNotLike(String value) {
            addCriterion("bill_state not like", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateIn(List<String> values) {
            addCriterion("bill_state in", values, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateNotIn(List<String> values) {
            addCriterion("bill_state not in", values, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateBetween(String value1, String value2) {
            addCriterion("bill_state between", value1, value2, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateNotBetween(String value1, String value2) {
            addCriterion("bill_state not between", value1, value2, "billState");
            return (Criteria) this;
        }

        public Criteria andBillTypeIsNull() {
            addCriterion("bill_type is null");
            return (Criteria) this;
        }

        public Criteria andBillTypeIsNotNull() {
            addCriterion("bill_type is not null");
            return (Criteria) this;
        }

        public Criteria andBillTypeEqualTo(String value) {
            addCriterion("bill_type =", value, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeNotEqualTo(String value) {
            addCriterion("bill_type <>", value, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeGreaterThan(String value) {
            addCriterion("bill_type >", value, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeGreaterThanOrEqualTo(String value) {
            addCriterion("bill_type >=", value, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeLessThan(String value) {
            addCriterion("bill_type <", value, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeLessThanOrEqualTo(String value) {
            addCriterion("bill_type <=", value, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeLike(String value) {
            addCriterion("bill_type like", value, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeNotLike(String value) {
            addCriterion("bill_type not like", value, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeIn(List<String> values) {
            addCriterion("bill_type in", values, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeNotIn(List<String> values) {
            addCriterion("bill_type not in", values, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeBetween(String value1, String value2) {
            addCriterion("bill_type between", value1, value2, "billType");
            return (Criteria) this;
        }

        public Criteria andBillTypeNotBetween(String value1, String value2) {
            addCriterion("bill_type not between", value1, value2, "billType");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCountiesIsNull() {
            addCriterion("counties is null");
            return (Criteria) this;
        }

        public Criteria andCountiesIsNotNull() {
            addCriterion("counties is not null");
            return (Criteria) this;
        }

        public Criteria andCountiesEqualTo(String value) {
            addCriterion("counties =", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesNotEqualTo(String value) {
            addCriterion("counties <>", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesGreaterThan(String value) {
            addCriterion("counties >", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesGreaterThanOrEqualTo(String value) {
            addCriterion("counties >=", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesLessThan(String value) {
            addCriterion("counties <", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesLessThanOrEqualTo(String value) {
            addCriterion("counties <=", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesLike(String value) {
            addCriterion("counties like", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesNotLike(String value) {
            addCriterion("counties not like", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesIn(List<String> values) {
            addCriterion("counties in", values, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesNotIn(List<String> values) {
            addCriterion("counties not in", values, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesBetween(String value1, String value2) {
            addCriterion("counties between", value1, value2, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesNotBetween(String value1, String value2) {
            addCriterion("counties not between", value1, value2, "counties");
            return (Criteria) this;
        }

        public Criteria andResourceCodeIsNull() {
            addCriterion("resource_code is null");
            return (Criteria) this;
        }

        public Criteria andResourceCodeIsNotNull() {
            addCriterion("resource_code is not null");
            return (Criteria) this;
        }

        public Criteria andResourceCodeEqualTo(String value) {
            addCriterion("resource_code =", value, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeNotEqualTo(String value) {
            addCriterion("resource_code <>", value, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeGreaterThan(String value) {
            addCriterion("resource_code >", value, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeGreaterThanOrEqualTo(String value) {
            addCriterion("resource_code >=", value, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeLessThan(String value) {
            addCriterion("resource_code <", value, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeLessThanOrEqualTo(String value) {
            addCriterion("resource_code <=", value, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeLike(String value) {
            addCriterion("resource_code like", value, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeNotLike(String value) {
            addCriterion("resource_code not like", value, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeIn(List<String> values) {
            addCriterion("resource_code in", values, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeNotIn(List<String> values) {
            addCriterion("resource_code not in", values, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeBetween(String value1, String value2) {
            addCriterion("resource_code between", value1, value2, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andResourceCodeNotBetween(String value1, String value2) {
            addCriterion("resource_code not between", value1, value2, "resourceCode");
            return (Criteria) this;
        }

        public Criteria andStationNameIsNull() {
            addCriterion("station_name is null");
            return (Criteria) this;
        }

        public Criteria andStationNameIsNotNull() {
            addCriterion("station_name is not null");
            return (Criteria) this;
        }

        public Criteria andStationNameEqualTo(String value) {
            addCriterion("station_name =", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameNotEqualTo(String value) {
            addCriterion("station_name <>", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameGreaterThan(String value) {
            addCriterion("station_name >", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameGreaterThanOrEqualTo(String value) {
            addCriterion("station_name >=", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameLessThan(String value) {
            addCriterion("station_name <", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameLessThanOrEqualTo(String value) {
            addCriterion("station_name <=", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameLike(String value) {
            addCriterion("station_name like", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameNotLike(String value) {
            addCriterion("station_name not like", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameIn(List<String> values) {
            addCriterion("station_name in", values, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameNotIn(List<String> values) {
            addCriterion("station_name not in", values, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameBetween(String value1, String value2) {
            addCriterion("station_name between", value1, value2, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameNotBetween(String value1, String value2) {
            addCriterion("station_name not between", value1, value2, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdIsNull() {
            addCriterion("station_servicing_id is null");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdIsNotNull() {
            addCriterion("station_servicing_id is not null");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdEqualTo(String value) {
            addCriterion("station_servicing_id =", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdNotEqualTo(String value) {
            addCriterion("station_servicing_id <>", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdGreaterThan(String value) {
            addCriterion("station_servicing_id >", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdGreaterThanOrEqualTo(String value) {
            addCriterion("station_servicing_id >=", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdLessThan(String value) {
            addCriterion("station_servicing_id <", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdLessThanOrEqualTo(String value) {
            addCriterion("station_servicing_id <=", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdLike(String value) {
            addCriterion("station_servicing_id like", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdNotLike(String value) {
            addCriterion("station_servicing_id not like", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdIn(List<String> values) {
            addCriterion("station_servicing_id in", values, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdNotIn(List<String> values) {
            addCriterion("station_servicing_id not in", values, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdBetween(String value1, String value2) {
            addCriterion("station_servicing_id between", value1, value2, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdNotBetween(String value1, String value2) {
            addCriterion("station_servicing_id not between", value1, value2, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andVoltageIsNull() {
            addCriterion("voltage is null");
            return (Criteria) this;
        }

        public Criteria andVoltageIsNotNull() {
            addCriterion("voltage is not null");
            return (Criteria) this;
        }

        public Criteria andVoltageEqualTo(String value) {
            addCriterion("voltage =", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageNotEqualTo(String value) {
            addCriterion("voltage <>", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageGreaterThan(String value) {
            addCriterion("voltage >", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageGreaterThanOrEqualTo(String value) {
            addCriterion("voltage >=", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageLessThan(String value) {
            addCriterion("voltage <", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageLessThanOrEqualTo(String value) {
            addCriterion("voltage <=", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageLike(String value) {
            addCriterion("voltage like", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageNotLike(String value) {
            addCriterion("voltage not like", value, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageIn(List<String> values) {
            addCriterion("voltage in", values, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageNotIn(List<String> values) {
            addCriterion("voltage not in", values, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageBetween(String value1, String value2) {
            addCriterion("voltage between", value1, value2, "voltage");
            return (Criteria) this;
        }

        public Criteria andVoltageNotBetween(String value1, String value2) {
            addCriterion("voltage not between", value1, value2, "voltage");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentIsNull() {
            addCriterion("electric_current is null");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentIsNotNull() {
            addCriterion("electric_current is not null");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentEqualTo(String value) {
            addCriterion("electric_current =", value, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentNotEqualTo(String value) {
            addCriterion("electric_current <>", value, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentGreaterThan(String value) {
            addCriterion("electric_current >", value, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentGreaterThanOrEqualTo(String value) {
            addCriterion("electric_current >=", value, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentLessThan(String value) {
            addCriterion("electric_current <", value, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentLessThanOrEqualTo(String value) {
            addCriterion("electric_current <=", value, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentLike(String value) {
            addCriterion("electric_current like", value, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentNotLike(String value) {
            addCriterion("electric_current not like", value, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentIn(List<String> values) {
            addCriterion("electric_current in", values, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentNotIn(List<String> values) {
            addCriterion("electric_current not in", values, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentBetween(String value1, String value2) {
            addCriterion("electric_current between", value1, value2, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andElectricCurrentNotBetween(String value1, String value2) {
            addCriterion("electric_current not between", value1, value2, "electricCurrent");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(String value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(String value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(String value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(String value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(String value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(String value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLike(String value) {
            addCriterion("start_time like", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotLike(String value) {
            addCriterion("start_time not like", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<String> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<String> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(String value1, String value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(String value1, String value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(String value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(String value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(String value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(String value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(String value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(String value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLike(String value) {
            addCriterion("end_time like", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotLike(String value) {
            addCriterion("end_time not like", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<String> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<String> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(String value1, String value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(String value1, String value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andRemainMinIsNull() {
            addCriterion("remain_min is null");
            return (Criteria) this;
        }

        public Criteria andRemainMinIsNotNull() {
            addCriterion("remain_min is not null");
            return (Criteria) this;
        }

        public Criteria andRemainMinEqualTo(String value) {
            addCriterion("remain_min =", value, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinNotEqualTo(String value) {
            addCriterion("remain_min <>", value, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinGreaterThan(String value) {
            addCriterion("remain_min >", value, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinGreaterThanOrEqualTo(String value) {
            addCriterion("remain_min >=", value, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinLessThan(String value) {
            addCriterion("remain_min <", value, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinLessThanOrEqualTo(String value) {
            addCriterion("remain_min <=", value, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinLike(String value) {
            addCriterion("remain_min like", value, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinNotLike(String value) {
            addCriterion("remain_min not like", value, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinIn(List<String> values) {
            addCriterion("remain_min in", values, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinNotIn(List<String> values) {
            addCriterion("remain_min not in", values, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinBetween(String value1, String value2) {
            addCriterion("remain_min between", value1, value2, "remainMin");
            return (Criteria) this;
        }

        public Criteria andRemainMinNotBetween(String value1, String value2) {
            addCriterion("remain_min not between", value1, value2, "remainMin");
            return (Criteria) this;
        }

        public Criteria andGenerationStartIsNull() {
            addCriterion("generation_start is null");
            return (Criteria) this;
        }

        public Criteria andGenerationStartIsNotNull() {
            addCriterion("generation_start is not null");
            return (Criteria) this;
        }

        public Criteria andGenerationStartEqualTo(String value) {
            addCriterion("generation_start =", value, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartNotEqualTo(String value) {
            addCriterion("generation_start <>", value, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartGreaterThan(String value) {
            addCriterion("generation_start >", value, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartGreaterThanOrEqualTo(String value) {
            addCriterion("generation_start >=", value, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartLessThan(String value) {
            addCriterion("generation_start <", value, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartLessThanOrEqualTo(String value) {
            addCriterion("generation_start <=", value, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartLike(String value) {
            addCriterion("generation_start like", value, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartNotLike(String value) {
            addCriterion("generation_start not like", value, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartIn(List<String> values) {
            addCriterion("generation_start in", values, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartNotIn(List<String> values) {
            addCriterion("generation_start not in", values, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartBetween(String value1, String value2) {
            addCriterion("generation_start between", value1, value2, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationStartNotBetween(String value1, String value2) {
            addCriterion("generation_start not between", value1, value2, "generationStart");
            return (Criteria) this;
        }

        public Criteria andGenerationEndIsNull() {
            addCriterion("generation_end is null");
            return (Criteria) this;
        }

        public Criteria andGenerationEndIsNotNull() {
            addCriterion("generation_end is not null");
            return (Criteria) this;
        }

        public Criteria andGenerationEndEqualTo(String value) {
            addCriterion("generation_end =", value, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndNotEqualTo(String value) {
            addCriterion("generation_end <>", value, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndGreaterThan(String value) {
            addCriterion("generation_end >", value, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndGreaterThanOrEqualTo(String value) {
            addCriterion("generation_end >=", value, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndLessThan(String value) {
            addCriterion("generation_end <", value, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndLessThanOrEqualTo(String value) {
            addCriterion("generation_end <=", value, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndLike(String value) {
            addCriterion("generation_end like", value, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndNotLike(String value) {
            addCriterion("generation_end not like", value, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndIn(List<String> values) {
            addCriterion("generation_end in", values, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndNotIn(List<String> values) {
            addCriterion("generation_end not in", values, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndBetween(String value1, String value2) {
            addCriterion("generation_end between", value1, value2, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationEndNotBetween(String value1, String value2) {
            addCriterion("generation_end not between", value1, value2, "generationEnd");
            return (Criteria) this;
        }

        public Criteria andGenerationMinIsNull() {
            addCriterion("generation_min is null");
            return (Criteria) this;
        }

        public Criteria andGenerationMinIsNotNull() {
            addCriterion("generation_min is not null");
            return (Criteria) this;
        }

        public Criteria andGenerationMinEqualTo(String value) {
            addCriterion("generation_min =", value, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinNotEqualTo(String value) {
            addCriterion("generation_min <>", value, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinGreaterThan(String value) {
            addCriterion("generation_min >", value, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinGreaterThanOrEqualTo(String value) {
            addCriterion("generation_min >=", value, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinLessThan(String value) {
            addCriterion("generation_min <", value, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinLessThanOrEqualTo(String value) {
            addCriterion("generation_min <=", value, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinLike(String value) {
            addCriterion("generation_min like", value, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinNotLike(String value) {
            addCriterion("generation_min not like", value, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinIn(List<String> values) {
            addCriterion("generation_min in", values, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinNotIn(List<String> values) {
            addCriterion("generation_min not in", values, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinBetween(String value1, String value2) {
            addCriterion("generation_min between", value1, value2, "generationMin");
            return (Criteria) this;
        }

        public Criteria andGenerationMinNotBetween(String value1, String value2) {
            addCriterion("generation_min not between", value1, value2, "generationMin");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeIsNull() {
            addCriterion("breakdown_time is null");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeIsNotNull() {
            addCriterion("breakdown_time is not null");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeEqualTo(String value) {
            addCriterion("breakdown_time =", value, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeNotEqualTo(String value) {
            addCriterion("breakdown_time <>", value, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeGreaterThan(String value) {
            addCriterion("breakdown_time >", value, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeGreaterThanOrEqualTo(String value) {
            addCriterion("breakdown_time >=", value, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeLessThan(String value) {
            addCriterion("breakdown_time <", value, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeLessThanOrEqualTo(String value) {
            addCriterion("breakdown_time <=", value, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeLike(String value) {
            addCriterion("breakdown_time like", value, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeNotLike(String value) {
            addCriterion("breakdown_time not like", value, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeIn(List<String> values) {
            addCriterion("breakdown_time in", values, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeNotIn(List<String> values) {
            addCriterion("breakdown_time not in", values, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeBetween(String value1, String value2) {
            addCriterion("breakdown_time between", value1, value2, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownTimeNotBetween(String value1, String value2) {
            addCriterion("breakdown_time not between", value1, value2, "breakdownTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongIsNull() {
            addCriterion("breakdown_long is null");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongIsNotNull() {
            addCriterion("breakdown_long is not null");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongEqualTo(String value) {
            addCriterion("breakdown_long =", value, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongNotEqualTo(String value) {
            addCriterion("breakdown_long <>", value, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongGreaterThan(String value) {
            addCriterion("breakdown_long >", value, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongGreaterThanOrEqualTo(String value) {
            addCriterion("breakdown_long >=", value, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongLessThan(String value) {
            addCriterion("breakdown_long <", value, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongLessThanOrEqualTo(String value) {
            addCriterion("breakdown_long <=", value, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongLike(String value) {
            addCriterion("breakdown_long like", value, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongNotLike(String value) {
            addCriterion("breakdown_long not like", value, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongIn(List<String> values) {
            addCriterion("breakdown_long in", values, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongNotIn(List<String> values) {
            addCriterion("breakdown_long not in", values, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongBetween(String value1, String value2) {
            addCriterion("breakdown_long between", value1, value2, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andBreakdownLongNotBetween(String value1, String value2) {
            addCriterion("breakdown_long not between", value1, value2, "breakdownLong");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeIsNull() {
            addCriterion("dispatch_time is null");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeIsNotNull() {
            addCriterion("dispatch_time is not null");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeEqualTo(String value) {
            addCriterion("dispatch_time =", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeNotEqualTo(String value) {
            addCriterion("dispatch_time <>", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeGreaterThan(String value) {
            addCriterion("dispatch_time >", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeGreaterThanOrEqualTo(String value) {
            addCriterion("dispatch_time >=", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeLessThan(String value) {
            addCriterion("dispatch_time <", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeLessThanOrEqualTo(String value) {
            addCriterion("dispatch_time <=", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeLike(String value) {
            addCriterion("dispatch_time like", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeNotLike(String value) {
            addCriterion("dispatch_time not like", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeIn(List<String> values) {
            addCriterion("dispatch_time in", values, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeNotIn(List<String> values) {
            addCriterion("dispatch_time not in", values, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeBetween(String value1, String value2) {
            addCriterion("dispatch_time between", value1, value2, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeNotBetween(String value1, String value2) {
            addCriterion("dispatch_time not between", value1, value2, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andBillCostIsNull() {
            addCriterion("bill_cost is null");
            return (Criteria) this;
        }

        public Criteria andBillCostIsNotNull() {
            addCriterion("bill_cost is not null");
            return (Criteria) this;
        }

        public Criteria andBillCostEqualTo(String value) {
            addCriterion("bill_cost =", value, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostNotEqualTo(String value) {
            addCriterion("bill_cost <>", value, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostGreaterThan(String value) {
            addCriterion("bill_cost >", value, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostGreaterThanOrEqualTo(String value) {
            addCriterion("bill_cost >=", value, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostLessThan(String value) {
            addCriterion("bill_cost <", value, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostLessThanOrEqualTo(String value) {
            addCriterion("bill_cost <=", value, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostLike(String value) {
            addCriterion("bill_cost like", value, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostNotLike(String value) {
            addCriterion("bill_cost not like", value, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostIn(List<String> values) {
            addCriterion("bill_cost in", values, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostNotIn(List<String> values) {
            addCriterion("bill_cost not in", values, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostBetween(String value1, String value2) {
            addCriterion("bill_cost between", value1, value2, "billCost");
            return (Criteria) this;
        }

        public Criteria andBillCostNotBetween(String value1, String value2) {
            addCriterion("bill_cost not between", value1, value2, "billCost");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameIsNull() {
            addCriterion("operators_name is null");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameIsNotNull() {
            addCriterion("operators_name is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameEqualTo(String value) {
            addCriterion("operators_name =", value, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameNotEqualTo(String value) {
            addCriterion("operators_name <>", value, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameGreaterThan(String value) {
            addCriterion("operators_name >", value, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameGreaterThanOrEqualTo(String value) {
            addCriterion("operators_name >=", value, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameLessThan(String value) {
            addCriterion("operators_name <", value, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameLessThanOrEqualTo(String value) {
            addCriterion("operators_name <=", value, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameLike(String value) {
            addCriterion("operators_name like", value, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameNotLike(String value) {
            addCriterion("operators_name not like", value, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameIn(List<String> values) {
            addCriterion("operators_name in", values, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameNotIn(List<String> values) {
            addCriterion("operators_name not in", values, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameBetween(String value1, String value2) {
            addCriterion("operators_name between", value1, value2, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andOperatorsNameNotBetween(String value1, String value2) {
            addCriterion("operators_name not between", value1, value2, "operatorsName");
            return (Criteria) this;
        }

        public Criteria andMobileLevelIsNull() {
            addCriterion("mobile_level is null");
            return (Criteria) this;
        }

        public Criteria andMobileLevelIsNotNull() {
            addCriterion("mobile_level is not null");
            return (Criteria) this;
        }

        public Criteria andMobileLevelEqualTo(String value) {
            addCriterion("mobile_level =", value, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelNotEqualTo(String value) {
            addCriterion("mobile_level <>", value, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelGreaterThan(String value) {
            addCriterion("mobile_level >", value, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelGreaterThanOrEqualTo(String value) {
            addCriterion("mobile_level >=", value, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelLessThan(String value) {
            addCriterion("mobile_level <", value, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelLessThanOrEqualTo(String value) {
            addCriterion("mobile_level <=", value, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelLike(String value) {
            addCriterion("mobile_level like", value, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelNotLike(String value) {
            addCriterion("mobile_level not like", value, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelIn(List<String> values) {
            addCriterion("mobile_level in", values, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelNotIn(List<String> values) {
            addCriterion("mobile_level not in", values, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelBetween(String value1, String value2) {
            addCriterion("mobile_level between", value1, value2, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andMobileLevelNotBetween(String value1, String value2) {
            addCriterion("mobile_level not between", value1, value2, "mobileLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelIsNull() {
            addCriterion("unicom_level is null");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelIsNotNull() {
            addCriterion("unicom_level is not null");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelEqualTo(String value) {
            addCriterion("unicom_level =", value, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelNotEqualTo(String value) {
            addCriterion("unicom_level <>", value, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelGreaterThan(String value) {
            addCriterion("unicom_level >", value, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelGreaterThanOrEqualTo(String value) {
            addCriterion("unicom_level >=", value, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelLessThan(String value) {
            addCriterion("unicom_level <", value, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelLessThanOrEqualTo(String value) {
            addCriterion("unicom_level <=", value, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelLike(String value) {
            addCriterion("unicom_level like", value, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelNotLike(String value) {
            addCriterion("unicom_level not like", value, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelIn(List<String> values) {
            addCriterion("unicom_level in", values, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelNotIn(List<String> values) {
            addCriterion("unicom_level not in", values, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelBetween(String value1, String value2) {
            addCriterion("unicom_level between", value1, value2, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andUnicomLevelNotBetween(String value1, String value2) {
            addCriterion("unicom_level not between", value1, value2, "unicomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelIsNull() {
            addCriterion("telecom_level is null");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelIsNotNull() {
            addCriterion("telecom_level is not null");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelEqualTo(String value) {
            addCriterion("telecom_level =", value, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelNotEqualTo(String value) {
            addCriterion("telecom_level <>", value, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelGreaterThan(String value) {
            addCriterion("telecom_level >", value, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelGreaterThanOrEqualTo(String value) {
            addCriterion("telecom_level >=", value, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelLessThan(String value) {
            addCriterion("telecom_level <", value, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelLessThanOrEqualTo(String value) {
            addCriterion("telecom_level <=", value, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelLike(String value) {
            addCriterion("telecom_level like", value, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelNotLike(String value) {
            addCriterion("telecom_level not like", value, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelIn(List<String> values) {
            addCriterion("telecom_level in", values, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelNotIn(List<String> values) {
            addCriterion("telecom_level not in", values, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelBetween(String value1, String value2) {
            addCriterion("telecom_level between", value1, value2, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andTelecomLevelNotBetween(String value1, String value2) {
            addCriterion("telecom_level not between", value1, value2, "telecomLevel");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenIsNull() {
            addCriterion("buy_power_gen is null");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenIsNotNull() {
            addCriterion("buy_power_gen is not null");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenEqualTo(String value) {
            addCriterion("buy_power_gen =", value, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenNotEqualTo(String value) {
            addCriterion("buy_power_gen <>", value, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenGreaterThan(String value) {
            addCriterion("buy_power_gen >", value, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenGreaterThanOrEqualTo(String value) {
            addCriterion("buy_power_gen >=", value, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenLessThan(String value) {
            addCriterion("buy_power_gen <", value, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenLessThanOrEqualTo(String value) {
            addCriterion("buy_power_gen <=", value, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenLike(String value) {
            addCriterion("buy_power_gen like", value, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenNotLike(String value) {
            addCriterion("buy_power_gen not like", value, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenIn(List<String> values) {
            addCriterion("buy_power_gen in", values, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenNotIn(List<String> values) {
            addCriterion("buy_power_gen not in", values, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenBetween(String value1, String value2) {
            addCriterion("buy_power_gen between", value1, value2, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andBuyPowerGenNotBetween(String value1, String value2) {
            addCriterion("buy_power_gen not between", value1, value2, "buyPowerGen");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptIsNull() {
            addCriterion("operators_accept is null");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptIsNotNull() {
            addCriterion("operators_accept is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptEqualTo(String value) {
            addCriterion("operators_accept =", value, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptNotEqualTo(String value) {
            addCriterion("operators_accept <>", value, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptGreaterThan(String value) {
            addCriterion("operators_accept >", value, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptGreaterThanOrEqualTo(String value) {
            addCriterion("operators_accept >=", value, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptLessThan(String value) {
            addCriterion("operators_accept <", value, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptLessThanOrEqualTo(String value) {
            addCriterion("operators_accept <=", value, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptLike(String value) {
            addCriterion("operators_accept like", value, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptNotLike(String value) {
            addCriterion("operators_accept not like", value, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptIn(List<String> values) {
            addCriterion("operators_accept in", values, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptNotIn(List<String> values) {
            addCriterion("operators_accept not in", values, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptBetween(String value1, String value2) {
            addCriterion("operators_accept between", value1, value2, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andOperatorsAcceptNotBetween(String value1, String value2) {
            addCriterion("operators_accept not between", value1, value2, "operatorsAccept");
            return (Criteria) this;
        }

        public Criteria andServicingComIsNull() {
            addCriterion("servicing_com is null");
            return (Criteria) this;
        }

        public Criteria andServicingComIsNotNull() {
            addCriterion("servicing_com is not null");
            return (Criteria) this;
        }

        public Criteria andServicingComEqualTo(String value) {
            addCriterion("servicing_com =", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComNotEqualTo(String value) {
            addCriterion("servicing_com <>", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComGreaterThan(String value) {
            addCriterion("servicing_com >", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComGreaterThanOrEqualTo(String value) {
            addCriterion("servicing_com >=", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComLessThan(String value) {
            addCriterion("servicing_com <", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComLessThanOrEqualTo(String value) {
            addCriterion("servicing_com <=", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComLike(String value) {
            addCriterion("servicing_com like", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComNotLike(String value) {
            addCriterion("servicing_com not like", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComIn(List<String> values) {
            addCriterion("servicing_com in", values, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComNotIn(List<String> values) {
            addCriterion("servicing_com not in", values, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComBetween(String value1, String value2) {
            addCriterion("servicing_com between", value1, value2, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComNotBetween(String value1, String value2) {
            addCriterion("servicing_com not between", value1, value2, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andIsGetValIsNull() {
            addCriterion("is_get_val is null");
            return (Criteria) this;
        }

        public Criteria andIsGetValIsNotNull() {
            addCriterion("is_get_val is not null");
            return (Criteria) this;
        }

        public Criteria andIsGetValEqualTo(String value) {
            addCriterion("is_get_val =", value, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValNotEqualTo(String value) {
            addCriterion("is_get_val <>", value, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValGreaterThan(String value) {
            addCriterion("is_get_val >", value, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValGreaterThanOrEqualTo(String value) {
            addCriterion("is_get_val >=", value, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValLessThan(String value) {
            addCriterion("is_get_val <", value, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValLessThanOrEqualTo(String value) {
            addCriterion("is_get_val <=", value, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValLike(String value) {
            addCriterion("is_get_val like", value, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValNotLike(String value) {
            addCriterion("is_get_val not like", value, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValIn(List<String> values) {
            addCriterion("is_get_val in", values, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValNotIn(List<String> values) {
            addCriterion("is_get_val not in", values, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValBetween(String value1, String value2) {
            addCriterion("is_get_val between", value1, value2, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andIsGetValNotBetween(String value1, String value2) {
            addCriterion("is_get_val not between", value1, value2, "isGetVal");
            return (Criteria) this;
        }

        public Criteria andRequirementIsNull() {
            addCriterion("requirement is null");
            return (Criteria) this;
        }

        public Criteria andRequirementIsNotNull() {
            addCriterion("requirement is not null");
            return (Criteria) this;
        }

        public Criteria andRequirementEqualTo(String value) {
            addCriterion("requirement =", value, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementNotEqualTo(String value) {
            addCriterion("requirement <>", value, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementGreaterThan(String value) {
            addCriterion("requirement >", value, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementGreaterThanOrEqualTo(String value) {
            addCriterion("requirement >=", value, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementLessThan(String value) {
            addCriterion("requirement <", value, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementLessThanOrEqualTo(String value) {
            addCriterion("requirement <=", value, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementLike(String value) {
            addCriterion("requirement like", value, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementNotLike(String value) {
            addCriterion("requirement not like", value, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementIn(List<String> values) {
            addCriterion("requirement in", values, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementNotIn(List<String> values) {
            addCriterion("requirement not in", values, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementBetween(String value1, String value2) {
            addCriterion("requirement between", value1, value2, "requirement");
            return (Criteria) this;
        }

        public Criteria andRequirementNotBetween(String value1, String value2) {
            addCriterion("requirement not between", value1, value2, "requirement");
            return (Criteria) this;
        }

        public Criteria andDispatchLongIsNull() {
            addCriterion("dispatch_long is null");
            return (Criteria) this;
        }

        public Criteria andDispatchLongIsNotNull() {
            addCriterion("dispatch_long is not null");
            return (Criteria) this;
        }

        public Criteria andDispatchLongEqualTo(String value) {
            addCriterion("dispatch_long =", value, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongNotEqualTo(String value) {
            addCriterion("dispatch_long <>", value, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongGreaterThan(String value) {
            addCriterion("dispatch_long >", value, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongGreaterThanOrEqualTo(String value) {
            addCriterion("dispatch_long >=", value, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongLessThan(String value) {
            addCriterion("dispatch_long <", value, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongLessThanOrEqualTo(String value) {
            addCriterion("dispatch_long <=", value, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongLike(String value) {
            addCriterion("dispatch_long like", value, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongNotLike(String value) {
            addCriterion("dispatch_long not like", value, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongIn(List<String> values) {
            addCriterion("dispatch_long in", values, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongNotIn(List<String> values) {
            addCriterion("dispatch_long not in", values, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongBetween(String value1, String value2) {
            addCriterion("dispatch_long between", value1, value2, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andDispatchLongNotBetween(String value1, String value2) {
            addCriterion("dispatch_long not between", value1, value2, "dispatchLong");
            return (Criteria) this;
        }

        public Criteria andCormLongIsNull() {
            addCriterion("corm_long is null");
            return (Criteria) this;
        }

        public Criteria andCormLongIsNotNull() {
            addCriterion("corm_long is not null");
            return (Criteria) this;
        }

        public Criteria andCormLongEqualTo(String value) {
            addCriterion("corm_long =", value, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongNotEqualTo(String value) {
            addCriterion("corm_long <>", value, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongGreaterThan(String value) {
            addCriterion("corm_long >", value, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongGreaterThanOrEqualTo(String value) {
            addCriterion("corm_long >=", value, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongLessThan(String value) {
            addCriterion("corm_long <", value, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongLessThanOrEqualTo(String value) {
            addCriterion("corm_long <=", value, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongLike(String value) {
            addCriterion("corm_long like", value, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongNotLike(String value) {
            addCriterion("corm_long not like", value, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongIn(List<String> values) {
            addCriterion("corm_long in", values, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongNotIn(List<String> values) {
            addCriterion("corm_long not in", values, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongBetween(String value1, String value2) {
            addCriterion("corm_long between", value1, value2, "cormLong");
            return (Criteria) this;
        }

        public Criteria andCormLongNotBetween(String value1, String value2) {
            addCriterion("corm_long not between", value1, value2, "cormLong");
            return (Criteria) this;
        }

        public Criteria andGetinIsNull() {
            addCriterion("getin is null");
            return (Criteria) this;
        }

        public Criteria andGetinIsNotNull() {
            addCriterion("getin is not null");
            return (Criteria) this;
        }

        public Criteria andGetinEqualTo(String value) {
            addCriterion("getin =", value, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinNotEqualTo(String value) {
            addCriterion("getin <>", value, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinGreaterThan(String value) {
            addCriterion("getin >", value, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinGreaterThanOrEqualTo(String value) {
            addCriterion("getin >=", value, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinLessThan(String value) {
            addCriterion("getin <", value, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinLessThanOrEqualTo(String value) {
            addCriterion("getin <=", value, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinLike(String value) {
            addCriterion("getin like", value, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinNotLike(String value) {
            addCriterion("getin not like", value, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinIn(List<String> values) {
            addCriterion("getin in", values, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinNotIn(List<String> values) {
            addCriterion("getin not in", values, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinBetween(String value1, String value2) {
            addCriterion("getin between", value1, value2, "getin");
            return (Criteria) this;
        }

        public Criteria andGetinNotBetween(String value1, String value2) {
            addCriterion("getin not between", value1, value2, "getin");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("`operator` is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("`operator` is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(String value) {
            addCriterion("`operator` =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(String value) {
            addCriterion("`operator` <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(String value) {
            addCriterion("`operator` >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(String value) {
            addCriterion("`operator` >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(String value) {
            addCriterion("`operator` <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(String value) {
            addCriterion("`operator` <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLike(String value) {
            addCriterion("`operator` like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotLike(String value) {
            addCriterion("`operator` not like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<String> values) {
            addCriterion("`operator` in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<String> values) {
            addCriterion("`operator` not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(String value1, String value2) {
            addCriterion("`operator` between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(String value1, String value2) {
            addCriterion("`operator` not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNull() {
            addCriterion("create_by is null");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNotNull() {
            addCriterion("create_by is not null");
            return (Criteria) this;
        }

        public Criteria andCreateByEqualTo(String value) {
            addCriterion("create_by =", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotEqualTo(String value) {
            addCriterion("create_by <>", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThan(String value) {
            addCriterion("create_by >", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThanOrEqualTo(String value) {
            addCriterion("create_by >=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThan(String value) {
            addCriterion("create_by <", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThanOrEqualTo(String value) {
            addCriterion("create_by <=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLike(String value) {
            addCriterion("create_by like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotLike(String value) {
            addCriterion("create_by not like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByIn(List<String> values) {
            addCriterion("create_by in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotIn(List<String> values) {
            addCriterion("create_by not in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByBetween(String value1, String value2) {
            addCriterion("create_by between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotBetween(String value1, String value2) {
            addCriterion("create_by not between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNull() {
            addCriterion("update_by is null");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNotNull() {
            addCriterion("update_by is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateByEqualTo(String value) {
            addCriterion("update_by =", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotEqualTo(String value) {
            addCriterion("update_by <>", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThan(String value) {
            addCriterion("update_by >", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThanOrEqualTo(String value) {
            addCriterion("update_by >=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThan(String value) {
            addCriterion("update_by <", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThanOrEqualTo(String value) {
            addCriterion("update_by <=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLike(String value) {
            addCriterion("update_by like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotLike(String value) {
            addCriterion("update_by not like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIn(List<String> values) {
            addCriterion("update_by in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotIn(List<String> values) {
            addCriterion("update_by not in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByBetween(String value1, String value2) {
            addCriterion("update_by between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotBetween(String value1, String value2) {
            addCriterion("update_by not between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("remarks is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("remarks is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("remarks =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("remarks <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("remarks >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("remarks >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("remarks <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("remarks <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("remarks like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("remarks not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("remarks in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("remarks not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("remarks between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("remarks not between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("`status` is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("`status` is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("`status` =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("`status` <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("`status` >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("`status` >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("`status` <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("`status` <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("`status` like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("`status` not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("`status` in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("`status` not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("`status` between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("`status` not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}