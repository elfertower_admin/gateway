package com.eat.gateway.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClPowerGenBillExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ClPowerGenBillExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPowerGenIdIsNull() {
            addCriterion("power_gen_id is null");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdIsNotNull() {
            addCriterion("power_gen_id is not null");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdEqualTo(String value) {
            addCriterion("power_gen_id =", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdNotEqualTo(String value) {
            addCriterion("power_gen_id <>", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdGreaterThan(String value) {
            addCriterion("power_gen_id >", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdGreaterThanOrEqualTo(String value) {
            addCriterion("power_gen_id >=", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdLessThan(String value) {
            addCriterion("power_gen_id <", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdLessThanOrEqualTo(String value) {
            addCriterion("power_gen_id <=", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdLike(String value) {
            addCriterion("power_gen_id like", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdNotLike(String value) {
            addCriterion("power_gen_id not like", value, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdIn(List<String> values) {
            addCriterion("power_gen_id in", values, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdNotIn(List<String> values) {
            addCriterion("power_gen_id not in", values, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdBetween(String value1, String value2) {
            addCriterion("power_gen_id between", value1, value2, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andPowerGenIdNotBetween(String value1, String value2) {
            addCriterion("power_gen_id not between", value1, value2, "powerGenId");
            return (Criteria) this;
        }

        public Criteria andDetailIdIsNull() {
            addCriterion("detail_id is null");
            return (Criteria) this;
        }

        public Criteria andDetailIdIsNotNull() {
            addCriterion("detail_id is not null");
            return (Criteria) this;
        }

        public Criteria andDetailIdEqualTo(String value) {
            addCriterion("detail_id =", value, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdNotEqualTo(String value) {
            addCriterion("detail_id <>", value, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdGreaterThan(String value) {
            addCriterion("detail_id >", value, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdGreaterThanOrEqualTo(String value) {
            addCriterion("detail_id >=", value, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdLessThan(String value) {
            addCriterion("detail_id <", value, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdLessThanOrEqualTo(String value) {
            addCriterion("detail_id <=", value, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdLike(String value) {
            addCriterion("detail_id like", value, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdNotLike(String value) {
            addCriterion("detail_id not like", value, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdIn(List<String> values) {
            addCriterion("detail_id in", values, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdNotIn(List<String> values) {
            addCriterion("detail_id not in", values, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdBetween(String value1, String value2) {
            addCriterion("detail_id between", value1, value2, "detailId");
            return (Criteria) this;
        }

        public Criteria andDetailIdNotBetween(String value1, String value2) {
            addCriterion("detail_id not between", value1, value2, "detailId");
            return (Criteria) this;
        }

        public Criteria andBillCodeIsNull() {
            addCriterion("bill_code is null");
            return (Criteria) this;
        }

        public Criteria andBillCodeIsNotNull() {
            addCriterion("bill_code is not null");
            return (Criteria) this;
        }

        public Criteria andBillCodeEqualTo(String value) {
            addCriterion("bill_code =", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotEqualTo(String value) {
            addCriterion("bill_code <>", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeGreaterThan(String value) {
            addCriterion("bill_code >", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeGreaterThanOrEqualTo(String value) {
            addCriterion("bill_code >=", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLessThan(String value) {
            addCriterion("bill_code <", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLessThanOrEqualTo(String value) {
            addCriterion("bill_code <=", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLike(String value) {
            addCriterion("bill_code like", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotLike(String value) {
            addCriterion("bill_code not like", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeIn(List<String> values) {
            addCriterion("bill_code in", values, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotIn(List<String> values) {
            addCriterion("bill_code not in", values, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeBetween(String value1, String value2) {
            addCriterion("bill_code between", value1, value2, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotBetween(String value1, String value2) {
            addCriterion("bill_code not between", value1, value2, "billCode");
            return (Criteria) this;
        }

        public Criteria andShadowIdIsNull() {
            addCriterion("shadow_id is null");
            return (Criteria) this;
        }

        public Criteria andShadowIdIsNotNull() {
            addCriterion("shadow_id is not null");
            return (Criteria) this;
        }

        public Criteria andShadowIdEqualTo(String value) {
            addCriterion("shadow_id =", value, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdNotEqualTo(String value) {
            addCriterion("shadow_id <>", value, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdGreaterThan(String value) {
            addCriterion("shadow_id >", value, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdGreaterThanOrEqualTo(String value) {
            addCriterion("shadow_id >=", value, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdLessThan(String value) {
            addCriterion("shadow_id <", value, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdLessThanOrEqualTo(String value) {
            addCriterion("shadow_id <=", value, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdLike(String value) {
            addCriterion("shadow_id like", value, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdNotLike(String value) {
            addCriterion("shadow_id not like", value, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdIn(List<String> values) {
            addCriterion("shadow_id in", values, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdNotIn(List<String> values) {
            addCriterion("shadow_id not in", values, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdBetween(String value1, String value2) {
            addCriterion("shadow_id between", value1, value2, "shadowId");
            return (Criteria) this;
        }

        public Criteria andShadowIdNotBetween(String value1, String value2) {
            addCriterion("shadow_id not between", value1, value2, "shadowId");
            return (Criteria) this;
        }

        public Criteria andStationIdIsNull() {
            addCriterion("station_id is null");
            return (Criteria) this;
        }

        public Criteria andStationIdIsNotNull() {
            addCriterion("station_id is not null");
            return (Criteria) this;
        }

        public Criteria andStationIdEqualTo(String value) {
            addCriterion("station_id =", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdNotEqualTo(String value) {
            addCriterion("station_id <>", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdGreaterThan(String value) {
            addCriterion("station_id >", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdGreaterThanOrEqualTo(String value) {
            addCriterion("station_id >=", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdLessThan(String value) {
            addCriterion("station_id <", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdLessThanOrEqualTo(String value) {
            addCriterion("station_id <=", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdLike(String value) {
            addCriterion("station_id like", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdNotLike(String value) {
            addCriterion("station_id not like", value, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdIn(List<String> values) {
            addCriterion("station_id in", values, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdNotIn(List<String> values) {
            addCriterion("station_id not in", values, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdBetween(String value1, String value2) {
            addCriterion("station_id between", value1, value2, "stationId");
            return (Criteria) this;
        }

        public Criteria andStationIdNotBetween(String value1, String value2) {
            addCriterion("station_id not between", value1, value2, "stationId");
            return (Criteria) this;
        }

        public Criteria andUserCodeIsNull() {
            addCriterion("user_code is null");
            return (Criteria) this;
        }

        public Criteria andUserCodeIsNotNull() {
            addCriterion("user_code is not null");
            return (Criteria) this;
        }

        public Criteria andUserCodeEqualTo(String value) {
            addCriterion("user_code =", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotEqualTo(String value) {
            addCriterion("user_code <>", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeGreaterThan(String value) {
            addCriterion("user_code >", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeGreaterThanOrEqualTo(String value) {
            addCriterion("user_code >=", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLessThan(String value) {
            addCriterion("user_code <", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLessThanOrEqualTo(String value) {
            addCriterion("user_code <=", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLike(String value) {
            addCriterion("user_code like", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotLike(String value) {
            addCriterion("user_code not like", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeIn(List<String> values) {
            addCriterion("user_code in", values, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotIn(List<String> values) {
            addCriterion("user_code not in", values, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeBetween(String value1, String value2) {
            addCriterion("user_code between", value1, value2, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotBetween(String value1, String value2) {
            addCriterion("user_code not between", value1, value2, "userCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeIsNull() {
            addCriterion("office_code is null");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeIsNotNull() {
            addCriterion("office_code is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeEqualTo(String value) {
            addCriterion("office_code =", value, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeNotEqualTo(String value) {
            addCriterion("office_code <>", value, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeGreaterThan(String value) {
            addCriterion("office_code >", value, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("office_code >=", value, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeLessThan(String value) {
            addCriterion("office_code <", value, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeLessThanOrEqualTo(String value) {
            addCriterion("office_code <=", value, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeLike(String value) {
            addCriterion("office_code like", value, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeNotLike(String value) {
            addCriterion("office_code not like", value, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeIn(List<String> values) {
            addCriterion("office_code in", values, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeNotIn(List<String> values) {
            addCriterion("office_code not in", values, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeBetween(String value1, String value2) {
            addCriterion("office_code between", value1, value2, "officeCode");
            return (Criteria) this;
        }

        public Criteria andOfficeCodeNotBetween(String value1, String value2) {
            addCriterion("office_code not between", value1, value2, "officeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeIsNull() {
            addCriterion("parent_office_code is null");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeIsNotNull() {
            addCriterion("parent_office_code is not null");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeEqualTo(String value) {
            addCriterion("parent_office_code =", value, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeNotEqualTo(String value) {
            addCriterion("parent_office_code <>", value, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeGreaterThan(String value) {
            addCriterion("parent_office_code >", value, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("parent_office_code >=", value, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeLessThan(String value) {
            addCriterion("parent_office_code <", value, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeLessThanOrEqualTo(String value) {
            addCriterion("parent_office_code <=", value, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeLike(String value) {
            addCriterion("parent_office_code like", value, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeNotLike(String value) {
            addCriterion("parent_office_code not like", value, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeIn(List<String> values) {
            addCriterion("parent_office_code in", values, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeNotIn(List<String> values) {
            addCriterion("parent_office_code not in", values, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeBetween(String value1, String value2) {
            addCriterion("parent_office_code between", value1, value2, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andParentOfficeCodeNotBetween(String value1, String value2) {
            addCriterion("parent_office_code not between", value1, value2, "parentOfficeCode");
            return (Criteria) this;
        }

        public Criteria andStationLevelIsNull() {
            addCriterion("station_level is null");
            return (Criteria) this;
        }

        public Criteria andStationLevelIsNotNull() {
            addCriterion("station_level is not null");
            return (Criteria) this;
        }

        public Criteria andStationLevelEqualTo(String value) {
            addCriterion("station_level =", value, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelNotEqualTo(String value) {
            addCriterion("station_level <>", value, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelGreaterThan(String value) {
            addCriterion("station_level >", value, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelGreaterThanOrEqualTo(String value) {
            addCriterion("station_level >=", value, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelLessThan(String value) {
            addCriterion("station_level <", value, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelLessThanOrEqualTo(String value) {
            addCriterion("station_level <=", value, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelLike(String value) {
            addCriterion("station_level like", value, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelNotLike(String value) {
            addCriterion("station_level not like", value, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelIn(List<String> values) {
            addCriterion("station_level in", values, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelNotIn(List<String> values) {
            addCriterion("station_level not in", values, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelBetween(String value1, String value2) {
            addCriterion("station_level between", value1, value2, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andStationLevelNotBetween(String value1, String value2) {
            addCriterion("station_level not between", value1, value2, "stationLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelIsNull() {
            addCriterion("is_top_level is null");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelIsNotNull() {
            addCriterion("is_top_level is not null");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelEqualTo(String value) {
            addCriterion("is_top_level =", value, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelNotEqualTo(String value) {
            addCriterion("is_top_level <>", value, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelGreaterThan(String value) {
            addCriterion("is_top_level >", value, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelGreaterThanOrEqualTo(String value) {
            addCriterion("is_top_level >=", value, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelLessThan(String value) {
            addCriterion("is_top_level <", value, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelLessThanOrEqualTo(String value) {
            addCriterion("is_top_level <=", value, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelLike(String value) {
            addCriterion("is_top_level like", value, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelNotLike(String value) {
            addCriterion("is_top_level not like", value, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelIn(List<String> values) {
            addCriterion("is_top_level in", values, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelNotIn(List<String> values) {
            addCriterion("is_top_level not in", values, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelBetween(String value1, String value2) {
            addCriterion("is_top_level between", value1, value2, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsTopLevelNotBetween(String value1, String value2) {
            addCriterion("is_top_level not between", value1, value2, "isTopLevel");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenIsNull() {
            addCriterion("is_power_gen is null");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenIsNotNull() {
            addCriterion("is_power_gen is not null");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenEqualTo(String value) {
            addCriterion("is_power_gen =", value, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenNotEqualTo(String value) {
            addCriterion("is_power_gen <>", value, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenGreaterThan(String value) {
            addCriterion("is_power_gen >", value, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenGreaterThanOrEqualTo(String value) {
            addCriterion("is_power_gen >=", value, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenLessThan(String value) {
            addCriterion("is_power_gen <", value, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenLessThanOrEqualTo(String value) {
            addCriterion("is_power_gen <=", value, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenLike(String value) {
            addCriterion("is_power_gen like", value, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenNotLike(String value) {
            addCriterion("is_power_gen not like", value, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenIn(List<String> values) {
            addCriterion("is_power_gen in", values, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenNotIn(List<String> values) {
            addCriterion("is_power_gen not in", values, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenBetween(String value1, String value2) {
            addCriterion("is_power_gen between", value1, value2, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsPowerGenNotBetween(String value1, String value2) {
            addCriterion("is_power_gen not between", value1, value2, "isPowerGen");
            return (Criteria) this;
        }

        public Criteria andIsOffLineIsNull() {
            addCriterion("is_off_line is null");
            return (Criteria) this;
        }

        public Criteria andIsOffLineIsNotNull() {
            addCriterion("is_off_line is not null");
            return (Criteria) this;
        }

        public Criteria andIsOffLineEqualTo(String value) {
            addCriterion("is_off_line =", value, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineNotEqualTo(String value) {
            addCriterion("is_off_line <>", value, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineGreaterThan(String value) {
            addCriterion("is_off_line >", value, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineGreaterThanOrEqualTo(String value) {
            addCriterion("is_off_line >=", value, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineLessThan(String value) {
            addCriterion("is_off_line <", value, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineLessThanOrEqualTo(String value) {
            addCriterion("is_off_line <=", value, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineLike(String value) {
            addCriterion("is_off_line like", value, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineNotLike(String value) {
            addCriterion("is_off_line not like", value, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineIn(List<String> values) {
            addCriterion("is_off_line in", values, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineNotIn(List<String> values) {
            addCriterion("is_off_line not in", values, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineBetween(String value1, String value2) {
            addCriterion("is_off_line between", value1, value2, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andIsOffLineNotBetween(String value1, String value2) {
            addCriterion("is_off_line not between", value1, value2, "isOffLine");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNull() {
            addCriterion("create_by is null");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNotNull() {
            addCriterion("create_by is not null");
            return (Criteria) this;
        }

        public Criteria andCreateByEqualTo(String value) {
            addCriterion("create_by =", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotEqualTo(String value) {
            addCriterion("create_by <>", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThan(String value) {
            addCriterion("create_by >", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThanOrEqualTo(String value) {
            addCriterion("create_by >=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThan(String value) {
            addCriterion("create_by <", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThanOrEqualTo(String value) {
            addCriterion("create_by <=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLike(String value) {
            addCriterion("create_by like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotLike(String value) {
            addCriterion("create_by not like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByIn(List<String> values) {
            addCriterion("create_by in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotIn(List<String> values) {
            addCriterion("create_by not in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByBetween(String value1, String value2) {
            addCriterion("create_by between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotBetween(String value1, String value2) {
            addCriterion("create_by not between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNull() {
            addCriterion("update_by is null");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNotNull() {
            addCriterion("update_by is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateByEqualTo(String value) {
            addCriterion("update_by =", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotEqualTo(String value) {
            addCriterion("update_by <>", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThan(String value) {
            addCriterion("update_by >", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThanOrEqualTo(String value) {
            addCriterion("update_by >=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThan(String value) {
            addCriterion("update_by <", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThanOrEqualTo(String value) {
            addCriterion("update_by <=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLike(String value) {
            addCriterion("update_by like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotLike(String value) {
            addCriterion("update_by not like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIn(List<String> values) {
            addCriterion("update_by in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotIn(List<String> values) {
            addCriterion("update_by not in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByBetween(String value1, String value2) {
            addCriterion("update_by between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotBetween(String value1, String value2) {
            addCriterion("update_by not between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("remarks is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("remarks is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("remarks =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("remarks <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("remarks >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("remarks >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("remarks <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("remarks <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("remarks like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("remarks not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("remarks in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("remarks not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("remarks between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("remarks not between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("`status` is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("`status` is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("`status` =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("`status` <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("`status` >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("`status` >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("`status` <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("`status` <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("`status` like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("`status` not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("`status` in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("`status` not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("`status` between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("`status` not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeIsNotNull() {
            addCriterion("shadow.accept_time is not null");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeIsNull() {
            addCriterion("shadow.accept_time is null");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeIsNotNull() {
            addCriterion("shadow.warnning_clear_time is not null");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeIsNull() {
            addCriterion("shadow.warnning_clear_time is null");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}