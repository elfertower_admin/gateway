package com.eat.gateway.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClOperationStepArgsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ClOperationStepArgsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andArgIdIsNull() {
            addCriterion("arg_id is null");
            return (Criteria) this;
        }

        public Criteria andArgIdIsNotNull() {
            addCriterion("arg_id is not null");
            return (Criteria) this;
        }

        public Criteria andArgIdEqualTo(String value) {
            addCriterion("arg_id =", value, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdNotEqualTo(String value) {
            addCriterion("arg_id <>", value, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdGreaterThan(String value) {
            addCriterion("arg_id >", value, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdGreaterThanOrEqualTo(String value) {
            addCriterion("arg_id >=", value, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdLessThan(String value) {
            addCriterion("arg_id <", value, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdLessThanOrEqualTo(String value) {
            addCriterion("arg_id <=", value, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdLike(String value) {
            addCriterion("arg_id like", value, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdNotLike(String value) {
            addCriterion("arg_id not like", value, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdIn(List<String> values) {
            addCriterion("arg_id in", values, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdNotIn(List<String> values) {
            addCriterion("arg_id not in", values, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdBetween(String value1, String value2) {
            addCriterion("arg_id between", value1, value2, "argId");
            return (Criteria) this;
        }

        public Criteria andArgIdNotBetween(String value1, String value2) {
            addCriterion("arg_id not between", value1, value2, "argId");
            return (Criteria) this;
        }

        public Criteria andArgTypeIsNull() {
            addCriterion("arg_type is null");
            return (Criteria) this;
        }

        public Criteria andArgTypeIsNotNull() {
            addCriterion("arg_type is not null");
            return (Criteria) this;
        }

        public Criteria andArgTypeEqualTo(String value) {
            addCriterion("arg_type =", value, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeNotEqualTo(String value) {
            addCriterion("arg_type <>", value, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeGreaterThan(String value) {
            addCriterion("arg_type >", value, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeGreaterThanOrEqualTo(String value) {
            addCriterion("arg_type >=", value, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeLessThan(String value) {
            addCriterion("arg_type <", value, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeLessThanOrEqualTo(String value) {
            addCriterion("arg_type <=", value, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeLike(String value) {
            addCriterion("arg_type like", value, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeNotLike(String value) {
            addCriterion("arg_type not like", value, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeIn(List<String> values) {
            addCriterion("arg_type in", values, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeNotIn(List<String> values) {
            addCriterion("arg_type not in", values, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeBetween(String value1, String value2) {
            addCriterion("arg_type between", value1, value2, "argType");
            return (Criteria) this;
        }

        public Criteria andArgTypeNotBetween(String value1, String value2) {
            addCriterion("arg_type not between", value1, value2, "argType");
            return (Criteria) this;
        }

        public Criteria andArgValIsNull() {
            addCriterion("arg_val is null");
            return (Criteria) this;
        }

        public Criteria andArgValIsNotNull() {
            addCriterion("arg_val is not null");
            return (Criteria) this;
        }

        public Criteria andArgValEqualTo(String value) {
            addCriterion("arg_val =", value, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValNotEqualTo(String value) {
            addCriterion("arg_val <>", value, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValGreaterThan(String value) {
            addCriterion("arg_val >", value, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValGreaterThanOrEqualTo(String value) {
            addCriterion("arg_val >=", value, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValLessThan(String value) {
            addCriterion("arg_val <", value, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValLessThanOrEqualTo(String value) {
            addCriterion("arg_val <=", value, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValLike(String value) {
            addCriterion("arg_val like", value, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValNotLike(String value) {
            addCriterion("arg_val not like", value, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValIn(List<String> values) {
            addCriterion("arg_val in", values, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValNotIn(List<String> values) {
            addCriterion("arg_val not in", values, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValBetween(String value1, String value2) {
            addCriterion("arg_val between", value1, value2, "argVal");
            return (Criteria) this;
        }

        public Criteria andArgValNotBetween(String value1, String value2) {
            addCriterion("arg_val not between", value1, value2, "argVal");
            return (Criteria) this;
        }

        public Criteria andStepIdIsNull() {
            addCriterion("step_id is null");
            return (Criteria) this;
        }

        public Criteria andStepIdIsNotNull() {
            addCriterion("step_id is not null");
            return (Criteria) this;
        }

        public Criteria andStepIdEqualTo(String value) {
            addCriterion("step_id =", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotEqualTo(String value) {
            addCriterion("step_id <>", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdGreaterThan(String value) {
            addCriterion("step_id >", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdGreaterThanOrEqualTo(String value) {
            addCriterion("step_id >=", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdLessThan(String value) {
            addCriterion("step_id <", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdLessThanOrEqualTo(String value) {
            addCriterion("step_id <=", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdLike(String value) {
            addCriterion("step_id like", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotLike(String value) {
            addCriterion("step_id not like", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdIn(List<String> values) {
            addCriterion("step_id in", values, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotIn(List<String> values) {
            addCriterion("step_id not in", values, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdBetween(String value1, String value2) {
            addCriterion("step_id between", value1, value2, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotBetween(String value1, String value2) {
            addCriterion("step_id not between", value1, value2, "stepId");
            return (Criteria) this;
        }

        public Criteria andArgOrderIsNull() {
            addCriterion("arg_order is null");
            return (Criteria) this;
        }

        public Criteria andArgOrderIsNotNull() {
            addCriterion("arg_order is not null");
            return (Criteria) this;
        }

        public Criteria andArgOrderEqualTo(Integer value) {
            addCriterion("arg_order =", value, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderNotEqualTo(Integer value) {
            addCriterion("arg_order <>", value, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderGreaterThan(Integer value) {
            addCriterion("arg_order >", value, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("arg_order >=", value, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderLessThan(Integer value) {
            addCriterion("arg_order <", value, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderLessThanOrEqualTo(Integer value) {
            addCriterion("arg_order <=", value, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderIn(List<Integer> values) {
            addCriterion("arg_order in", values, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderNotIn(List<Integer> values) {
            addCriterion("arg_order not in", values, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderBetween(Integer value1, Integer value2) {
            addCriterion("arg_order between", value1, value2, "argOrder");
            return (Criteria) this;
        }

        public Criteria andArgOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("arg_order not between", value1, value2, "argOrder");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNull() {
            addCriterion("create_by is null");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNotNull() {
            addCriterion("create_by is not null");
            return (Criteria) this;
        }

        public Criteria andCreateByEqualTo(String value) {
            addCriterion("create_by =", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotEqualTo(String value) {
            addCriterion("create_by <>", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThan(String value) {
            addCriterion("create_by >", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThanOrEqualTo(String value) {
            addCriterion("create_by >=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThan(String value) {
            addCriterion("create_by <", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThanOrEqualTo(String value) {
            addCriterion("create_by <=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLike(String value) {
            addCriterion("create_by like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotLike(String value) {
            addCriterion("create_by not like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByIn(List<String> values) {
            addCriterion("create_by in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotIn(List<String> values) {
            addCriterion("create_by not in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByBetween(String value1, String value2) {
            addCriterion("create_by between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotBetween(String value1, String value2) {
            addCriterion("create_by not between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNull() {
            addCriterion("update_by is null");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNotNull() {
            addCriterion("update_by is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateByEqualTo(String value) {
            addCriterion("update_by =", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotEqualTo(String value) {
            addCriterion("update_by <>", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThan(String value) {
            addCriterion("update_by >", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThanOrEqualTo(String value) {
            addCriterion("update_by >=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThan(String value) {
            addCriterion("update_by <", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThanOrEqualTo(String value) {
            addCriterion("update_by <=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLike(String value) {
            addCriterion("update_by like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotLike(String value) {
            addCriterion("update_by not like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIn(List<String> values) {
            addCriterion("update_by in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotIn(List<String> values) {
            addCriterion("update_by not in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByBetween(String value1, String value2) {
            addCriterion("update_by between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotBetween(String value1, String value2) {
            addCriterion("update_by not between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("remarks is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("remarks is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("remarks =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("remarks <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("remarks >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("remarks >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("remarks <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("remarks <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("remarks like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("remarks not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("remarks in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("remarks not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("remarks between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("remarks not between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("`status` is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("`status` is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("`status` =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("`status` <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("`status` >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("`status` >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("`status` <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("`status` <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("`status` like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("`status` not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("`status` in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("`status` not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("`status` between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("`status` not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}