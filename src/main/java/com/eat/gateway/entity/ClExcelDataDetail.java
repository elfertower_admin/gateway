package com.eat.gateway.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * cl_excel_data_detail
 * @author 
 */
public class ClExcelDataDetail implements Serializable {
    private String id;

    private String billCode;

    private String billState;

    private String takeupTime;

    private String dispatchTime;

    private String acceptTime;

    private String warnningTime;

    private String timeLimit;

    private String backTime;

    private String pigeonholeTime;

    private String breakdownOriginate;

    private String warnningName;

    private String warnningState;

    private String warnningDescription;

    private String warnningLevel;

    private String stationServicingId;

    private String stationName;

    private String province;

    private String city;

    private String counties;

    private String villages;

    private String getinStation;

    private String breakdownReason;

    private String exonerative;

    private String exonerationClause;

    private String specificReasons;

    private String reductionMin;

    private String isTowerReason;

    private String breakdownClassify;

    private String contactsMan;

    private String contactsWay;

    private String restrain;

    private String restrainReason;

    private String warnningClearTime;

    private String reply;

    private String deviceType;

    private String prompt;

    private String timeout;

    private String backMan;

    private String servicingCom;

    private String machineRoomType;

    private String powerSupply;

    private String fsu;

    private String fsuManufacturer;

    private String airManufacturer;

    private String batteryManufacturer;

    private String powerManufacturer;

    private String generation;

    private String batterysCount;

    private String capacitance;

    private String totalElectric;

    private String duration;

    private String warnningDuration;

    private String billExecDuration;

    private String department;

    private String backTerminal;

    private String acceptBillLong;

    private String firstFeedbackTime;

    private String overtime;

    private String belongTo;

    private String confirmContent;

    private String manager;

    private String originate;

    private String acceptTerminal;

    private String signinTime;

    private String signinLongitude;

    private String signinLatitude;

    private String fsuLongitude;

    private String fsuLatitude;

    private String powerGenerationBill;

    private String breakdownType;

    private String limitVoltage;

    private String operators;

    private String operatorsBill;

    private String mobileName;

    private String mobileCode;

    private String unicomName;

    private String unicomCode;

    private String telecomName;

    private String telecomCode;

    private String batchId;

    private String status;

    private Date createDate;

    private String createBy;

    private String updateBy;

    private Date updateDate;

    private String remarks;

    private String servicingWay;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getBillState() {
        return billState;
    }

    public void setBillState(String billState) {
        this.billState = billState;
    }

    public String getTakeupTime() {
        return takeupTime;
    }

    public void setTakeupTime(String takeupTime) {
        this.takeupTime = takeupTime;
    }

    public String getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(String dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public String getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(String acceptTime) {
        this.acceptTime = acceptTime;
    }

    public String getWarnningTime() {
        return warnningTime;
    }

    public void setWarnningTime(String warnningTime) {
        this.warnningTime = warnningTime;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getBackTime() {
        return backTime;
    }

    public void setBackTime(String backTime) {
        this.backTime = backTime;
    }

    public String getPigeonholeTime() {
        return pigeonholeTime;
    }

    public void setPigeonholeTime(String pigeonholeTime) {
        this.pigeonholeTime = pigeonholeTime;
    }

    public String getBreakdownOriginate() {
        return breakdownOriginate;
    }

    public void setBreakdownOriginate(String breakdownOriginate) {
        this.breakdownOriginate = breakdownOriginate;
    }

    public String getWarnningName() {
        return warnningName;
    }

    public void setWarnningName(String warnningName) {
        this.warnningName = warnningName;
    }

    public String getWarnningState() {
        return warnningState;
    }

    public void setWarnningState(String warnningState) {
        this.warnningState = warnningState;
    }

    public String getWarnningDescription() {
        return warnningDescription;
    }

    public void setWarnningDescription(String warnningDescription) {
        this.warnningDescription = warnningDescription;
    }

    public String getWarnningLevel() {
        return warnningLevel;
    }

    public void setWarnningLevel(String warnningLevel) {
        this.warnningLevel = warnningLevel;
    }

    public String getStationServicingId() {
        return stationServicingId;
    }

    public void setStationServicingId(String stationServicingId) {
        this.stationServicingId = stationServicingId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounties() {
        return counties;
    }

    public void setCounties(String counties) {
        this.counties = counties;
    }

    public String getVillages() {
        return villages;
    }

    public void setVillages(String villages) {
        this.villages = villages;
    }

    public String getGetinStation() {
        return getinStation;
    }

    public void setGetinStation(String getinStation) {
        this.getinStation = getinStation;
    }

    public String getBreakdownReason() {
        return breakdownReason;
    }

    public void setBreakdownReason(String breakdownReason) {
        this.breakdownReason = breakdownReason;
    }

    public String getExonerative() {
        return exonerative;
    }

    public void setExonerative(String exonerative) {
        this.exonerative = exonerative;
    }

    public String getExonerationClause() {
        return exonerationClause;
    }

    public void setExonerationClause(String exonerationClause) {
        this.exonerationClause = exonerationClause;
    }

    public String getSpecificReasons() {
        return specificReasons;
    }

    public void setSpecificReasons(String specificReasons) {
        this.specificReasons = specificReasons;
    }

    public String getReductionMin() {
        return reductionMin;
    }

    public void setReductionMin(String reductionMin) {
        this.reductionMin = reductionMin;
    }

    public String getIsTowerReason() {
        return isTowerReason;
    }

    public void setIsTowerReason(String isTowerReason) {
        this.isTowerReason = isTowerReason;
    }

    public String getBreakdownClassify() {
        return breakdownClassify;
    }

    public void setBreakdownClassify(String breakdownClassify) {
        this.breakdownClassify = breakdownClassify;
    }

    public String getContactsMan() {
        return contactsMan;
    }

    public void setContactsMan(String contactsMan) {
        this.contactsMan = contactsMan;
    }

    public String getContactsWay() {
        return contactsWay;
    }

    public void setContactsWay(String contactsWay) {
        this.contactsWay = contactsWay;
    }

    public String getRestrain() {
        return restrain;
    }

    public void setRestrain(String restrain) {
        this.restrain = restrain;
    }

    public String getRestrainReason() {
        return restrainReason;
    }

    public void setRestrainReason(String restrainReason) {
        this.restrainReason = restrainReason;
    }

    public String getWarnningClearTime() {
        return warnningClearTime;
    }

    public void setWarnningClearTime(String warnningClearTime) {
        this.warnningClearTime = warnningClearTime;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getBackMan() {
        return backMan;
    }

    public void setBackMan(String backMan) {
        this.backMan = backMan;
    }

    public String getServicingCom() {
        return servicingCom;
    }

    public void setServicingCom(String servicingCom) {
        this.servicingCom = servicingCom;
    }

    public String getMachineRoomType() {
        return machineRoomType;
    }

    public void setMachineRoomType(String machineRoomType) {
        this.machineRoomType = machineRoomType;
    }

    public String getPowerSupply() {
        return powerSupply;
    }

    public void setPowerSupply(String powerSupply) {
        this.powerSupply = powerSupply;
    }

    public String getFsu() {
        return fsu;
    }

    public void setFsu(String fsu) {
        this.fsu = fsu;
    }

    public String getFsuManufacturer() {
        return fsuManufacturer;
    }

    public void setFsuManufacturer(String fsuManufacturer) {
        this.fsuManufacturer = fsuManufacturer;
    }

    public String getAirManufacturer() {
        return airManufacturer;
    }

    public void setAirManufacturer(String airManufacturer) {
        this.airManufacturer = airManufacturer;
    }

    public String getBatteryManufacturer() {
        return batteryManufacturer;
    }

    public void setBatteryManufacturer(String batteryManufacturer) {
        this.batteryManufacturer = batteryManufacturer;
    }

    public String getPowerManufacturer() {
        return powerManufacturer;
    }

    public void setPowerManufacturer(String powerManufacturer) {
        this.powerManufacturer = powerManufacturer;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public String getBatterysCount() {
        return batterysCount;
    }

    public void setBatterysCount(String batterysCount) {
        this.batterysCount = batterysCount;
    }

    public String getCapacitance() {
        return capacitance;
    }

    public void setCapacitance(String capacitance) {
        this.capacitance = capacitance;
    }

    public String getTotalElectric() {
        return totalElectric;
    }

    public void setTotalElectric(String totalElectric) {
        this.totalElectric = totalElectric;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getWarnningDuration() {
        return warnningDuration;
    }

    public void setWarnningDuration(String warnningDuration) {
        this.warnningDuration = warnningDuration;
    }

    public String getBillExecDuration() {
        return billExecDuration;
    }

    public void setBillExecDuration(String billExecDuration) {
        this.billExecDuration = billExecDuration;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBackTerminal() {
        return backTerminal;
    }

    public void setBackTerminal(String backTerminal) {
        this.backTerminal = backTerminal;
    }

    public String getAcceptBillLong() {
        return acceptBillLong;
    }

    public void setAcceptBillLong(String acceptBillLong) {
        this.acceptBillLong = acceptBillLong;
    }

    public String getFirstFeedbackTime() {
        return firstFeedbackTime;
    }

    public void setFirstFeedbackTime(String firstFeedbackTime) {
        this.firstFeedbackTime = firstFeedbackTime;
    }

    public String getOvertime() {
        return overtime;
    }

    public void setOvertime(String overtime) {
        this.overtime = overtime;
    }

    public String getBelongTo() {
        return belongTo;
    }

    public void setBelongTo(String belongTo) {
        this.belongTo = belongTo;
    }

    public String getConfirmContent() {
        return confirmContent;
    }

    public void setConfirmContent(String confirmContent) {
        this.confirmContent = confirmContent;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getOriginate() {
        return originate;
    }

    public void setOriginate(String originate) {
        this.originate = originate;
    }

    public String getAcceptTerminal() {
        return acceptTerminal;
    }

    public void setAcceptTerminal(String acceptTerminal) {
        this.acceptTerminal = acceptTerminal;
    }

    public String getSigninTime() {
        return signinTime;
    }

    public void setSigninTime(String signinTime) {
        this.signinTime = signinTime;
    }

    public String getSigninLongitude() {
        return signinLongitude;
    }

    public void setSigninLongitude(String signinLongitude) {
        this.signinLongitude = signinLongitude;
    }

    public String getSigninLatitude() {
        return signinLatitude;
    }

    public void setSigninLatitude(String signinLatitude) {
        this.signinLatitude = signinLatitude;
    }

    public String getFsuLongitude() {
        return fsuLongitude;
    }

    public void setFsuLongitude(String fsuLongitude) {
        this.fsuLongitude = fsuLongitude;
    }

    public String getFsuLatitude() {
        return fsuLatitude;
    }

    public void setFsuLatitude(String fsuLatitude) {
        this.fsuLatitude = fsuLatitude;
    }

    public String getPowerGenerationBill() {
        return powerGenerationBill;
    }

    public void setPowerGenerationBill(String powerGenerationBill) {
        this.powerGenerationBill = powerGenerationBill;
    }

    public String getBreakdownType() {
        return breakdownType;
    }

    public void setBreakdownType(String breakdownType) {
        this.breakdownType = breakdownType;
    }

    public String getLimitVoltage() {
        return limitVoltage;
    }

    public void setLimitVoltage(String limitVoltage) {
        this.limitVoltage = limitVoltage;
    }

    public String getOperators() {
        return operators;
    }

    public void setOperators(String operators) {
        this.operators = operators;
    }

    public String getOperatorsBill() {
        return operatorsBill;
    }

    public void setOperatorsBill(String operatorsBill) {
        this.operatorsBill = operatorsBill;
    }

    public String getMobileName() {
        return mobileName;
    }

    public void setMobileName(String mobileName) {
        this.mobileName = mobileName;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public String getUnicomName() {
        return unicomName;
    }

    public void setUnicomName(String unicomName) {
        this.unicomName = unicomName;
    }

    public String getUnicomCode() {
        return unicomCode;
    }

    public void setUnicomCode(String unicomCode) {
        this.unicomCode = unicomCode;
    }

    public String getTelecomName() {
        return telecomName;
    }

    public void setTelecomName(String telecomName) {
        this.telecomName = telecomName;
    }

    public String getTelecomCode() {
        return telecomCode;
    }

    public void setTelecomCode(String telecomCode) {
        this.telecomCode = telecomCode;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getServicingWay() {
        return servicingWay;
    }

    public void setServicingWay(String servicingWay) {
        this.servicingWay = servicingWay;
    }
}