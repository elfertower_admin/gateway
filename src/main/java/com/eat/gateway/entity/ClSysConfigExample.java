package com.eat.gateway.entity;

import java.util.ArrayList;
import java.util.List;

public class ClSysConfigExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ClSysConfigExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBrowserNameIsNull() {
            addCriterion("browser_name is null");
            return (Criteria) this;
        }

        public Criteria andBrowserNameIsNotNull() {
            addCriterion("browser_name is not null");
            return (Criteria) this;
        }

        public Criteria andBrowserNameEqualTo(String value) {
            addCriterion("browser_name =", value, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameNotEqualTo(String value) {
            addCriterion("browser_name <>", value, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameGreaterThan(String value) {
            addCriterion("browser_name >", value, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameGreaterThanOrEqualTo(String value) {
            addCriterion("browser_name >=", value, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameLessThan(String value) {
            addCriterion("browser_name <", value, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameLessThanOrEqualTo(String value) {
            addCriterion("browser_name <=", value, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameLike(String value) {
            addCriterion("browser_name like", value, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameNotLike(String value) {
            addCriterion("browser_name not like", value, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameIn(List<String> values) {
            addCriterion("browser_name in", values, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameNotIn(List<String> values) {
            addCriterion("browser_name not in", values, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameBetween(String value1, String value2) {
            addCriterion("browser_name between", value1, value2, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserNameNotBetween(String value1, String value2) {
            addCriterion("browser_name not between", value1, value2, "browserName");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeIsNull() {
            addCriterion("browser_home is null");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeIsNotNull() {
            addCriterion("browser_home is not null");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeEqualTo(String value) {
            addCriterion("browser_home =", value, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeNotEqualTo(String value) {
            addCriterion("browser_home <>", value, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeGreaterThan(String value) {
            addCriterion("browser_home >", value, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeGreaterThanOrEqualTo(String value) {
            addCriterion("browser_home >=", value, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeLessThan(String value) {
            addCriterion("browser_home <", value, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeLessThanOrEqualTo(String value) {
            addCriterion("browser_home <=", value, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeLike(String value) {
            addCriterion("browser_home like", value, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeNotLike(String value) {
            addCriterion("browser_home not like", value, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeIn(List<String> values) {
            addCriterion("browser_home in", values, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeNotIn(List<String> values) {
            addCriterion("browser_home not in", values, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeBetween(String value1, String value2) {
            addCriterion("browser_home between", value1, value2, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserHomeNotBetween(String value1, String value2) {
            addCriterion("browser_home not between", value1, value2, "browserHome");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverIsNull() {
            addCriterion("browser_driver is null");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverIsNotNull() {
            addCriterion("browser_driver is not null");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverEqualTo(String value) {
            addCriterion("browser_driver =", value, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverNotEqualTo(String value) {
            addCriterion("browser_driver <>", value, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverGreaterThan(String value) {
            addCriterion("browser_driver >", value, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverGreaterThanOrEqualTo(String value) {
            addCriterion("browser_driver >=", value, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverLessThan(String value) {
            addCriterion("browser_driver <", value, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverLessThanOrEqualTo(String value) {
            addCriterion("browser_driver <=", value, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverLike(String value) {
            addCriterion("browser_driver like", value, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverNotLike(String value) {
            addCriterion("browser_driver not like", value, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverIn(List<String> values) {
            addCriterion("browser_driver in", values, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverNotIn(List<String> values) {
            addCriterion("browser_driver not in", values, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverBetween(String value1, String value2) {
            addCriterion("browser_driver between", value1, value2, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDriverNotBetween(String value1, String value2) {
            addCriterion("browser_driver not between", value1, value2, "browserDriver");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathIsNull() {
            addCriterion("browser_download_path is null");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathIsNotNull() {
            addCriterion("browser_download_path is not null");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathEqualTo(String value) {
            addCriterion("browser_download_path =", value, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathNotEqualTo(String value) {
            addCriterion("browser_download_path <>", value, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathGreaterThan(String value) {
            addCriterion("browser_download_path >", value, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathGreaterThanOrEqualTo(String value) {
            addCriterion("browser_download_path >=", value, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathLessThan(String value) {
            addCriterion("browser_download_path <", value, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathLessThanOrEqualTo(String value) {
            addCriterion("browser_download_path <=", value, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathLike(String value) {
            addCriterion("browser_download_path like", value, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathNotLike(String value) {
            addCriterion("browser_download_path not like", value, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathIn(List<String> values) {
            addCriterion("browser_download_path in", values, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathNotIn(List<String> values) {
            addCriterion("browser_download_path not in", values, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathBetween(String value1, String value2) {
            addCriterion("browser_download_path between", value1, value2, "browserDownloadPath");
            return (Criteria) this;
        }

        public Criteria andBrowserDownloadPathNotBetween(String value1, String value2) {
            addCriterion("browser_download_path not between", value1, value2, "browserDownloadPath");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}