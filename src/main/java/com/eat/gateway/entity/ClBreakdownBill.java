package com.eat.gateway.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * cl_breakdown_bill
 * @author 
 */
public class ClBreakdownBill implements Serializable {
    /**
     * 主键
     */
    private String breakdownId;

    /**
     * 抓取明细
     */
    private String detailId;

    /**
     * 故障单编码
     */
    private String billCode;

    /**
     * 站点主键
     */
    private String stationId;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 机构编码
     */
    private String officeCode;

    /**
     * 上级机构
     */
    private String parentOfficeCode;

    /**
     * 接单是否超时 1=超时
     */
    private String acceptOvertime;

    /**
     * 回单是否超时
     */
    private String backOvertime;

    private Date createDate;

    private String createBy;

    private String updateBy;

    private Date updateDate;

    private String remarks;

    private String status;

    private Date warnningTime;

    private Date acceptTime;

    private Date backTime;

    private String stationType;

    private String loginCode;

    private String stationName;

    private String warnningName;

    private String warnningDuration;

    private String billExecDuration;

    private String userName;

    private String mobile;

    private String city;

    private String counties;

    private String villages;

    private static final long serialVersionUID = 1L;

    public String getVillages() {
        return villages;
    }

    public void setVillages(String villages) {
        this.villages = villages;
    }

    public String getCounties() {
        return counties;
    }

    public void setCounties(String counties) {
        this.counties = counties;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBillExecDuration() {
        return billExecDuration;
    }

    public void setBillExecDuration(String billExecDuration) {
        this.billExecDuration = billExecDuration;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWarnningDuration() {
        return warnningDuration;
    }

    public void setWarnningDuration(String warnningDuration) {
        this.warnningDuration = warnningDuration;
    }

    public String getWarnningName() {
        return warnningName;
    }

    public void setWarnningName(String warnningName) {
        this.warnningName = warnningName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getStationType() {
        return stationType;
    }

    public void setStationType(String stationType) {
        this.stationType = stationType;
    }

    public Date getBackTime() {
        return backTime;
    }

    public void setBackTime(Date backTime) {
        this.backTime = backTime;
    }

    public Date getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(Date acceptTime) {
        this.acceptTime = acceptTime;
    }

    public Date getWarnningTime() {
        return warnningTime;
    }

    public void setWarnningTime(Date warnningTime) {
        this.warnningTime = warnningTime;
    }

    public String getBreakdownId() {
        return breakdownId;
    }

    public void setBreakdownId(String breakdownId) {
        this.breakdownId = breakdownId;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getParentOfficeCode() {
        return parentOfficeCode;
    }

    public void setParentOfficeCode(String parentOfficeCode) {
        this.parentOfficeCode = parentOfficeCode;
    }

    public String getAcceptOvertime() {
        return acceptOvertime;
    }

    public void setAcceptOvertime(String acceptOvertime) {
        this.acceptOvertime = acceptOvertime;
    }

    public String getBackOvertime() {
        return backOvertime;
    }

    public void setBackOvertime(String backOvertime) {
        this.backOvertime = backOvertime;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}