package com.eat.gateway.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * cl_power_gen_data_detail
 * @author 
 */
public class ClPowerGenDataDetail implements Serializable {
    /**
     * 主键
     */
    private String powerGenId;

    /**
     * 工单编码
     */
    private String billCode;

    /**
     * 工单状态
     */
    private String billState;

    /**
     * 工单类型
     */
    private String billType;

    /**
     * 所属省份
     */
    private String province;

    /**
     * 所属地市
     */
    private String city;

    /**
     * 区县
     */
    private String counties;

    /**
     * 资源编码
     */
    private String resourceCode;

    /**
     * 站址名称
     */
    private String stationName;

    /**
     * 站点维护ID
     */
    private String stationServicingId;

    /**
     * 电压
     */
    private String voltage;

    /**
     * 电流
     */
    private String electricCurrent;

    /**
     * 停电开始时间
     */
    private String startTime;

    /**
     * 停电结束时间
     */
    private String endTime;

    /**
     * 蓄电池剩余续航时长
     */
    private String remainMin;

    /**
     * 发电开始时间
     */
    private String generationStart;

    /**
     * 发电结束时间
     */
    private String generationEnd;

    /**
     * 发电时长
     */
    private String generationMin;

    private String breakdownTime;

    /**
     * 断站时长
     */
    private String breakdownLong;

    /**
     * 派单时间
     */
    private String dispatchTime;

    /**
     * 工单历时
     */
    private String billCost;

    /**
     * 运营商名称
     */
    private String operatorsName;

    /**
     * 移动维护服务等级
     */
    private String mobileLevel;

    /**
     * 联通维护服务等级
     */
    private String unicomLevel;

    /**
     * 电信维护服务等级
     */
    private String telecomLevel;

    /**
     * 是否购买发电服务
     */
    private String buyPowerGen;

    /**
     * 运营商是否同意发电
     */
    private String operatorsAccept;

    /**
     * 代维护公司
     */
    private String servicingCom;

    /**
     * 是否取信
     */
    private String isGetVal;

    /**
     * 派单条件
     */
    private String requirement;

    /**
     * 派单判断历时
     */
    private String dispatchLong;

    /**
     * 运营商确认历时
     */
    private String cormLong;

    /**
     * 上站时长
     */
    private String getin;

    /**
     * 发电人
     */
    private String operator;

    private Date createDate;

    private String createBy;

    private String updateBy;

    private Date updateDate;

    private String remarks;

    private String status;

    private static final long serialVersionUID = 1L;

    public String getPowerGenId() {
        return powerGenId;
    }

    public void setPowerGenId(String powerGenId) {
        this.powerGenId = powerGenId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getBillState() {
        return billState;
    }

    public void setBillState(String billState) {
        this.billState = billState;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounties() {
        return counties;
    }

    public void setCounties(String counties) {
        this.counties = counties;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationServicingId() {
        return stationServicingId;
    }

    public void setStationServicingId(String stationServicingId) {
        this.stationServicingId = stationServicingId;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getElectricCurrent() {
        return electricCurrent;
    }

    public void setElectricCurrent(String electricCurrent) {
        this.electricCurrent = electricCurrent;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRemainMin() {
        return remainMin;
    }

    public void setRemainMin(String remainMin) {
        this.remainMin = remainMin;
    }

    public String getGenerationStart() {
        return generationStart;
    }

    public void setGenerationStart(String generationStart) {
        this.generationStart = generationStart;
    }

    public String getGenerationEnd() {
        return generationEnd;
    }

    public void setGenerationEnd(String generationEnd) {
        this.generationEnd = generationEnd;
    }

    public String getGenerationMin() {
        return generationMin;
    }

    public void setGenerationMin(String generationMin) {
        this.generationMin = generationMin;
    }

    public String getBreakdownTime() {
        return breakdownTime;
    }

    public void setBreakdownTime(String breakdownTime) {
        this.breakdownTime = breakdownTime;
    }

    public String getBreakdownLong() {
        return breakdownLong;
    }

    public void setBreakdownLong(String breakdownLong) {
        this.breakdownLong = breakdownLong;
    }

    public String getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(String dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public String getBillCost() {
        return billCost;
    }

    public void setBillCost(String billCost) {
        this.billCost = billCost;
    }

    public String getOperatorsName() {
        return operatorsName;
    }

    public void setOperatorsName(String operatorsName) {
        this.operatorsName = operatorsName;
    }

    public String getMobileLevel() {
        return mobileLevel;
    }

    public void setMobileLevel(String mobileLevel) {
        this.mobileLevel = mobileLevel;
    }

    public String getUnicomLevel() {
        return unicomLevel;
    }

    public void setUnicomLevel(String unicomLevel) {
        this.unicomLevel = unicomLevel;
    }

    public String getTelecomLevel() {
        return telecomLevel;
    }

    public void setTelecomLevel(String telecomLevel) {
        this.telecomLevel = telecomLevel;
    }

    public String getBuyPowerGen() {
        return buyPowerGen;
    }

    public void setBuyPowerGen(String buyPowerGen) {
        this.buyPowerGen = buyPowerGen;
    }

    public String getOperatorsAccept() {
        return operatorsAccept;
    }

    public void setOperatorsAccept(String operatorsAccept) {
        this.operatorsAccept = operatorsAccept;
    }

    public String getServicingCom() {
        return servicingCom;
    }

    public void setServicingCom(String servicingCom) {
        this.servicingCom = servicingCom;
    }

    public String getIsGetVal() {
        return isGetVal;
    }

    public void setIsGetVal(String isGetVal) {
        this.isGetVal = isGetVal;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getDispatchLong() {
        return dispatchLong;
    }

    public void setDispatchLong(String dispatchLong) {
        this.dispatchLong = dispatchLong;
    }

    public String getCormLong() {
        return cormLong;
    }

    public void setCormLong(String cormLong) {
        this.cormLong = cormLong;
    }

    public String getGetin() {
        return getin;
    }

    public void setGetin(String getin) {
        this.getin = getin;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}