package com.eat.gateway.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * cl_excel_mode
 * @author 
 */
public class ClExcelMode implements Serializable {
    /**
     * 主键
     */
    private String modeId;

    /**
     * 模型关键字
     */
    private String modeName;

    /**
     * 模型解析类
     */
    private String modeClass;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 创建人
     */
    private String createBy;

    private String updateBy;

    private Date updateDate;

    private String remarks;

    private String status;

    private static final long serialVersionUID = 1L;

    public String getModeId() {
        return modeId;
    }

    public void setModeId(String modeId) {
        this.modeId = modeId;
    }

    public String getModeName() {
        return modeName;
    }

    public void setModeName(String modeName) {
        this.modeName = modeName;
    }

    public String getModeClass() {
        return modeClass;
    }

    public void setModeClass(String modeClass) {
        this.modeClass = modeClass;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}