package com.eat.gateway.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * cl_power_gen_bill
 * @author 
 */
public class ClPowerGenBill implements Serializable {
    private String powerGenId;

    private String detailId;

    /**
     * 工单编码
     */
    private String billCode;

    /**
     * 投影记录主键
     */
    private String shadowId;

    private String stationId;

    private String userCode;

    private String officeCode;

    private String parentOfficeCode;

    private String stationLevel;

    /**
     * 是否一级脱压
     */
    private String isTopLevel;

    /**
     * 是否已经发电
     */
    private String isPowerGen;

    /**
     * 是否断站
     */
    private String isOffLine;

    private Date createDate;

    private String createBy;

    private String updateBy;

    private Date updateDate;

    private String remarks;

    private String status;

    private Date acceptTime;

    private Date warnningClearTime;

    private Date warnningTime;

    private String stationType;

    private String warnningName;

    private String loginCode;

    private BigDecimal voltage;

    private Date startTime;

    private String userName;

    private String mobile;

    private String stationName;
    private String city;

    private String counties;

    private String villages;

    private String breakdownLong;

    private static final long serialVersionUID = 1L;

    public String getBreakdownLong() {
        return breakdownLong;
    }

    public void setBreakdownLong(String breakdownLong) {
        this.breakdownLong = breakdownLong;
    }

    public String getVillages() {
        return villages;
    }

    public void setVillages(String villages) {
        this.villages = villages;
    }

    public String getCounties() {
        return counties;
    }

    public void setCounties(String counties) {
        this.counties = counties;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public BigDecimal getVoltage() {
        return voltage;
    }

    public void setVoltage(BigDecimal voltage) {
        this.voltage = voltage;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getWarnningName() {
        return warnningName;
    }

    public void setWarnningName(String warnningName) {
        this.warnningName = warnningName;
    }

    public String getStationType() {
        return stationType;
    }

    public void setStationType(String stationType) {
        this.stationType = stationType;
    }

    public Date getWarnningTime() {
        return warnningTime;
    }

    public void setWarnningTime(Date warnningTime) {
        this.warnningTime = warnningTime;
    }

    public Date getWarnningClearTime() {
        return warnningClearTime;
    }

    public void setWarnningClearTime(Date warnningClearTime) {
        this.warnningClearTime = warnningClearTime;
    }

    public Date getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(Date acceptTime) {
        this.acceptTime = acceptTime;
    }

    public String getPowerGenId() {
        return powerGenId;
    }

    public void setPowerGenId(String powerGenId) {
        this.powerGenId = powerGenId;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getShadowId() {
        return shadowId;
    }

    public void setShadowId(String shadowId) {
        this.shadowId = shadowId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getParentOfficeCode() {
        return parentOfficeCode;
    }

    public void setParentOfficeCode(String parentOfficeCode) {
        this.parentOfficeCode = parentOfficeCode;
    }

    public String getStationLevel() {
        return stationLevel;
    }

    public void setStationLevel(String stationLevel) {
        this.stationLevel = stationLevel;
    }

    public String getIsTopLevel() {
        return isTopLevel;
    }

    public void setIsTopLevel(String isTopLevel) {
        this.isTopLevel = isTopLevel;
    }

    public String getIsPowerGen() {
        return isPowerGen;
    }

    public void setIsPowerGen(String isPowerGen) {
        this.isPowerGen = isPowerGen;
    }

    public String getIsOffLine() {
        return isOffLine;
    }

    public void setIsOffLine(String isOffLine) {
        this.isOffLine = isOffLine;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}