package com.eat.gateway.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClExcelDataDetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ClExcelDataDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBillCodeIsNull() {
            addCriterion("bill_code is null");
            return (Criteria) this;
        }

        public Criteria andBillCodeIsNotNull() {
            addCriterion("bill_code is not null");
            return (Criteria) this;
        }

        public Criteria andBillCodeEqualTo(String value) {
            addCriterion("bill_code =", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotEqualTo(String value) {
            addCriterion("bill_code <>", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeGreaterThan(String value) {
            addCriterion("bill_code >", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeGreaterThanOrEqualTo(String value) {
            addCriterion("bill_code >=", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLessThan(String value) {
            addCriterion("bill_code <", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLessThanOrEqualTo(String value) {
            addCriterion("bill_code <=", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeLike(String value) {
            addCriterion("bill_code like", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotLike(String value) {
            addCriterion("bill_code not like", value, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeIn(List<String> values) {
            addCriterion("bill_code in", values, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotIn(List<String> values) {
            addCriterion("bill_code not in", values, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeBetween(String value1, String value2) {
            addCriterion("bill_code between", value1, value2, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillCodeNotBetween(String value1, String value2) {
            addCriterion("bill_code not between", value1, value2, "billCode");
            return (Criteria) this;
        }

        public Criteria andBillStateIsNull() {
            addCriterion("bill_state is null");
            return (Criteria) this;
        }

        public Criteria andBillStateIsNotNull() {
            addCriterion("bill_state is not null");
            return (Criteria) this;
        }

        public Criteria andBillStateEqualTo(String value) {
            addCriterion("bill_state =", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateNotEqualTo(String value) {
            addCriterion("bill_state <>", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateGreaterThan(String value) {
            addCriterion("bill_state >", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateGreaterThanOrEqualTo(String value) {
            addCriterion("bill_state >=", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateLessThan(String value) {
            addCriterion("bill_state <", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateLessThanOrEqualTo(String value) {
            addCriterion("bill_state <=", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateLike(String value) {
            addCriterion("bill_state like", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateNotLike(String value) {
            addCriterion("bill_state not like", value, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateIn(List<String> values) {
            addCriterion("bill_state in", values, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateNotIn(List<String> values) {
            addCriterion("bill_state not in", values, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateBetween(String value1, String value2) {
            addCriterion("bill_state between", value1, value2, "billState");
            return (Criteria) this;
        }

        public Criteria andBillStateNotBetween(String value1, String value2) {
            addCriterion("bill_state not between", value1, value2, "billState");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeIsNull() {
            addCriterion("takeup_time is null");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeIsNotNull() {
            addCriterion("takeup_time is not null");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeEqualTo(String value) {
            addCriterion("takeup_time =", value, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeNotEqualTo(String value) {
            addCriterion("takeup_time <>", value, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeGreaterThan(String value) {
            addCriterion("takeup_time >", value, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeGreaterThanOrEqualTo(String value) {
            addCriterion("takeup_time >=", value, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeLessThan(String value) {
            addCriterion("takeup_time <", value, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeLessThanOrEqualTo(String value) {
            addCriterion("takeup_time <=", value, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeLike(String value) {
            addCriterion("takeup_time like", value, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeNotLike(String value) {
            addCriterion("takeup_time not like", value, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeIn(List<String> values) {
            addCriterion("takeup_time in", values, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeNotIn(List<String> values) {
            addCriterion("takeup_time not in", values, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeBetween(String value1, String value2) {
            addCriterion("takeup_time between", value1, value2, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andTakeupTimeNotBetween(String value1, String value2) {
            addCriterion("takeup_time not between", value1, value2, "takeupTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeIsNull() {
            addCriterion("dispatch_time is null");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeIsNotNull() {
            addCriterion("dispatch_time is not null");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeEqualTo(String value) {
            addCriterion("dispatch_time =", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeNotEqualTo(String value) {
            addCriterion("dispatch_time <>", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeGreaterThan(String value) {
            addCriterion("dispatch_time >", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeGreaterThanOrEqualTo(String value) {
            addCriterion("dispatch_time >=", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeLessThan(String value) {
            addCriterion("dispatch_time <", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeLessThanOrEqualTo(String value) {
            addCriterion("dispatch_time <=", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeLike(String value) {
            addCriterion("dispatch_time like", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeNotLike(String value) {
            addCriterion("dispatch_time not like", value, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeIn(List<String> values) {
            addCriterion("dispatch_time in", values, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeNotIn(List<String> values) {
            addCriterion("dispatch_time not in", values, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeBetween(String value1, String value2) {
            addCriterion("dispatch_time between", value1, value2, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andDispatchTimeNotBetween(String value1, String value2) {
            addCriterion("dispatch_time not between", value1, value2, "dispatchTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeIsNull() {
            addCriterion("accept_time is null");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeIsNotNull() {
            addCriterion("accept_time is not null");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeEqualTo(String value) {
            addCriterion("accept_time =", value, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeNotEqualTo(String value) {
            addCriterion("accept_time <>", value, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeGreaterThan(String value) {
            addCriterion("accept_time >", value, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeGreaterThanOrEqualTo(String value) {
            addCriterion("accept_time >=", value, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeLessThan(String value) {
            addCriterion("accept_time <", value, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeLessThanOrEqualTo(String value) {
            addCriterion("accept_time <=", value, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeLike(String value) {
            addCriterion("accept_time like", value, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeNotLike(String value) {
            addCriterion("accept_time not like", value, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeIn(List<String> values) {
            addCriterion("accept_time in", values, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeNotIn(List<String> values) {
            addCriterion("accept_time not in", values, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeBetween(String value1, String value2) {
            addCriterion("accept_time between", value1, value2, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andAcceptTimeNotBetween(String value1, String value2) {
            addCriterion("accept_time not between", value1, value2, "acceptTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeIsNull() {
            addCriterion("warnning_time is null");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeIsNotNull() {
            addCriterion("warnning_time is not null");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeEqualTo(String value) {
            addCriterion("warnning_time =", value, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeNotEqualTo(String value) {
            addCriterion("warnning_time <>", value, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeGreaterThan(String value) {
            addCriterion("warnning_time >", value, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeGreaterThanOrEqualTo(String value) {
            addCriterion("warnning_time >=", value, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeLessThan(String value) {
            addCriterion("warnning_time <", value, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeLessThanOrEqualTo(String value) {
            addCriterion("warnning_time <=", value, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeLike(String value) {
            addCriterion("warnning_time like", value, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeNotLike(String value) {
            addCriterion("warnning_time not like", value, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeIn(List<String> values) {
            addCriterion("warnning_time in", values, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeNotIn(List<String> values) {
            addCriterion("warnning_time not in", values, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeBetween(String value1, String value2) {
            addCriterion("warnning_time between", value1, value2, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andWarnningTimeNotBetween(String value1, String value2) {
            addCriterion("warnning_time not between", value1, value2, "warnningTime");
            return (Criteria) this;
        }

        public Criteria andTimeLimitIsNull() {
            addCriterion("time_limit is null");
            return (Criteria) this;
        }

        public Criteria andTimeLimitIsNotNull() {
            addCriterion("time_limit is not null");
            return (Criteria) this;
        }

        public Criteria andTimeLimitEqualTo(String value) {
            addCriterion("time_limit =", value, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitNotEqualTo(String value) {
            addCriterion("time_limit <>", value, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitGreaterThan(String value) {
            addCriterion("time_limit >", value, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitGreaterThanOrEqualTo(String value) {
            addCriterion("time_limit >=", value, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitLessThan(String value) {
            addCriterion("time_limit <", value, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitLessThanOrEqualTo(String value) {
            addCriterion("time_limit <=", value, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitLike(String value) {
            addCriterion("time_limit like", value, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitNotLike(String value) {
            addCriterion("time_limit not like", value, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitIn(List<String> values) {
            addCriterion("time_limit in", values, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitNotIn(List<String> values) {
            addCriterion("time_limit not in", values, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitBetween(String value1, String value2) {
            addCriterion("time_limit between", value1, value2, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andTimeLimitNotBetween(String value1, String value2) {
            addCriterion("time_limit not between", value1, value2, "timeLimit");
            return (Criteria) this;
        }

        public Criteria andBackTimeIsNull() {
            addCriterion("back_time is null");
            return (Criteria) this;
        }

        public Criteria andBackTimeIsNotNull() {
            addCriterion("back_time is not null");
            return (Criteria) this;
        }

        public Criteria andBackTimeEqualTo(String value) {
            addCriterion("back_time =", value, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeNotEqualTo(String value) {
            addCriterion("back_time <>", value, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeGreaterThan(String value) {
            addCriterion("back_time >", value, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeGreaterThanOrEqualTo(String value) {
            addCriterion("back_time >=", value, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeLessThan(String value) {
            addCriterion("back_time <", value, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeLessThanOrEqualTo(String value) {
            addCriterion("back_time <=", value, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeLike(String value) {
            addCriterion("back_time like", value, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeNotLike(String value) {
            addCriterion("back_time not like", value, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeIn(List<String> values) {
            addCriterion("back_time in", values, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeNotIn(List<String> values) {
            addCriterion("back_time not in", values, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeBetween(String value1, String value2) {
            addCriterion("back_time between", value1, value2, "backTime");
            return (Criteria) this;
        }

        public Criteria andBackTimeNotBetween(String value1, String value2) {
            addCriterion("back_time not between", value1, value2, "backTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeIsNull() {
            addCriterion("pigeonhole_time is null");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeIsNotNull() {
            addCriterion("pigeonhole_time is not null");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeEqualTo(String value) {
            addCriterion("pigeonhole_time =", value, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeNotEqualTo(String value) {
            addCriterion("pigeonhole_time <>", value, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeGreaterThan(String value) {
            addCriterion("pigeonhole_time >", value, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeGreaterThanOrEqualTo(String value) {
            addCriterion("pigeonhole_time >=", value, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeLessThan(String value) {
            addCriterion("pigeonhole_time <", value, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeLessThanOrEqualTo(String value) {
            addCriterion("pigeonhole_time <=", value, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeLike(String value) {
            addCriterion("pigeonhole_time like", value, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeNotLike(String value) {
            addCriterion("pigeonhole_time not like", value, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeIn(List<String> values) {
            addCriterion("pigeonhole_time in", values, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeNotIn(List<String> values) {
            addCriterion("pigeonhole_time not in", values, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeBetween(String value1, String value2) {
            addCriterion("pigeonhole_time between", value1, value2, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andPigeonholeTimeNotBetween(String value1, String value2) {
            addCriterion("pigeonhole_time not between", value1, value2, "pigeonholeTime");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateIsNull() {
            addCriterion("breakdown_originate is null");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateIsNotNull() {
            addCriterion("breakdown_originate is not null");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateEqualTo(String value) {
            addCriterion("breakdown_originate =", value, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateNotEqualTo(String value) {
            addCriterion("breakdown_originate <>", value, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateGreaterThan(String value) {
            addCriterion("breakdown_originate >", value, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateGreaterThanOrEqualTo(String value) {
            addCriterion("breakdown_originate >=", value, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateLessThan(String value) {
            addCriterion("breakdown_originate <", value, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateLessThanOrEqualTo(String value) {
            addCriterion("breakdown_originate <=", value, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateLike(String value) {
            addCriterion("breakdown_originate like", value, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateNotLike(String value) {
            addCriterion("breakdown_originate not like", value, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateIn(List<String> values) {
            addCriterion("breakdown_originate in", values, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateNotIn(List<String> values) {
            addCriterion("breakdown_originate not in", values, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateBetween(String value1, String value2) {
            addCriterion("breakdown_originate between", value1, value2, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andBreakdownOriginateNotBetween(String value1, String value2) {
            addCriterion("breakdown_originate not between", value1, value2, "breakdownOriginate");
            return (Criteria) this;
        }

        public Criteria andWarnningNameIsNull() {
            addCriterion("warnning_name is null");
            return (Criteria) this;
        }

        public Criteria andWarnningNameIsNotNull() {
            addCriterion("warnning_name is not null");
            return (Criteria) this;
        }

        public Criteria andWarnningNameEqualTo(String value) {
            addCriterion("warnning_name =", value, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameNotEqualTo(String value) {
            addCriterion("warnning_name <>", value, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameGreaterThan(String value) {
            addCriterion("warnning_name >", value, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameGreaterThanOrEqualTo(String value) {
            addCriterion("warnning_name >=", value, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameLessThan(String value) {
            addCriterion("warnning_name <", value, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameLessThanOrEqualTo(String value) {
            addCriterion("warnning_name <=", value, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameLike(String value) {
            addCriterion("warnning_name like", value, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameNotLike(String value) {
            addCriterion("warnning_name not like", value, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameIn(List<String> values) {
            addCriterion("warnning_name in", values, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameNotIn(List<String> values) {
            addCriterion("warnning_name not in", values, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameBetween(String value1, String value2) {
            addCriterion("warnning_name between", value1, value2, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningNameNotBetween(String value1, String value2) {
            addCriterion("warnning_name not between", value1, value2, "warnningName");
            return (Criteria) this;
        }

        public Criteria andWarnningStateIsNull() {
            addCriterion("warnning_state is null");
            return (Criteria) this;
        }

        public Criteria andWarnningStateIsNotNull() {
            addCriterion("warnning_state is not null");
            return (Criteria) this;
        }

        public Criteria andWarnningStateEqualTo(String value) {
            addCriterion("warnning_state =", value, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateNotEqualTo(String value) {
            addCriterion("warnning_state <>", value, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateGreaterThan(String value) {
            addCriterion("warnning_state >", value, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateGreaterThanOrEqualTo(String value) {
            addCriterion("warnning_state >=", value, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateLessThan(String value) {
            addCriterion("warnning_state <", value, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateLessThanOrEqualTo(String value) {
            addCriterion("warnning_state <=", value, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateLike(String value) {
            addCriterion("warnning_state like", value, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateNotLike(String value) {
            addCriterion("warnning_state not like", value, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateIn(List<String> values) {
            addCriterion("warnning_state in", values, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateNotIn(List<String> values) {
            addCriterion("warnning_state not in", values, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateBetween(String value1, String value2) {
            addCriterion("warnning_state between", value1, value2, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningStateNotBetween(String value1, String value2) {
            addCriterion("warnning_state not between", value1, value2, "warnningState");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionIsNull() {
            addCriterion("warnning_description is null");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionIsNotNull() {
            addCriterion("warnning_description is not null");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionEqualTo(String value) {
            addCriterion("warnning_description =", value, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionNotEqualTo(String value) {
            addCriterion("warnning_description <>", value, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionGreaterThan(String value) {
            addCriterion("warnning_description >", value, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("warnning_description >=", value, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionLessThan(String value) {
            addCriterion("warnning_description <", value, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionLessThanOrEqualTo(String value) {
            addCriterion("warnning_description <=", value, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionLike(String value) {
            addCriterion("warnning_description like", value, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionNotLike(String value) {
            addCriterion("warnning_description not like", value, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionIn(List<String> values) {
            addCriterion("warnning_description in", values, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionNotIn(List<String> values) {
            addCriterion("warnning_description not in", values, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionBetween(String value1, String value2) {
            addCriterion("warnning_description between", value1, value2, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningDescriptionNotBetween(String value1, String value2) {
            addCriterion("warnning_description not between", value1, value2, "warnningDescription");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelIsNull() {
            addCriterion("warnning_level is null");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelIsNotNull() {
            addCriterion("warnning_level is not null");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelEqualTo(String value) {
            addCriterion("warnning_level =", value, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelNotEqualTo(String value) {
            addCriterion("warnning_level <>", value, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelGreaterThan(String value) {
            addCriterion("warnning_level >", value, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelGreaterThanOrEqualTo(String value) {
            addCriterion("warnning_level >=", value, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelLessThan(String value) {
            addCriterion("warnning_level <", value, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelLessThanOrEqualTo(String value) {
            addCriterion("warnning_level <=", value, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelLike(String value) {
            addCriterion("warnning_level like", value, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelNotLike(String value) {
            addCriterion("warnning_level not like", value, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelIn(List<String> values) {
            addCriterion("warnning_level in", values, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelNotIn(List<String> values) {
            addCriterion("warnning_level not in", values, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelBetween(String value1, String value2) {
            addCriterion("warnning_level between", value1, value2, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andWarnningLevelNotBetween(String value1, String value2) {
            addCriterion("warnning_level not between", value1, value2, "warnningLevel");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdIsNull() {
            addCriterion("station_servicing_id is null");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdIsNotNull() {
            addCriterion("station_servicing_id is not null");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdEqualTo(String value) {
            addCriterion("station_servicing_id =", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdNotEqualTo(String value) {
            addCriterion("station_servicing_id <>", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdGreaterThan(String value) {
            addCriterion("station_servicing_id >", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdGreaterThanOrEqualTo(String value) {
            addCriterion("station_servicing_id >=", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdLessThan(String value) {
            addCriterion("station_servicing_id <", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdLessThanOrEqualTo(String value) {
            addCriterion("station_servicing_id <=", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdLike(String value) {
            addCriterion("station_servicing_id like", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdNotLike(String value) {
            addCriterion("station_servicing_id not like", value, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdIn(List<String> values) {
            addCriterion("station_servicing_id in", values, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdNotIn(List<String> values) {
            addCriterion("station_servicing_id not in", values, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdBetween(String value1, String value2) {
            addCriterion("station_servicing_id between", value1, value2, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationServicingIdNotBetween(String value1, String value2) {
            addCriterion("station_servicing_id not between", value1, value2, "stationServicingId");
            return (Criteria) this;
        }

        public Criteria andStationNameIsNull() {
            addCriterion("station_name is null");
            return (Criteria) this;
        }

        public Criteria andStationNameIsNotNull() {
            addCriterion("station_name is not null");
            return (Criteria) this;
        }

        public Criteria andStationNameEqualTo(String value) {
            addCriterion("station_name =", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameNotEqualTo(String value) {
            addCriterion("station_name <>", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameGreaterThan(String value) {
            addCriterion("station_name >", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameGreaterThanOrEqualTo(String value) {
            addCriterion("station_name >=", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameLessThan(String value) {
            addCriterion("station_name <", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameLessThanOrEqualTo(String value) {
            addCriterion("station_name <=", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameLike(String value) {
            addCriterion("station_name like", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameNotLike(String value) {
            addCriterion("station_name not like", value, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameIn(List<String> values) {
            addCriterion("station_name in", values, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameNotIn(List<String> values) {
            addCriterion("station_name not in", values, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameBetween(String value1, String value2) {
            addCriterion("station_name between", value1, value2, "stationName");
            return (Criteria) this;
        }

        public Criteria andStationNameNotBetween(String value1, String value2) {
            addCriterion("station_name not between", value1, value2, "stationName");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCountiesIsNull() {
            addCriterion("counties is null");
            return (Criteria) this;
        }

        public Criteria andCountiesIsNotNull() {
            addCriterion("counties is not null");
            return (Criteria) this;
        }

        public Criteria andCountiesEqualTo(String value) {
            addCriterion("counties =", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesNotEqualTo(String value) {
            addCriterion("counties <>", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesGreaterThan(String value) {
            addCriterion("counties >", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesGreaterThanOrEqualTo(String value) {
            addCriterion("counties >=", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesLessThan(String value) {
            addCriterion("counties <", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesLessThanOrEqualTo(String value) {
            addCriterion("counties <=", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesLike(String value) {
            addCriterion("counties like", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesNotLike(String value) {
            addCriterion("counties not like", value, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesIn(List<String> values) {
            addCriterion("counties in", values, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesNotIn(List<String> values) {
            addCriterion("counties not in", values, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesBetween(String value1, String value2) {
            addCriterion("counties between", value1, value2, "counties");
            return (Criteria) this;
        }

        public Criteria andCountiesNotBetween(String value1, String value2) {
            addCriterion("counties not between", value1, value2, "counties");
            return (Criteria) this;
        }

        public Criteria andVillagesIsNull() {
            addCriterion("villages is null");
            return (Criteria) this;
        }

        public Criteria andVillagesIsNotNull() {
            addCriterion("villages is not null");
            return (Criteria) this;
        }

        public Criteria andVillagesEqualTo(String value) {
            addCriterion("villages =", value, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesNotEqualTo(String value) {
            addCriterion("villages <>", value, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesGreaterThan(String value) {
            addCriterion("villages >", value, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesGreaterThanOrEqualTo(String value) {
            addCriterion("villages >=", value, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesLessThan(String value) {
            addCriterion("villages <", value, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesLessThanOrEqualTo(String value) {
            addCriterion("villages <=", value, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesLike(String value) {
            addCriterion("villages like", value, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesNotLike(String value) {
            addCriterion("villages not like", value, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesIn(List<String> values) {
            addCriterion("villages in", values, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesNotIn(List<String> values) {
            addCriterion("villages not in", values, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesBetween(String value1, String value2) {
            addCriterion("villages between", value1, value2, "villages");
            return (Criteria) this;
        }

        public Criteria andVillagesNotBetween(String value1, String value2) {
            addCriterion("villages not between", value1, value2, "villages");
            return (Criteria) this;
        }

        public Criteria andGetinStationIsNull() {
            addCriterion("getin_station is null");
            return (Criteria) this;
        }

        public Criteria andGetinStationIsNotNull() {
            addCriterion("getin_station is not null");
            return (Criteria) this;
        }

        public Criteria andGetinStationEqualTo(String value) {
            addCriterion("getin_station =", value, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationNotEqualTo(String value) {
            addCriterion("getin_station <>", value, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationGreaterThan(String value) {
            addCriterion("getin_station >", value, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationGreaterThanOrEqualTo(String value) {
            addCriterion("getin_station >=", value, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationLessThan(String value) {
            addCriterion("getin_station <", value, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationLessThanOrEqualTo(String value) {
            addCriterion("getin_station <=", value, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationLike(String value) {
            addCriterion("getin_station like", value, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationNotLike(String value) {
            addCriterion("getin_station not like", value, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationIn(List<String> values) {
            addCriterion("getin_station in", values, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationNotIn(List<String> values) {
            addCriterion("getin_station not in", values, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationBetween(String value1, String value2) {
            addCriterion("getin_station between", value1, value2, "getinStation");
            return (Criteria) this;
        }

        public Criteria andGetinStationNotBetween(String value1, String value2) {
            addCriterion("getin_station not between", value1, value2, "getinStation");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonIsNull() {
            addCriterion("breakdown_reason is null");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonIsNotNull() {
            addCriterion("breakdown_reason is not null");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonEqualTo(String value) {
            addCriterion("breakdown_reason =", value, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonNotEqualTo(String value) {
            addCriterion("breakdown_reason <>", value, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonGreaterThan(String value) {
            addCriterion("breakdown_reason >", value, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonGreaterThanOrEqualTo(String value) {
            addCriterion("breakdown_reason >=", value, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonLessThan(String value) {
            addCriterion("breakdown_reason <", value, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonLessThanOrEqualTo(String value) {
            addCriterion("breakdown_reason <=", value, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonLike(String value) {
            addCriterion("breakdown_reason like", value, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonNotLike(String value) {
            addCriterion("breakdown_reason not like", value, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonIn(List<String> values) {
            addCriterion("breakdown_reason in", values, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonNotIn(List<String> values) {
            addCriterion("breakdown_reason not in", values, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonBetween(String value1, String value2) {
            addCriterion("breakdown_reason between", value1, value2, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownReasonNotBetween(String value1, String value2) {
            addCriterion("breakdown_reason not between", value1, value2, "breakdownReason");
            return (Criteria) this;
        }

        public Criteria andExonerativeIsNull() {
            addCriterion("exonerative is null");
            return (Criteria) this;
        }

        public Criteria andExonerativeIsNotNull() {
            addCriterion("exonerative is not null");
            return (Criteria) this;
        }

        public Criteria andExonerativeEqualTo(String value) {
            addCriterion("exonerative =", value, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeNotEqualTo(String value) {
            addCriterion("exonerative <>", value, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeGreaterThan(String value) {
            addCriterion("exonerative >", value, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeGreaterThanOrEqualTo(String value) {
            addCriterion("exonerative >=", value, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeLessThan(String value) {
            addCriterion("exonerative <", value, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeLessThanOrEqualTo(String value) {
            addCriterion("exonerative <=", value, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeLike(String value) {
            addCriterion("exonerative like", value, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeNotLike(String value) {
            addCriterion("exonerative not like", value, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeIn(List<String> values) {
            addCriterion("exonerative in", values, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeNotIn(List<String> values) {
            addCriterion("exonerative not in", values, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeBetween(String value1, String value2) {
            addCriterion("exonerative between", value1, value2, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerativeNotBetween(String value1, String value2) {
            addCriterion("exonerative not between", value1, value2, "exonerative");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseIsNull() {
            addCriterion("exoneration_clause is null");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseIsNotNull() {
            addCriterion("exoneration_clause is not null");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseEqualTo(String value) {
            addCriterion("exoneration_clause =", value, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseNotEqualTo(String value) {
            addCriterion("exoneration_clause <>", value, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseGreaterThan(String value) {
            addCriterion("exoneration_clause >", value, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseGreaterThanOrEqualTo(String value) {
            addCriterion("exoneration_clause >=", value, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseLessThan(String value) {
            addCriterion("exoneration_clause <", value, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseLessThanOrEqualTo(String value) {
            addCriterion("exoneration_clause <=", value, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseLike(String value) {
            addCriterion("exoneration_clause like", value, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseNotLike(String value) {
            addCriterion("exoneration_clause not like", value, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseIn(List<String> values) {
            addCriterion("exoneration_clause in", values, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseNotIn(List<String> values) {
            addCriterion("exoneration_clause not in", values, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseBetween(String value1, String value2) {
            addCriterion("exoneration_clause between", value1, value2, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andExonerationClauseNotBetween(String value1, String value2) {
            addCriterion("exoneration_clause not between", value1, value2, "exonerationClause");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsIsNull() {
            addCriterion("specific_reasons is null");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsIsNotNull() {
            addCriterion("specific_reasons is not null");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsEqualTo(String value) {
            addCriterion("specific_reasons =", value, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsNotEqualTo(String value) {
            addCriterion("specific_reasons <>", value, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsGreaterThan(String value) {
            addCriterion("specific_reasons >", value, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsGreaterThanOrEqualTo(String value) {
            addCriterion("specific_reasons >=", value, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsLessThan(String value) {
            addCriterion("specific_reasons <", value, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsLessThanOrEqualTo(String value) {
            addCriterion("specific_reasons <=", value, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsLike(String value) {
            addCriterion("specific_reasons like", value, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsNotLike(String value) {
            addCriterion("specific_reasons not like", value, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsIn(List<String> values) {
            addCriterion("specific_reasons in", values, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsNotIn(List<String> values) {
            addCriterion("specific_reasons not in", values, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsBetween(String value1, String value2) {
            addCriterion("specific_reasons between", value1, value2, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andSpecificReasonsNotBetween(String value1, String value2) {
            addCriterion("specific_reasons not between", value1, value2, "specificReasons");
            return (Criteria) this;
        }

        public Criteria andReductionMinIsNull() {
            addCriterion("reduction_min is null");
            return (Criteria) this;
        }

        public Criteria andReductionMinIsNotNull() {
            addCriterion("reduction_min is not null");
            return (Criteria) this;
        }

        public Criteria andReductionMinEqualTo(String value) {
            addCriterion("reduction_min =", value, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinNotEqualTo(String value) {
            addCriterion("reduction_min <>", value, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinGreaterThan(String value) {
            addCriterion("reduction_min >", value, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinGreaterThanOrEqualTo(String value) {
            addCriterion("reduction_min >=", value, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinLessThan(String value) {
            addCriterion("reduction_min <", value, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinLessThanOrEqualTo(String value) {
            addCriterion("reduction_min <=", value, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinLike(String value) {
            addCriterion("reduction_min like", value, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinNotLike(String value) {
            addCriterion("reduction_min not like", value, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinIn(List<String> values) {
            addCriterion("reduction_min in", values, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinNotIn(List<String> values) {
            addCriterion("reduction_min not in", values, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinBetween(String value1, String value2) {
            addCriterion("reduction_min between", value1, value2, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andReductionMinNotBetween(String value1, String value2) {
            addCriterion("reduction_min not between", value1, value2, "reductionMin");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonIsNull() {
            addCriterion("is_tower_reason is null");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonIsNotNull() {
            addCriterion("is_tower_reason is not null");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonEqualTo(String value) {
            addCriterion("is_tower_reason =", value, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonNotEqualTo(String value) {
            addCriterion("is_tower_reason <>", value, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonGreaterThan(String value) {
            addCriterion("is_tower_reason >", value, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonGreaterThanOrEqualTo(String value) {
            addCriterion("is_tower_reason >=", value, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonLessThan(String value) {
            addCriterion("is_tower_reason <", value, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonLessThanOrEqualTo(String value) {
            addCriterion("is_tower_reason <=", value, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonLike(String value) {
            addCriterion("is_tower_reason like", value, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonNotLike(String value) {
            addCriterion("is_tower_reason not like", value, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonIn(List<String> values) {
            addCriterion("is_tower_reason in", values, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonNotIn(List<String> values) {
            addCriterion("is_tower_reason not in", values, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonBetween(String value1, String value2) {
            addCriterion("is_tower_reason between", value1, value2, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andIsTowerReasonNotBetween(String value1, String value2) {
            addCriterion("is_tower_reason not between", value1, value2, "isTowerReason");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyIsNull() {
            addCriterion("breakdown_classify is null");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyIsNotNull() {
            addCriterion("breakdown_classify is not null");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyEqualTo(String value) {
            addCriterion("breakdown_classify =", value, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyNotEqualTo(String value) {
            addCriterion("breakdown_classify <>", value, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyGreaterThan(String value) {
            addCriterion("breakdown_classify >", value, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyGreaterThanOrEqualTo(String value) {
            addCriterion("breakdown_classify >=", value, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyLessThan(String value) {
            addCriterion("breakdown_classify <", value, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyLessThanOrEqualTo(String value) {
            addCriterion("breakdown_classify <=", value, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyLike(String value) {
            addCriterion("breakdown_classify like", value, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyNotLike(String value) {
            addCriterion("breakdown_classify not like", value, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyIn(List<String> values) {
            addCriterion("breakdown_classify in", values, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyNotIn(List<String> values) {
            addCriterion("breakdown_classify not in", values, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyBetween(String value1, String value2) {
            addCriterion("breakdown_classify between", value1, value2, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andBreakdownClassifyNotBetween(String value1, String value2) {
            addCriterion("breakdown_classify not between", value1, value2, "breakdownClassify");
            return (Criteria) this;
        }

        public Criteria andContactsManIsNull() {
            addCriterion("contacts_man is null");
            return (Criteria) this;
        }

        public Criteria andContactsManIsNotNull() {
            addCriterion("contacts_man is not null");
            return (Criteria) this;
        }

        public Criteria andContactsManEqualTo(String value) {
            addCriterion("contacts_man =", value, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManNotEqualTo(String value) {
            addCriterion("contacts_man <>", value, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManGreaterThan(String value) {
            addCriterion("contacts_man >", value, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManGreaterThanOrEqualTo(String value) {
            addCriterion("contacts_man >=", value, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManLessThan(String value) {
            addCriterion("contacts_man <", value, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManLessThanOrEqualTo(String value) {
            addCriterion("contacts_man <=", value, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManLike(String value) {
            addCriterion("contacts_man like", value, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManNotLike(String value) {
            addCriterion("contacts_man not like", value, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManIn(List<String> values) {
            addCriterion("contacts_man in", values, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManNotIn(List<String> values) {
            addCriterion("contacts_man not in", values, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManBetween(String value1, String value2) {
            addCriterion("contacts_man between", value1, value2, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsManNotBetween(String value1, String value2) {
            addCriterion("contacts_man not between", value1, value2, "contactsMan");
            return (Criteria) this;
        }

        public Criteria andContactsWayIsNull() {
            addCriterion("contacts_way is null");
            return (Criteria) this;
        }

        public Criteria andContactsWayIsNotNull() {
            addCriterion("contacts_way is not null");
            return (Criteria) this;
        }

        public Criteria andContactsWayEqualTo(String value) {
            addCriterion("contacts_way =", value, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayNotEqualTo(String value) {
            addCriterion("contacts_way <>", value, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayGreaterThan(String value) {
            addCriterion("contacts_way >", value, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayGreaterThanOrEqualTo(String value) {
            addCriterion("contacts_way >=", value, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayLessThan(String value) {
            addCriterion("contacts_way <", value, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayLessThanOrEqualTo(String value) {
            addCriterion("contacts_way <=", value, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayLike(String value) {
            addCriterion("contacts_way like", value, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayNotLike(String value) {
            addCriterion("contacts_way not like", value, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayIn(List<String> values) {
            addCriterion("contacts_way in", values, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayNotIn(List<String> values) {
            addCriterion("contacts_way not in", values, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayBetween(String value1, String value2) {
            addCriterion("contacts_way between", value1, value2, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andContactsWayNotBetween(String value1, String value2) {
            addCriterion("contacts_way not between", value1, value2, "contactsWay");
            return (Criteria) this;
        }

        public Criteria andRestrainIsNull() {
            addCriterion("restrain is null");
            return (Criteria) this;
        }

        public Criteria andRestrainIsNotNull() {
            addCriterion("restrain is not null");
            return (Criteria) this;
        }

        public Criteria andRestrainEqualTo(String value) {
            addCriterion("restrain =", value, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainNotEqualTo(String value) {
            addCriterion("restrain <>", value, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainGreaterThan(String value) {
            addCriterion("restrain >", value, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainGreaterThanOrEqualTo(String value) {
            addCriterion("restrain >=", value, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainLessThan(String value) {
            addCriterion("restrain <", value, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainLessThanOrEqualTo(String value) {
            addCriterion("restrain <=", value, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainLike(String value) {
            addCriterion("restrain like", value, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainNotLike(String value) {
            addCriterion("restrain not like", value, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainIn(List<String> values) {
            addCriterion("restrain in", values, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainNotIn(List<String> values) {
            addCriterion("restrain not in", values, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainBetween(String value1, String value2) {
            addCriterion("restrain between", value1, value2, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainNotBetween(String value1, String value2) {
            addCriterion("restrain not between", value1, value2, "restrain");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonIsNull() {
            addCriterion("restrain_reason is null");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonIsNotNull() {
            addCriterion("restrain_reason is not null");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonEqualTo(String value) {
            addCriterion("restrain_reason =", value, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonNotEqualTo(String value) {
            addCriterion("restrain_reason <>", value, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonGreaterThan(String value) {
            addCriterion("restrain_reason >", value, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonGreaterThanOrEqualTo(String value) {
            addCriterion("restrain_reason >=", value, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonLessThan(String value) {
            addCriterion("restrain_reason <", value, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonLessThanOrEqualTo(String value) {
            addCriterion("restrain_reason <=", value, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonLike(String value) {
            addCriterion("restrain_reason like", value, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonNotLike(String value) {
            addCriterion("restrain_reason not like", value, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonIn(List<String> values) {
            addCriterion("restrain_reason in", values, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonNotIn(List<String> values) {
            addCriterion("restrain_reason not in", values, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonBetween(String value1, String value2) {
            addCriterion("restrain_reason between", value1, value2, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andRestrainReasonNotBetween(String value1, String value2) {
            addCriterion("restrain_reason not between", value1, value2, "restrainReason");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeIsNull() {
            addCriterion("warnning_clear_time is null");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeIsNotNull() {
            addCriterion("warnning_clear_time is not null");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeEqualTo(String value) {
            addCriterion("warnning_clear_time =", value, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeNotEqualTo(String value) {
            addCriterion("warnning_clear_time <>", value, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeGreaterThan(String value) {
            addCriterion("warnning_clear_time >", value, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeGreaterThanOrEqualTo(String value) {
            addCriterion("warnning_clear_time >=", value, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeLessThan(String value) {
            addCriterion("warnning_clear_time <", value, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeLessThanOrEqualTo(String value) {
            addCriterion("warnning_clear_time <=", value, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeLike(String value) {
            addCriterion("warnning_clear_time like", value, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeNotLike(String value) {
            addCriterion("warnning_clear_time not like", value, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeIn(List<String> values) {
            addCriterion("warnning_clear_time in", values, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeNotIn(List<String> values) {
            addCriterion("warnning_clear_time not in", values, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeBetween(String value1, String value2) {
            addCriterion("warnning_clear_time between", value1, value2, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andWarnningClearTimeNotBetween(String value1, String value2) {
            addCriterion("warnning_clear_time not between", value1, value2, "warnningClearTime");
            return (Criteria) this;
        }

        public Criteria andReplyIsNull() {
            addCriterion("reply is null");
            return (Criteria) this;
        }

        public Criteria andReplyIsNotNull() {
            addCriterion("reply is not null");
            return (Criteria) this;
        }

        public Criteria andReplyEqualTo(String value) {
            addCriterion("reply =", value, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyNotEqualTo(String value) {
            addCriterion("reply <>", value, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyGreaterThan(String value) {
            addCriterion("reply >", value, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyGreaterThanOrEqualTo(String value) {
            addCriterion("reply >=", value, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyLessThan(String value) {
            addCriterion("reply <", value, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyLessThanOrEqualTo(String value) {
            addCriterion("reply <=", value, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyLike(String value) {
            addCriterion("reply like", value, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyNotLike(String value) {
            addCriterion("reply not like", value, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyIn(List<String> values) {
            addCriterion("reply in", values, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyNotIn(List<String> values) {
            addCriterion("reply not in", values, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyBetween(String value1, String value2) {
            addCriterion("reply between", value1, value2, "reply");
            return (Criteria) this;
        }

        public Criteria andReplyNotBetween(String value1, String value2) {
            addCriterion("reply not between", value1, value2, "reply");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIsNull() {
            addCriterion("device_type is null");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIsNotNull() {
            addCriterion("device_type is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeEqualTo(String value) {
            addCriterion("device_type =", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotEqualTo(String value) {
            addCriterion("device_type <>", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeGreaterThan(String value) {
            addCriterion("device_type >", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeGreaterThanOrEqualTo(String value) {
            addCriterion("device_type >=", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeLessThan(String value) {
            addCriterion("device_type <", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeLessThanOrEqualTo(String value) {
            addCriterion("device_type <=", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeLike(String value) {
            addCriterion("device_type like", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotLike(String value) {
            addCriterion("device_type not like", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIn(List<String> values) {
            addCriterion("device_type in", values, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotIn(List<String> values) {
            addCriterion("device_type not in", values, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeBetween(String value1, String value2) {
            addCriterion("device_type between", value1, value2, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotBetween(String value1, String value2) {
            addCriterion("device_type not between", value1, value2, "deviceType");
            return (Criteria) this;
        }

        public Criteria andPromptIsNull() {
            addCriterion("prompt is null");
            return (Criteria) this;
        }

        public Criteria andPromptIsNotNull() {
            addCriterion("prompt is not null");
            return (Criteria) this;
        }

        public Criteria andPromptEqualTo(String value) {
            addCriterion("prompt =", value, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptNotEqualTo(String value) {
            addCriterion("prompt <>", value, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptGreaterThan(String value) {
            addCriterion("prompt >", value, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptGreaterThanOrEqualTo(String value) {
            addCriterion("prompt >=", value, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptLessThan(String value) {
            addCriterion("prompt <", value, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptLessThanOrEqualTo(String value) {
            addCriterion("prompt <=", value, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptLike(String value) {
            addCriterion("prompt like", value, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptNotLike(String value) {
            addCriterion("prompt not like", value, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptIn(List<String> values) {
            addCriterion("prompt in", values, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptNotIn(List<String> values) {
            addCriterion("prompt not in", values, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptBetween(String value1, String value2) {
            addCriterion("prompt between", value1, value2, "prompt");
            return (Criteria) this;
        }

        public Criteria andPromptNotBetween(String value1, String value2) {
            addCriterion("prompt not between", value1, value2, "prompt");
            return (Criteria) this;
        }

        public Criteria andTimeoutIsNull() {
            addCriterion("timeout is null");
            return (Criteria) this;
        }

        public Criteria andTimeoutIsNotNull() {
            addCriterion("timeout is not null");
            return (Criteria) this;
        }

        public Criteria andTimeoutEqualTo(String value) {
            addCriterion("timeout =", value, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutNotEqualTo(String value) {
            addCriterion("timeout <>", value, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutGreaterThan(String value) {
            addCriterion("timeout >", value, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutGreaterThanOrEqualTo(String value) {
            addCriterion("timeout >=", value, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutLessThan(String value) {
            addCriterion("timeout <", value, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutLessThanOrEqualTo(String value) {
            addCriterion("timeout <=", value, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutLike(String value) {
            addCriterion("timeout like", value, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutNotLike(String value) {
            addCriterion("timeout not like", value, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutIn(List<String> values) {
            addCriterion("timeout in", values, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutNotIn(List<String> values) {
            addCriterion("timeout not in", values, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutBetween(String value1, String value2) {
            addCriterion("timeout between", value1, value2, "timeout");
            return (Criteria) this;
        }

        public Criteria andTimeoutNotBetween(String value1, String value2) {
            addCriterion("timeout not between", value1, value2, "timeout");
            return (Criteria) this;
        }

        public Criteria andBackManIsNull() {
            addCriterion("back_man is null");
            return (Criteria) this;
        }

        public Criteria andBackManIsNotNull() {
            addCriterion("back_man is not null");
            return (Criteria) this;
        }

        public Criteria andBackManEqualTo(String value) {
            addCriterion("back_man =", value, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManNotEqualTo(String value) {
            addCriterion("back_man <>", value, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManGreaterThan(String value) {
            addCriterion("back_man >", value, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManGreaterThanOrEqualTo(String value) {
            addCriterion("back_man >=", value, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManLessThan(String value) {
            addCriterion("back_man <", value, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManLessThanOrEqualTo(String value) {
            addCriterion("back_man <=", value, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManLike(String value) {
            addCriterion("back_man like", value, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManNotLike(String value) {
            addCriterion("back_man not like", value, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManIn(List<String> values) {
            addCriterion("back_man in", values, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManNotIn(List<String> values) {
            addCriterion("back_man not in", values, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManBetween(String value1, String value2) {
            addCriterion("back_man between", value1, value2, "backMan");
            return (Criteria) this;
        }

        public Criteria andBackManNotBetween(String value1, String value2) {
            addCriterion("back_man not between", value1, value2, "backMan");
            return (Criteria) this;
        }

        public Criteria andServicingComIsNull() {
            addCriterion("servicing_com is null");
            return (Criteria) this;
        }

        public Criteria andServicingComIsNotNull() {
            addCriterion("servicing_com is not null");
            return (Criteria) this;
        }

        public Criteria andServicingComEqualTo(String value) {
            addCriterion("servicing_com =", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComNotEqualTo(String value) {
            addCriterion("servicing_com <>", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComGreaterThan(String value) {
            addCriterion("servicing_com >", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComGreaterThanOrEqualTo(String value) {
            addCriterion("servicing_com >=", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComLessThan(String value) {
            addCriterion("servicing_com <", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComLessThanOrEqualTo(String value) {
            addCriterion("servicing_com <=", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComLike(String value) {
            addCriterion("servicing_com like", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComNotLike(String value) {
            addCriterion("servicing_com not like", value, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComIn(List<String> values) {
            addCriterion("servicing_com in", values, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComNotIn(List<String> values) {
            addCriterion("servicing_com not in", values, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComBetween(String value1, String value2) {
            addCriterion("servicing_com between", value1, value2, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andServicingComNotBetween(String value1, String value2) {
            addCriterion("servicing_com not between", value1, value2, "servicingCom");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeIsNull() {
            addCriterion("machine_room_type is null");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeIsNotNull() {
            addCriterion("machine_room_type is not null");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeEqualTo(String value) {
            addCriterion("machine_room_type =", value, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeNotEqualTo(String value) {
            addCriterion("machine_room_type <>", value, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeGreaterThan(String value) {
            addCriterion("machine_room_type >", value, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeGreaterThanOrEqualTo(String value) {
            addCriterion("machine_room_type >=", value, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeLessThan(String value) {
            addCriterion("machine_room_type <", value, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeLessThanOrEqualTo(String value) {
            addCriterion("machine_room_type <=", value, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeLike(String value) {
            addCriterion("machine_room_type like", value, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeNotLike(String value) {
            addCriterion("machine_room_type not like", value, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeIn(List<String> values) {
            addCriterion("machine_room_type in", values, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeNotIn(List<String> values) {
            addCriterion("machine_room_type not in", values, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeBetween(String value1, String value2) {
            addCriterion("machine_room_type between", value1, value2, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andMachineRoomTypeNotBetween(String value1, String value2) {
            addCriterion("machine_room_type not between", value1, value2, "machineRoomType");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyIsNull() {
            addCriterion("power_supply is null");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyIsNotNull() {
            addCriterion("power_supply is not null");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyEqualTo(String value) {
            addCriterion("power_supply =", value, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyNotEqualTo(String value) {
            addCriterion("power_supply <>", value, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyGreaterThan(String value) {
            addCriterion("power_supply >", value, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyGreaterThanOrEqualTo(String value) {
            addCriterion("power_supply >=", value, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyLessThan(String value) {
            addCriterion("power_supply <", value, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyLessThanOrEqualTo(String value) {
            addCriterion("power_supply <=", value, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyLike(String value) {
            addCriterion("power_supply like", value, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyNotLike(String value) {
            addCriterion("power_supply not like", value, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyIn(List<String> values) {
            addCriterion("power_supply in", values, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyNotIn(List<String> values) {
            addCriterion("power_supply not in", values, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyBetween(String value1, String value2) {
            addCriterion("power_supply between", value1, value2, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andPowerSupplyNotBetween(String value1, String value2) {
            addCriterion("power_supply not between", value1, value2, "powerSupply");
            return (Criteria) this;
        }

        public Criteria andFsuIsNull() {
            addCriterion("fsu is null");
            return (Criteria) this;
        }

        public Criteria andFsuIsNotNull() {
            addCriterion("fsu is not null");
            return (Criteria) this;
        }

        public Criteria andFsuEqualTo(String value) {
            addCriterion("fsu =", value, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuNotEqualTo(String value) {
            addCriterion("fsu <>", value, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuGreaterThan(String value) {
            addCriterion("fsu >", value, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuGreaterThanOrEqualTo(String value) {
            addCriterion("fsu >=", value, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuLessThan(String value) {
            addCriterion("fsu <", value, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuLessThanOrEqualTo(String value) {
            addCriterion("fsu <=", value, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuLike(String value) {
            addCriterion("fsu like", value, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuNotLike(String value) {
            addCriterion("fsu not like", value, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuIn(List<String> values) {
            addCriterion("fsu in", values, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuNotIn(List<String> values) {
            addCriterion("fsu not in", values, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuBetween(String value1, String value2) {
            addCriterion("fsu between", value1, value2, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuNotBetween(String value1, String value2) {
            addCriterion("fsu not between", value1, value2, "fsu");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerIsNull() {
            addCriterion("fsu_manufacturer is null");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerIsNotNull() {
            addCriterion("fsu_manufacturer is not null");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerEqualTo(String value) {
            addCriterion("fsu_manufacturer =", value, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerNotEqualTo(String value) {
            addCriterion("fsu_manufacturer <>", value, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerGreaterThan(String value) {
            addCriterion("fsu_manufacturer >", value, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerGreaterThanOrEqualTo(String value) {
            addCriterion("fsu_manufacturer >=", value, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerLessThan(String value) {
            addCriterion("fsu_manufacturer <", value, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerLessThanOrEqualTo(String value) {
            addCriterion("fsu_manufacturer <=", value, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerLike(String value) {
            addCriterion("fsu_manufacturer like", value, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerNotLike(String value) {
            addCriterion("fsu_manufacturer not like", value, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerIn(List<String> values) {
            addCriterion("fsu_manufacturer in", values, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerNotIn(List<String> values) {
            addCriterion("fsu_manufacturer not in", values, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerBetween(String value1, String value2) {
            addCriterion("fsu_manufacturer between", value1, value2, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andFsuManufacturerNotBetween(String value1, String value2) {
            addCriterion("fsu_manufacturer not between", value1, value2, "fsuManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerIsNull() {
            addCriterion("air_manufacturer is null");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerIsNotNull() {
            addCriterion("air_manufacturer is not null");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerEqualTo(String value) {
            addCriterion("air_manufacturer =", value, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerNotEqualTo(String value) {
            addCriterion("air_manufacturer <>", value, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerGreaterThan(String value) {
            addCriterion("air_manufacturer >", value, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerGreaterThanOrEqualTo(String value) {
            addCriterion("air_manufacturer >=", value, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerLessThan(String value) {
            addCriterion("air_manufacturer <", value, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerLessThanOrEqualTo(String value) {
            addCriterion("air_manufacturer <=", value, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerLike(String value) {
            addCriterion("air_manufacturer like", value, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerNotLike(String value) {
            addCriterion("air_manufacturer not like", value, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerIn(List<String> values) {
            addCriterion("air_manufacturer in", values, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerNotIn(List<String> values) {
            addCriterion("air_manufacturer not in", values, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerBetween(String value1, String value2) {
            addCriterion("air_manufacturer between", value1, value2, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andAirManufacturerNotBetween(String value1, String value2) {
            addCriterion("air_manufacturer not between", value1, value2, "airManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerIsNull() {
            addCriterion("battery_manufacturer is null");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerIsNotNull() {
            addCriterion("battery_manufacturer is not null");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerEqualTo(String value) {
            addCriterion("battery_manufacturer =", value, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerNotEqualTo(String value) {
            addCriterion("battery_manufacturer <>", value, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerGreaterThan(String value) {
            addCriterion("battery_manufacturer >", value, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerGreaterThanOrEqualTo(String value) {
            addCriterion("battery_manufacturer >=", value, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerLessThan(String value) {
            addCriterion("battery_manufacturer <", value, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerLessThanOrEqualTo(String value) {
            addCriterion("battery_manufacturer <=", value, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerLike(String value) {
            addCriterion("battery_manufacturer like", value, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerNotLike(String value) {
            addCriterion("battery_manufacturer not like", value, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerIn(List<String> values) {
            addCriterion("battery_manufacturer in", values, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerNotIn(List<String> values) {
            addCriterion("battery_manufacturer not in", values, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerBetween(String value1, String value2) {
            addCriterion("battery_manufacturer between", value1, value2, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andBatteryManufacturerNotBetween(String value1, String value2) {
            addCriterion("battery_manufacturer not between", value1, value2, "batteryManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerIsNull() {
            addCriterion("power_manufacturer is null");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerIsNotNull() {
            addCriterion("power_manufacturer is not null");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerEqualTo(String value) {
            addCriterion("power_manufacturer =", value, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerNotEqualTo(String value) {
            addCriterion("power_manufacturer <>", value, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerGreaterThan(String value) {
            addCriterion("power_manufacturer >", value, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerGreaterThanOrEqualTo(String value) {
            addCriterion("power_manufacturer >=", value, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerLessThan(String value) {
            addCriterion("power_manufacturer <", value, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerLessThanOrEqualTo(String value) {
            addCriterion("power_manufacturer <=", value, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerLike(String value) {
            addCriterion("power_manufacturer like", value, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerNotLike(String value) {
            addCriterion("power_manufacturer not like", value, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerIn(List<String> values) {
            addCriterion("power_manufacturer in", values, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerNotIn(List<String> values) {
            addCriterion("power_manufacturer not in", values, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerBetween(String value1, String value2) {
            addCriterion("power_manufacturer between", value1, value2, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andPowerManufacturerNotBetween(String value1, String value2) {
            addCriterion("power_manufacturer not between", value1, value2, "powerManufacturer");
            return (Criteria) this;
        }

        public Criteria andGenerationIsNull() {
            addCriterion("generation is null");
            return (Criteria) this;
        }

        public Criteria andGenerationIsNotNull() {
            addCriterion("generation is not null");
            return (Criteria) this;
        }

        public Criteria andGenerationEqualTo(String value) {
            addCriterion("generation =", value, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationNotEqualTo(String value) {
            addCriterion("generation <>", value, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationGreaterThan(String value) {
            addCriterion("generation >", value, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationGreaterThanOrEqualTo(String value) {
            addCriterion("generation >=", value, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationLessThan(String value) {
            addCriterion("generation <", value, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationLessThanOrEqualTo(String value) {
            addCriterion("generation <=", value, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationLike(String value) {
            addCriterion("generation like", value, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationNotLike(String value) {
            addCriterion("generation not like", value, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationIn(List<String> values) {
            addCriterion("generation in", values, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationNotIn(List<String> values) {
            addCriterion("generation not in", values, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationBetween(String value1, String value2) {
            addCriterion("generation between", value1, value2, "generation");
            return (Criteria) this;
        }

        public Criteria andGenerationNotBetween(String value1, String value2) {
            addCriterion("generation not between", value1, value2, "generation");
            return (Criteria) this;
        }

        public Criteria andBatterysCountIsNull() {
            addCriterion("batterys_count is null");
            return (Criteria) this;
        }

        public Criteria andBatterysCountIsNotNull() {
            addCriterion("batterys_count is not null");
            return (Criteria) this;
        }

        public Criteria andBatterysCountEqualTo(String value) {
            addCriterion("batterys_count =", value, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountNotEqualTo(String value) {
            addCriterion("batterys_count <>", value, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountGreaterThan(String value) {
            addCriterion("batterys_count >", value, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountGreaterThanOrEqualTo(String value) {
            addCriterion("batterys_count >=", value, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountLessThan(String value) {
            addCriterion("batterys_count <", value, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountLessThanOrEqualTo(String value) {
            addCriterion("batterys_count <=", value, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountLike(String value) {
            addCriterion("batterys_count like", value, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountNotLike(String value) {
            addCriterion("batterys_count not like", value, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountIn(List<String> values) {
            addCriterion("batterys_count in", values, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountNotIn(List<String> values) {
            addCriterion("batterys_count not in", values, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountBetween(String value1, String value2) {
            addCriterion("batterys_count between", value1, value2, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andBatterysCountNotBetween(String value1, String value2) {
            addCriterion("batterys_count not between", value1, value2, "batterysCount");
            return (Criteria) this;
        }

        public Criteria andCapacitanceIsNull() {
            addCriterion("capacitance is null");
            return (Criteria) this;
        }

        public Criteria andCapacitanceIsNotNull() {
            addCriterion("capacitance is not null");
            return (Criteria) this;
        }

        public Criteria andCapacitanceEqualTo(String value) {
            addCriterion("capacitance =", value, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceNotEqualTo(String value) {
            addCriterion("capacitance <>", value, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceGreaterThan(String value) {
            addCriterion("capacitance >", value, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceGreaterThanOrEqualTo(String value) {
            addCriterion("capacitance >=", value, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceLessThan(String value) {
            addCriterion("capacitance <", value, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceLessThanOrEqualTo(String value) {
            addCriterion("capacitance <=", value, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceLike(String value) {
            addCriterion("capacitance like", value, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceNotLike(String value) {
            addCriterion("capacitance not like", value, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceIn(List<String> values) {
            addCriterion("capacitance in", values, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceNotIn(List<String> values) {
            addCriterion("capacitance not in", values, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceBetween(String value1, String value2) {
            addCriterion("capacitance between", value1, value2, "capacitance");
            return (Criteria) this;
        }

        public Criteria andCapacitanceNotBetween(String value1, String value2) {
            addCriterion("capacitance not between", value1, value2, "capacitance");
            return (Criteria) this;
        }

        public Criteria andTotalElectricIsNull() {
            addCriterion("total_electric is null");
            return (Criteria) this;
        }

        public Criteria andTotalElectricIsNotNull() {
            addCriterion("total_electric is not null");
            return (Criteria) this;
        }

        public Criteria andTotalElectricEqualTo(String value) {
            addCriterion("total_electric =", value, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricNotEqualTo(String value) {
            addCriterion("total_electric <>", value, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricGreaterThan(String value) {
            addCriterion("total_electric >", value, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricGreaterThanOrEqualTo(String value) {
            addCriterion("total_electric >=", value, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricLessThan(String value) {
            addCriterion("total_electric <", value, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricLessThanOrEqualTo(String value) {
            addCriterion("total_electric <=", value, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricLike(String value) {
            addCriterion("total_electric like", value, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricNotLike(String value) {
            addCriterion("total_electric not like", value, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricIn(List<String> values) {
            addCriterion("total_electric in", values, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricNotIn(List<String> values) {
            addCriterion("total_electric not in", values, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricBetween(String value1, String value2) {
            addCriterion("total_electric between", value1, value2, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andTotalElectricNotBetween(String value1, String value2) {
            addCriterion("total_electric not between", value1, value2, "totalElectric");
            return (Criteria) this;
        }

        public Criteria andDurationIsNull() {
            addCriterion("duration is null");
            return (Criteria) this;
        }

        public Criteria andDurationIsNotNull() {
            addCriterion("duration is not null");
            return (Criteria) this;
        }

        public Criteria andDurationEqualTo(String value) {
            addCriterion("duration =", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotEqualTo(String value) {
            addCriterion("duration <>", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationGreaterThan(String value) {
            addCriterion("duration >", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationGreaterThanOrEqualTo(String value) {
            addCriterion("duration >=", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLessThan(String value) {
            addCriterion("duration <", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLessThanOrEqualTo(String value) {
            addCriterion("duration <=", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLike(String value) {
            addCriterion("duration like", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotLike(String value) {
            addCriterion("duration not like", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationIn(List<String> values) {
            addCriterion("duration in", values, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotIn(List<String> values) {
            addCriterion("duration not in", values, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationBetween(String value1, String value2) {
            addCriterion("duration between", value1, value2, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotBetween(String value1, String value2) {
            addCriterion("duration not between", value1, value2, "duration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationIsNull() {
            addCriterion("warnning_duration is null");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationIsNotNull() {
            addCriterion("warnning_duration is not null");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationEqualTo(String value) {
            addCriterion("warnning_duration =", value, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationNotEqualTo(String value) {
            addCriterion("warnning_duration <>", value, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationGreaterThan(String value) {
            addCriterion("warnning_duration >", value, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationGreaterThanOrEqualTo(String value) {
            addCriterion("warnning_duration >=", value, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationLessThan(String value) {
            addCriterion("warnning_duration <", value, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationLessThanOrEqualTo(String value) {
            addCriterion("warnning_duration <=", value, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationLike(String value) {
            addCriterion("warnning_duration like", value, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationNotLike(String value) {
            addCriterion("warnning_duration not like", value, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationIn(List<String> values) {
            addCriterion("warnning_duration in", values, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationNotIn(List<String> values) {
            addCriterion("warnning_duration not in", values, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationBetween(String value1, String value2) {
            addCriterion("warnning_duration between", value1, value2, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andWarnningDurationNotBetween(String value1, String value2) {
            addCriterion("warnning_duration not between", value1, value2, "warnningDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationIsNull() {
            addCriterion("bill_exec_duration is null");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationIsNotNull() {
            addCriterion("bill_exec_duration is not null");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationEqualTo(String value) {
            addCriterion("bill_exec_duration =", value, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationNotEqualTo(String value) {
            addCriterion("bill_exec_duration <>", value, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationGreaterThan(String value) {
            addCriterion("bill_exec_duration >", value, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationGreaterThanOrEqualTo(String value) {
            addCriterion("bill_exec_duration >=", value, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationLessThan(String value) {
            addCriterion("bill_exec_duration <", value, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationLessThanOrEqualTo(String value) {
            addCriterion("bill_exec_duration <=", value, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationLike(String value) {
            addCriterion("bill_exec_duration like", value, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationNotLike(String value) {
            addCriterion("bill_exec_duration not like", value, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationIn(List<String> values) {
            addCriterion("bill_exec_duration in", values, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationNotIn(List<String> values) {
            addCriterion("bill_exec_duration not in", values, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationBetween(String value1, String value2) {
            addCriterion("bill_exec_duration between", value1, value2, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andBillExecDurationNotBetween(String value1, String value2) {
            addCriterion("bill_exec_duration not between", value1, value2, "billExecDuration");
            return (Criteria) this;
        }

        public Criteria andDepartmentIsNull() {
            addCriterion("department is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIsNotNull() {
            addCriterion("department is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentEqualTo(String value) {
            addCriterion("department =", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotEqualTo(String value) {
            addCriterion("department <>", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentGreaterThan(String value) {
            addCriterion("department >", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentGreaterThanOrEqualTo(String value) {
            addCriterion("department >=", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLessThan(String value) {
            addCriterion("department <", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLessThanOrEqualTo(String value) {
            addCriterion("department <=", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLike(String value) {
            addCriterion("department like", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotLike(String value) {
            addCriterion("department not like", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentIn(List<String> values) {
            addCriterion("department in", values, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotIn(List<String> values) {
            addCriterion("department not in", values, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentBetween(String value1, String value2) {
            addCriterion("department between", value1, value2, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotBetween(String value1, String value2) {
            addCriterion("department not between", value1, value2, "department");
            return (Criteria) this;
        }

        public Criteria andBackTerminalIsNull() {
            addCriterion("back_terminal is null");
            return (Criteria) this;
        }

        public Criteria andBackTerminalIsNotNull() {
            addCriterion("back_terminal is not null");
            return (Criteria) this;
        }

        public Criteria andBackTerminalEqualTo(String value) {
            addCriterion("back_terminal =", value, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalNotEqualTo(String value) {
            addCriterion("back_terminal <>", value, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalGreaterThan(String value) {
            addCriterion("back_terminal >", value, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalGreaterThanOrEqualTo(String value) {
            addCriterion("back_terminal >=", value, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalLessThan(String value) {
            addCriterion("back_terminal <", value, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalLessThanOrEqualTo(String value) {
            addCriterion("back_terminal <=", value, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalLike(String value) {
            addCriterion("back_terminal like", value, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalNotLike(String value) {
            addCriterion("back_terminal not like", value, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalIn(List<String> values) {
            addCriterion("back_terminal in", values, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalNotIn(List<String> values) {
            addCriterion("back_terminal not in", values, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalBetween(String value1, String value2) {
            addCriterion("back_terminal between", value1, value2, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andBackTerminalNotBetween(String value1, String value2) {
            addCriterion("back_terminal not between", value1, value2, "backTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongIsNull() {
            addCriterion("accept_bill_long is null");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongIsNotNull() {
            addCriterion("accept_bill_long is not null");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongEqualTo(String value) {
            addCriterion("accept_bill_long =", value, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongNotEqualTo(String value) {
            addCriterion("accept_bill_long <>", value, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongGreaterThan(String value) {
            addCriterion("accept_bill_long >", value, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongGreaterThanOrEqualTo(String value) {
            addCriterion("accept_bill_long >=", value, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongLessThan(String value) {
            addCriterion("accept_bill_long <", value, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongLessThanOrEqualTo(String value) {
            addCriterion("accept_bill_long <=", value, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongLike(String value) {
            addCriterion("accept_bill_long like", value, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongNotLike(String value) {
            addCriterion("accept_bill_long not like", value, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongIn(List<String> values) {
            addCriterion("accept_bill_long in", values, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongNotIn(List<String> values) {
            addCriterion("accept_bill_long not in", values, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongBetween(String value1, String value2) {
            addCriterion("accept_bill_long between", value1, value2, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andAcceptBillLongNotBetween(String value1, String value2) {
            addCriterion("accept_bill_long not between", value1, value2, "acceptBillLong");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeIsNull() {
            addCriterion("first_feedback_time is null");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeIsNotNull() {
            addCriterion("first_feedback_time is not null");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeEqualTo(String value) {
            addCriterion("first_feedback_time =", value, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeNotEqualTo(String value) {
            addCriterion("first_feedback_time <>", value, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeGreaterThan(String value) {
            addCriterion("first_feedback_time >", value, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeGreaterThanOrEqualTo(String value) {
            addCriterion("first_feedback_time >=", value, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeLessThan(String value) {
            addCriterion("first_feedback_time <", value, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeLessThanOrEqualTo(String value) {
            addCriterion("first_feedback_time <=", value, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeLike(String value) {
            addCriterion("first_feedback_time like", value, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeNotLike(String value) {
            addCriterion("first_feedback_time not like", value, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeIn(List<String> values) {
            addCriterion("first_feedback_time in", values, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeNotIn(List<String> values) {
            addCriterion("first_feedback_time not in", values, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeBetween(String value1, String value2) {
            addCriterion("first_feedback_time between", value1, value2, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andFirstFeedbackTimeNotBetween(String value1, String value2) {
            addCriterion("first_feedback_time not between", value1, value2, "firstFeedbackTime");
            return (Criteria) this;
        }

        public Criteria andOvertimeIsNull() {
            addCriterion("overtime is null");
            return (Criteria) this;
        }

        public Criteria andOvertimeIsNotNull() {
            addCriterion("overtime is not null");
            return (Criteria) this;
        }

        public Criteria andOvertimeEqualTo(String value) {
            addCriterion("overtime =", value, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeNotEqualTo(String value) {
            addCriterion("overtime <>", value, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeGreaterThan(String value) {
            addCriterion("overtime >", value, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeGreaterThanOrEqualTo(String value) {
            addCriterion("overtime >=", value, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeLessThan(String value) {
            addCriterion("overtime <", value, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeLessThanOrEqualTo(String value) {
            addCriterion("overtime <=", value, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeLike(String value) {
            addCriterion("overtime like", value, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeNotLike(String value) {
            addCriterion("overtime not like", value, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeIn(List<String> values) {
            addCriterion("overtime in", values, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeNotIn(List<String> values) {
            addCriterion("overtime not in", values, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeBetween(String value1, String value2) {
            addCriterion("overtime between", value1, value2, "overtime");
            return (Criteria) this;
        }

        public Criteria andOvertimeNotBetween(String value1, String value2) {
            addCriterion("overtime not between", value1, value2, "overtime");
            return (Criteria) this;
        }

        public Criteria andBelongToIsNull() {
            addCriterion("belong_to is null");
            return (Criteria) this;
        }

        public Criteria andBelongToIsNotNull() {
            addCriterion("belong_to is not null");
            return (Criteria) this;
        }

        public Criteria andBelongToEqualTo(String value) {
            addCriterion("belong_to =", value, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToNotEqualTo(String value) {
            addCriterion("belong_to <>", value, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToGreaterThan(String value) {
            addCriterion("belong_to >", value, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToGreaterThanOrEqualTo(String value) {
            addCriterion("belong_to >=", value, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToLessThan(String value) {
            addCriterion("belong_to <", value, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToLessThanOrEqualTo(String value) {
            addCriterion("belong_to <=", value, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToLike(String value) {
            addCriterion("belong_to like", value, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToNotLike(String value) {
            addCriterion("belong_to not like", value, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToIn(List<String> values) {
            addCriterion("belong_to in", values, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToNotIn(List<String> values) {
            addCriterion("belong_to not in", values, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToBetween(String value1, String value2) {
            addCriterion("belong_to between", value1, value2, "belongTo");
            return (Criteria) this;
        }

        public Criteria andBelongToNotBetween(String value1, String value2) {
            addCriterion("belong_to not between", value1, value2, "belongTo");
            return (Criteria) this;
        }

        public Criteria andConfirmContentIsNull() {
            addCriterion("confirm_content is null");
            return (Criteria) this;
        }

        public Criteria andConfirmContentIsNotNull() {
            addCriterion("confirm_content is not null");
            return (Criteria) this;
        }

        public Criteria andConfirmContentEqualTo(String value) {
            addCriterion("confirm_content =", value, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentNotEqualTo(String value) {
            addCriterion("confirm_content <>", value, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentGreaterThan(String value) {
            addCriterion("confirm_content >", value, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentGreaterThanOrEqualTo(String value) {
            addCriterion("confirm_content >=", value, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentLessThan(String value) {
            addCriterion("confirm_content <", value, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentLessThanOrEqualTo(String value) {
            addCriterion("confirm_content <=", value, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentLike(String value) {
            addCriterion("confirm_content like", value, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentNotLike(String value) {
            addCriterion("confirm_content not like", value, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentIn(List<String> values) {
            addCriterion("confirm_content in", values, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentNotIn(List<String> values) {
            addCriterion("confirm_content not in", values, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentBetween(String value1, String value2) {
            addCriterion("confirm_content between", value1, value2, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andConfirmContentNotBetween(String value1, String value2) {
            addCriterion("confirm_content not between", value1, value2, "confirmContent");
            return (Criteria) this;
        }

        public Criteria andManagerIsNull() {
            addCriterion("manager is null");
            return (Criteria) this;
        }

        public Criteria andManagerIsNotNull() {
            addCriterion("manager is not null");
            return (Criteria) this;
        }

        public Criteria andManagerEqualTo(String value) {
            addCriterion("manager =", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotEqualTo(String value) {
            addCriterion("manager <>", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerGreaterThan(String value) {
            addCriterion("manager >", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerGreaterThanOrEqualTo(String value) {
            addCriterion("manager >=", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLessThan(String value) {
            addCriterion("manager <", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLessThanOrEqualTo(String value) {
            addCriterion("manager <=", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerLike(String value) {
            addCriterion("manager like", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotLike(String value) {
            addCriterion("manager not like", value, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerIn(List<String> values) {
            addCriterion("manager in", values, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotIn(List<String> values) {
            addCriterion("manager not in", values, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerBetween(String value1, String value2) {
            addCriterion("manager between", value1, value2, "manager");
            return (Criteria) this;
        }

        public Criteria andManagerNotBetween(String value1, String value2) {
            addCriterion("manager not between", value1, value2, "manager");
            return (Criteria) this;
        }

        public Criteria andOriginateIsNull() {
            addCriterion("originate is null");
            return (Criteria) this;
        }

        public Criteria andOriginateIsNotNull() {
            addCriterion("originate is not null");
            return (Criteria) this;
        }

        public Criteria andOriginateEqualTo(String value) {
            addCriterion("originate =", value, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateNotEqualTo(String value) {
            addCriterion("originate <>", value, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateGreaterThan(String value) {
            addCriterion("originate >", value, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateGreaterThanOrEqualTo(String value) {
            addCriterion("originate >=", value, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateLessThan(String value) {
            addCriterion("originate <", value, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateLessThanOrEqualTo(String value) {
            addCriterion("originate <=", value, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateLike(String value) {
            addCriterion("originate like", value, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateNotLike(String value) {
            addCriterion("originate not like", value, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateIn(List<String> values) {
            addCriterion("originate in", values, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateNotIn(List<String> values) {
            addCriterion("originate not in", values, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateBetween(String value1, String value2) {
            addCriterion("originate between", value1, value2, "originate");
            return (Criteria) this;
        }

        public Criteria andOriginateNotBetween(String value1, String value2) {
            addCriterion("originate not between", value1, value2, "originate");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalIsNull() {
            addCriterion("accept_terminal is null");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalIsNotNull() {
            addCriterion("accept_terminal is not null");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalEqualTo(String value) {
            addCriterion("accept_terminal =", value, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalNotEqualTo(String value) {
            addCriterion("accept_terminal <>", value, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalGreaterThan(String value) {
            addCriterion("accept_terminal >", value, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalGreaterThanOrEqualTo(String value) {
            addCriterion("accept_terminal >=", value, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalLessThan(String value) {
            addCriterion("accept_terminal <", value, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalLessThanOrEqualTo(String value) {
            addCriterion("accept_terminal <=", value, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalLike(String value) {
            addCriterion("accept_terminal like", value, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalNotLike(String value) {
            addCriterion("accept_terminal not like", value, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalIn(List<String> values) {
            addCriterion("accept_terminal in", values, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalNotIn(List<String> values) {
            addCriterion("accept_terminal not in", values, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalBetween(String value1, String value2) {
            addCriterion("accept_terminal between", value1, value2, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andAcceptTerminalNotBetween(String value1, String value2) {
            addCriterion("accept_terminal not between", value1, value2, "acceptTerminal");
            return (Criteria) this;
        }

        public Criteria andSigninTimeIsNull() {
            addCriterion("signin_time is null");
            return (Criteria) this;
        }

        public Criteria andSigninTimeIsNotNull() {
            addCriterion("signin_time is not null");
            return (Criteria) this;
        }

        public Criteria andSigninTimeEqualTo(String value) {
            addCriterion("signin_time =", value, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeNotEqualTo(String value) {
            addCriterion("signin_time <>", value, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeGreaterThan(String value) {
            addCriterion("signin_time >", value, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeGreaterThanOrEqualTo(String value) {
            addCriterion("signin_time >=", value, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeLessThan(String value) {
            addCriterion("signin_time <", value, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeLessThanOrEqualTo(String value) {
            addCriterion("signin_time <=", value, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeLike(String value) {
            addCriterion("signin_time like", value, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeNotLike(String value) {
            addCriterion("signin_time not like", value, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeIn(List<String> values) {
            addCriterion("signin_time in", values, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeNotIn(List<String> values) {
            addCriterion("signin_time not in", values, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeBetween(String value1, String value2) {
            addCriterion("signin_time between", value1, value2, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninTimeNotBetween(String value1, String value2) {
            addCriterion("signin_time not between", value1, value2, "signinTime");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeIsNull() {
            addCriterion("signin_longitude is null");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeIsNotNull() {
            addCriterion("signin_longitude is not null");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeEqualTo(String value) {
            addCriterion("signin_longitude =", value, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeNotEqualTo(String value) {
            addCriterion("signin_longitude <>", value, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeGreaterThan(String value) {
            addCriterion("signin_longitude >", value, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeGreaterThanOrEqualTo(String value) {
            addCriterion("signin_longitude >=", value, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeLessThan(String value) {
            addCriterion("signin_longitude <", value, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeLessThanOrEqualTo(String value) {
            addCriterion("signin_longitude <=", value, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeLike(String value) {
            addCriterion("signin_longitude like", value, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeNotLike(String value) {
            addCriterion("signin_longitude not like", value, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeIn(List<String> values) {
            addCriterion("signin_longitude in", values, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeNotIn(List<String> values) {
            addCriterion("signin_longitude not in", values, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeBetween(String value1, String value2) {
            addCriterion("signin_longitude between", value1, value2, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLongitudeNotBetween(String value1, String value2) {
            addCriterion("signin_longitude not between", value1, value2, "signinLongitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeIsNull() {
            addCriterion("signin_latitude is null");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeIsNotNull() {
            addCriterion("signin_latitude is not null");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeEqualTo(String value) {
            addCriterion("signin_latitude =", value, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeNotEqualTo(String value) {
            addCriterion("signin_latitude <>", value, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeGreaterThan(String value) {
            addCriterion("signin_latitude >", value, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeGreaterThanOrEqualTo(String value) {
            addCriterion("signin_latitude >=", value, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeLessThan(String value) {
            addCriterion("signin_latitude <", value, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeLessThanOrEqualTo(String value) {
            addCriterion("signin_latitude <=", value, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeLike(String value) {
            addCriterion("signin_latitude like", value, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeNotLike(String value) {
            addCriterion("signin_latitude not like", value, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeIn(List<String> values) {
            addCriterion("signin_latitude in", values, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeNotIn(List<String> values) {
            addCriterion("signin_latitude not in", values, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeBetween(String value1, String value2) {
            addCriterion("signin_latitude between", value1, value2, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andSigninLatitudeNotBetween(String value1, String value2) {
            addCriterion("signin_latitude not between", value1, value2, "signinLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeIsNull() {
            addCriterion("fsu_longitude is null");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeIsNotNull() {
            addCriterion("fsu_longitude is not null");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeEqualTo(String value) {
            addCriterion("fsu_longitude =", value, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeNotEqualTo(String value) {
            addCriterion("fsu_longitude <>", value, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeGreaterThan(String value) {
            addCriterion("fsu_longitude >", value, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeGreaterThanOrEqualTo(String value) {
            addCriterion("fsu_longitude >=", value, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeLessThan(String value) {
            addCriterion("fsu_longitude <", value, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeLessThanOrEqualTo(String value) {
            addCriterion("fsu_longitude <=", value, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeLike(String value) {
            addCriterion("fsu_longitude like", value, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeNotLike(String value) {
            addCriterion("fsu_longitude not like", value, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeIn(List<String> values) {
            addCriterion("fsu_longitude in", values, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeNotIn(List<String> values) {
            addCriterion("fsu_longitude not in", values, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeBetween(String value1, String value2) {
            addCriterion("fsu_longitude between", value1, value2, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLongitudeNotBetween(String value1, String value2) {
            addCriterion("fsu_longitude not between", value1, value2, "fsuLongitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeIsNull() {
            addCriterion("fsu_latitude is null");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeIsNotNull() {
            addCriterion("fsu_latitude is not null");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeEqualTo(String value) {
            addCriterion("fsu_latitude =", value, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeNotEqualTo(String value) {
            addCriterion("fsu_latitude <>", value, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeGreaterThan(String value) {
            addCriterion("fsu_latitude >", value, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeGreaterThanOrEqualTo(String value) {
            addCriterion("fsu_latitude >=", value, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeLessThan(String value) {
            addCriterion("fsu_latitude <", value, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeLessThanOrEqualTo(String value) {
            addCriterion("fsu_latitude <=", value, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeLike(String value) {
            addCriterion("fsu_latitude like", value, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeNotLike(String value) {
            addCriterion("fsu_latitude not like", value, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeIn(List<String> values) {
            addCriterion("fsu_latitude in", values, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeNotIn(List<String> values) {
            addCriterion("fsu_latitude not in", values, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeBetween(String value1, String value2) {
            addCriterion("fsu_latitude between", value1, value2, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andFsuLatitudeNotBetween(String value1, String value2) {
            addCriterion("fsu_latitude not between", value1, value2, "fsuLatitude");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillIsNull() {
            addCriterion("power_generation_bill is null");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillIsNotNull() {
            addCriterion("power_generation_bill is not null");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillEqualTo(String value) {
            addCriterion("power_generation_bill =", value, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillNotEqualTo(String value) {
            addCriterion("power_generation_bill <>", value, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillGreaterThan(String value) {
            addCriterion("power_generation_bill >", value, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillGreaterThanOrEqualTo(String value) {
            addCriterion("power_generation_bill >=", value, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillLessThan(String value) {
            addCriterion("power_generation_bill <", value, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillLessThanOrEqualTo(String value) {
            addCriterion("power_generation_bill <=", value, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillLike(String value) {
            addCriterion("power_generation_bill like", value, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillNotLike(String value) {
            addCriterion("power_generation_bill not like", value, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillIn(List<String> values) {
            addCriterion("power_generation_bill in", values, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillNotIn(List<String> values) {
            addCriterion("power_generation_bill not in", values, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillBetween(String value1, String value2) {
            addCriterion("power_generation_bill between", value1, value2, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andPowerGenerationBillNotBetween(String value1, String value2) {
            addCriterion("power_generation_bill not between", value1, value2, "powerGenerationBill");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeIsNull() {
            addCriterion("breakdown_type is null");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeIsNotNull() {
            addCriterion("breakdown_type is not null");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeEqualTo(String value) {
            addCriterion("breakdown_type =", value, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeNotEqualTo(String value) {
            addCriterion("breakdown_type <>", value, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeGreaterThan(String value) {
            addCriterion("breakdown_type >", value, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeGreaterThanOrEqualTo(String value) {
            addCriterion("breakdown_type >=", value, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeLessThan(String value) {
            addCriterion("breakdown_type <", value, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeLessThanOrEqualTo(String value) {
            addCriterion("breakdown_type <=", value, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeLike(String value) {
            addCriterion("breakdown_type like", value, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeNotLike(String value) {
            addCriterion("breakdown_type not like", value, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeIn(List<String> values) {
            addCriterion("breakdown_type in", values, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeNotIn(List<String> values) {
            addCriterion("breakdown_type not in", values, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeBetween(String value1, String value2) {
            addCriterion("breakdown_type between", value1, value2, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andBreakdownTypeNotBetween(String value1, String value2) {
            addCriterion("breakdown_type not between", value1, value2, "breakdownType");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageIsNull() {
            addCriterion("limit_voltage is null");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageIsNotNull() {
            addCriterion("limit_voltage is not null");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageEqualTo(String value) {
            addCriterion("limit_voltage =", value, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageNotEqualTo(String value) {
            addCriterion("limit_voltage <>", value, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageGreaterThan(String value) {
            addCriterion("limit_voltage >", value, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageGreaterThanOrEqualTo(String value) {
            addCriterion("limit_voltage >=", value, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageLessThan(String value) {
            addCriterion("limit_voltage <", value, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageLessThanOrEqualTo(String value) {
            addCriterion("limit_voltage <=", value, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageLike(String value) {
            addCriterion("limit_voltage like", value, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageNotLike(String value) {
            addCriterion("limit_voltage not like", value, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageIn(List<String> values) {
            addCriterion("limit_voltage in", values, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageNotIn(List<String> values) {
            addCriterion("limit_voltage not in", values, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageBetween(String value1, String value2) {
            addCriterion("limit_voltage between", value1, value2, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andLimitVoltageNotBetween(String value1, String value2) {
            addCriterion("limit_voltage not between", value1, value2, "limitVoltage");
            return (Criteria) this;
        }

        public Criteria andOperatorsIsNull() {
            addCriterion("operators is null");
            return (Criteria) this;
        }

        public Criteria andOperatorsIsNotNull() {
            addCriterion("operators is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorsEqualTo(String value) {
            addCriterion("operators =", value, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsNotEqualTo(String value) {
            addCriterion("operators <>", value, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsGreaterThan(String value) {
            addCriterion("operators >", value, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsGreaterThanOrEqualTo(String value) {
            addCriterion("operators >=", value, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsLessThan(String value) {
            addCriterion("operators <", value, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsLessThanOrEqualTo(String value) {
            addCriterion("operators <=", value, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsLike(String value) {
            addCriterion("operators like", value, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsNotLike(String value) {
            addCriterion("operators not like", value, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsIn(List<String> values) {
            addCriterion("operators in", values, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsNotIn(List<String> values) {
            addCriterion("operators not in", values, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsBetween(String value1, String value2) {
            addCriterion("operators between", value1, value2, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsNotBetween(String value1, String value2) {
            addCriterion("operators not between", value1, value2, "operators");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillIsNull() {
            addCriterion("operators_bill is null");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillIsNotNull() {
            addCriterion("operators_bill is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillEqualTo(String value) {
            addCriterion("operators_bill =", value, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillNotEqualTo(String value) {
            addCriterion("operators_bill <>", value, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillGreaterThan(String value) {
            addCriterion("operators_bill >", value, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillGreaterThanOrEqualTo(String value) {
            addCriterion("operators_bill >=", value, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillLessThan(String value) {
            addCriterion("operators_bill <", value, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillLessThanOrEqualTo(String value) {
            addCriterion("operators_bill <=", value, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillLike(String value) {
            addCriterion("operators_bill like", value, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillNotLike(String value) {
            addCriterion("operators_bill not like", value, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillIn(List<String> values) {
            addCriterion("operators_bill in", values, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillNotIn(List<String> values) {
            addCriterion("operators_bill not in", values, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillBetween(String value1, String value2) {
            addCriterion("operators_bill between", value1, value2, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andOperatorsBillNotBetween(String value1, String value2) {
            addCriterion("operators_bill not between", value1, value2, "operatorsBill");
            return (Criteria) this;
        }

        public Criteria andMobileNameIsNull() {
            addCriterion("mobile_name is null");
            return (Criteria) this;
        }

        public Criteria andMobileNameIsNotNull() {
            addCriterion("mobile_name is not null");
            return (Criteria) this;
        }

        public Criteria andMobileNameEqualTo(String value) {
            addCriterion("mobile_name =", value, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameNotEqualTo(String value) {
            addCriterion("mobile_name <>", value, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameGreaterThan(String value) {
            addCriterion("mobile_name >", value, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameGreaterThanOrEqualTo(String value) {
            addCriterion("mobile_name >=", value, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameLessThan(String value) {
            addCriterion("mobile_name <", value, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameLessThanOrEqualTo(String value) {
            addCriterion("mobile_name <=", value, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameLike(String value) {
            addCriterion("mobile_name like", value, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameNotLike(String value) {
            addCriterion("mobile_name not like", value, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameIn(List<String> values) {
            addCriterion("mobile_name in", values, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameNotIn(List<String> values) {
            addCriterion("mobile_name not in", values, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameBetween(String value1, String value2) {
            addCriterion("mobile_name between", value1, value2, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileNameNotBetween(String value1, String value2) {
            addCriterion("mobile_name not between", value1, value2, "mobileName");
            return (Criteria) this;
        }

        public Criteria andMobileCodeIsNull() {
            addCriterion("mobile_code is null");
            return (Criteria) this;
        }

        public Criteria andMobileCodeIsNotNull() {
            addCriterion("mobile_code is not null");
            return (Criteria) this;
        }

        public Criteria andMobileCodeEqualTo(String value) {
            addCriterion("mobile_code =", value, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeNotEqualTo(String value) {
            addCriterion("mobile_code <>", value, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeGreaterThan(String value) {
            addCriterion("mobile_code >", value, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeGreaterThanOrEqualTo(String value) {
            addCriterion("mobile_code >=", value, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeLessThan(String value) {
            addCriterion("mobile_code <", value, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeLessThanOrEqualTo(String value) {
            addCriterion("mobile_code <=", value, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeLike(String value) {
            addCriterion("mobile_code like", value, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeNotLike(String value) {
            addCriterion("mobile_code not like", value, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeIn(List<String> values) {
            addCriterion("mobile_code in", values, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeNotIn(List<String> values) {
            addCriterion("mobile_code not in", values, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeBetween(String value1, String value2) {
            addCriterion("mobile_code between", value1, value2, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andMobileCodeNotBetween(String value1, String value2) {
            addCriterion("mobile_code not between", value1, value2, "mobileCode");
            return (Criteria) this;
        }

        public Criteria andUnicomNameIsNull() {
            addCriterion("unicom_name is null");
            return (Criteria) this;
        }

        public Criteria andUnicomNameIsNotNull() {
            addCriterion("unicom_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnicomNameEqualTo(String value) {
            addCriterion("unicom_name =", value, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameNotEqualTo(String value) {
            addCriterion("unicom_name <>", value, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameGreaterThan(String value) {
            addCriterion("unicom_name >", value, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameGreaterThanOrEqualTo(String value) {
            addCriterion("unicom_name >=", value, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameLessThan(String value) {
            addCriterion("unicom_name <", value, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameLessThanOrEqualTo(String value) {
            addCriterion("unicom_name <=", value, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameLike(String value) {
            addCriterion("unicom_name like", value, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameNotLike(String value) {
            addCriterion("unicom_name not like", value, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameIn(List<String> values) {
            addCriterion("unicom_name in", values, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameNotIn(List<String> values) {
            addCriterion("unicom_name not in", values, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameBetween(String value1, String value2) {
            addCriterion("unicom_name between", value1, value2, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomNameNotBetween(String value1, String value2) {
            addCriterion("unicom_name not between", value1, value2, "unicomName");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeIsNull() {
            addCriterion("unicom_code is null");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeIsNotNull() {
            addCriterion("unicom_code is not null");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeEqualTo(String value) {
            addCriterion("unicom_code =", value, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeNotEqualTo(String value) {
            addCriterion("unicom_code <>", value, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeGreaterThan(String value) {
            addCriterion("unicom_code >", value, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeGreaterThanOrEqualTo(String value) {
            addCriterion("unicom_code >=", value, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeLessThan(String value) {
            addCriterion("unicom_code <", value, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeLessThanOrEqualTo(String value) {
            addCriterion("unicom_code <=", value, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeLike(String value) {
            addCriterion("unicom_code like", value, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeNotLike(String value) {
            addCriterion("unicom_code not like", value, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeIn(List<String> values) {
            addCriterion("unicom_code in", values, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeNotIn(List<String> values) {
            addCriterion("unicom_code not in", values, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeBetween(String value1, String value2) {
            addCriterion("unicom_code between", value1, value2, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andUnicomCodeNotBetween(String value1, String value2) {
            addCriterion("unicom_code not between", value1, value2, "unicomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomNameIsNull() {
            addCriterion("telecom_name is null");
            return (Criteria) this;
        }

        public Criteria andTelecomNameIsNotNull() {
            addCriterion("telecom_name is not null");
            return (Criteria) this;
        }

        public Criteria andTelecomNameEqualTo(String value) {
            addCriterion("telecom_name =", value, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameNotEqualTo(String value) {
            addCriterion("telecom_name <>", value, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameGreaterThan(String value) {
            addCriterion("telecom_name >", value, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameGreaterThanOrEqualTo(String value) {
            addCriterion("telecom_name >=", value, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameLessThan(String value) {
            addCriterion("telecom_name <", value, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameLessThanOrEqualTo(String value) {
            addCriterion("telecom_name <=", value, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameLike(String value) {
            addCriterion("telecom_name like", value, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameNotLike(String value) {
            addCriterion("telecom_name not like", value, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameIn(List<String> values) {
            addCriterion("telecom_name in", values, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameNotIn(List<String> values) {
            addCriterion("telecom_name not in", values, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameBetween(String value1, String value2) {
            addCriterion("telecom_name between", value1, value2, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomNameNotBetween(String value1, String value2) {
            addCriterion("telecom_name not between", value1, value2, "telecomName");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeIsNull() {
            addCriterion("telecom_code is null");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeIsNotNull() {
            addCriterion("telecom_code is not null");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeEqualTo(String value) {
            addCriterion("telecom_code =", value, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeNotEqualTo(String value) {
            addCriterion("telecom_code <>", value, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeGreaterThan(String value) {
            addCriterion("telecom_code >", value, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeGreaterThanOrEqualTo(String value) {
            addCriterion("telecom_code >=", value, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeLessThan(String value) {
            addCriterion("telecom_code <", value, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeLessThanOrEqualTo(String value) {
            addCriterion("telecom_code <=", value, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeLike(String value) {
            addCriterion("telecom_code like", value, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeNotLike(String value) {
            addCriterion("telecom_code not like", value, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeIn(List<String> values) {
            addCriterion("telecom_code in", values, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeNotIn(List<String> values) {
            addCriterion("telecom_code not in", values, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeBetween(String value1, String value2) {
            addCriterion("telecom_code between", value1, value2, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andTelecomCodeNotBetween(String value1, String value2) {
            addCriterion("telecom_code not between", value1, value2, "telecomCode");
            return (Criteria) this;
        }

        public Criteria andBatchIdIsNull() {
            addCriterion("batch_id is null");
            return (Criteria) this;
        }

        public Criteria andBatchIdIsNotNull() {
            addCriterion("batch_id is not null");
            return (Criteria) this;
        }

        public Criteria andBatchIdEqualTo(String value) {
            addCriterion("batch_id =", value, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdNotEqualTo(String value) {
            addCriterion("batch_id <>", value, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdGreaterThan(String value) {
            addCriterion("batch_id >", value, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdGreaterThanOrEqualTo(String value) {
            addCriterion("batch_id >=", value, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdLessThan(String value) {
            addCriterion("batch_id <", value, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdLessThanOrEqualTo(String value) {
            addCriterion("batch_id <=", value, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdLike(String value) {
            addCriterion("batch_id like", value, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdNotLike(String value) {
            addCriterion("batch_id not like", value, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdIn(List<String> values) {
            addCriterion("batch_id in", values, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdNotIn(List<String> values) {
            addCriterion("batch_id not in", values, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdBetween(String value1, String value2) {
            addCriterion("batch_id between", value1, value2, "batchId");
            return (Criteria) this;
        }

        public Criteria andBatchIdNotBetween(String value1, String value2) {
            addCriterion("batch_id not between", value1, value2, "batchId");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("`status` is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("`status` is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("`status` =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("`status` <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("`status` >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("`status` >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("`status` <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("`status` <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("`status` like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("`status` not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("`status` in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("`status` not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("`status` between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("`status` not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNull() {
            addCriterion("create_by is null");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNotNull() {
            addCriterion("create_by is not null");
            return (Criteria) this;
        }

        public Criteria andCreateByEqualTo(String value) {
            addCriterion("create_by =", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotEqualTo(String value) {
            addCriterion("create_by <>", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThan(String value) {
            addCriterion("create_by >", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThanOrEqualTo(String value) {
            addCriterion("create_by >=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThan(String value) {
            addCriterion("create_by <", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThanOrEqualTo(String value) {
            addCriterion("create_by <=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLike(String value) {
            addCriterion("create_by like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotLike(String value) {
            addCriterion("create_by not like", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByIn(List<String> values) {
            addCriterion("create_by in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotIn(List<String> values) {
            addCriterion("create_by not in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByBetween(String value1, String value2) {
            addCriterion("create_by between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotBetween(String value1, String value2) {
            addCriterion("create_by not between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNull() {
            addCriterion("update_by is null");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNotNull() {
            addCriterion("update_by is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateByEqualTo(String value) {
            addCriterion("update_by =", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotEqualTo(String value) {
            addCriterion("update_by <>", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThan(String value) {
            addCriterion("update_by >", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThanOrEqualTo(String value) {
            addCriterion("update_by >=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThan(String value) {
            addCriterion("update_by <", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThanOrEqualTo(String value) {
            addCriterion("update_by <=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLike(String value) {
            addCriterion("update_by like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotLike(String value) {
            addCriterion("update_by not like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIn(List<String> values) {
            addCriterion("update_by in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotIn(List<String> values) {
            addCriterion("update_by not in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByBetween(String value1, String value2) {
            addCriterion("update_by between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotBetween(String value1, String value2) {
            addCriterion("update_by not between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("remarks is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("remarks is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("remarks =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("remarks <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("remarks >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("remarks >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("remarks <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("remarks <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("remarks like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("remarks not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("remarks in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("remarks not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("remarks between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("remarks not between", value1, value2, "remarks");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}