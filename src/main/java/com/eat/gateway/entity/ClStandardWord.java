package com.eat.gateway.entity;

import java.io.Serializable;

/**
 * cl_standard_word
 * @author 
 */
public class ClStandardWord implements Serializable {
    private String id;

    private String standardWord;

    private String delfag;

    private byte[] standardPicture;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStandardWord() {
        return standardWord;
    }

    public void setStandardWord(String standardWord) {
        this.standardWord = standardWord;
    }

    public String getDelfag() {
        return delfag;
    }

    public void setDelfag(String delfag) {
        this.delfag = delfag;
    }

    public byte[] getStandardPicture() {
        return standardPicture;
    }

    public void setStandardPicture(byte[] standardPicture) {
        this.standardPicture = standardPicture;
    }
}