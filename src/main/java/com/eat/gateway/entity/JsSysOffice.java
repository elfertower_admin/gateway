package com.eat.gateway.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * js_sys_office
 * @author 
 */
public class JsSysOffice implements Serializable {
    /**
     * 机构编码
     */
    private String officeCode;

    /**
     * 父级编号
     */
    private String parentCode;

    /**
     * 所有父级编号
     */
    private String parentCodes;

    /**
     * 本级排序号（升序）
     */
    private Long treeSort;

    /**
     * 所有级别排序号
     */
    private String treeSorts;

    /**
     * 是否最末级
     */
    private String treeLeaf;

    /**
     * 层次级别
     */
    private Short treeLevel;

    /**
     * 全节点名
     */
    private String treeNames;

    /**
     * 机构代码
     */
    private String viewCode;

    /**
     * 机构名称
     */
    private String officeName;

    /**
     * 机构全称
     */
    private String fullName;

    /**
     * 机构类型
     */
    private String officeType;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 办公电话
     */
    private String phone;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 邮政编码
     */
    private String zipCode;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 状态（0正常 1删除 2停用）
     */
    private String status;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 租户代码
     */
    private String corpCode;

    /**
     * 租户名称
     */
    private String corpName;

    /**
     * 扩展 String 1
     */
    private String extendS1;

    /**
     * 扩展 String 2
     */
    private String extendS2;

    /**
     * 扩展 String 3
     */
    private String extendS3;

    /**
     * 扩展 String 4
     */
    private String extendS4;

    /**
     * 扩展 String 5
     */
    private String extendS5;

    /**
     * 扩展 String 6
     */
    private String extendS6;

    /**
     * 扩展 String 7
     */
    private String extendS7;

    /**
     * 扩展 String 8
     */
    private String extendS8;

    /**
     * 扩展 Integer 1
     */
    private BigDecimal extendI1;

    /**
     * 扩展 Integer 2
     */
    private BigDecimal extendI2;

    /**
     * 扩展 Integer 3
     */
    private BigDecimal extendI3;

    /**
     * 扩展 Integer 4
     */
    private BigDecimal extendI4;

    /**
     * 扩展 Float 1
     */
    private BigDecimal extendF1;

    /**
     * 扩展 Float 2
     */
    private BigDecimal extendF2;

    /**
     * 扩展 Float 3
     */
    private BigDecimal extendF3;

    /**
     * 扩展 Float 4
     */
    private BigDecimal extendF4;

    /**
     * 扩展 Date 1
     */
    private Date extendD1;

    /**
     * 扩展 Date 2
     */
    private Date extendD2;

    /**
     * 扩展 Date 3
     */
    private Date extendD3;

    /**
     * 扩展 Date 4
     */
    private Date extendD4;

    private static final long serialVersionUID = 1L;

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getParentCodes() {
        return parentCodes;
    }

    public void setParentCodes(String parentCodes) {
        this.parentCodes = parentCodes;
    }

    public Long getTreeSort() {
        return treeSort;
    }

    public void setTreeSort(Long treeSort) {
        this.treeSort = treeSort;
    }

    public String getTreeSorts() {
        return treeSorts;
    }

    public void setTreeSorts(String treeSorts) {
        this.treeSorts = treeSorts;
    }

    public String getTreeLeaf() {
        return treeLeaf;
    }

    public void setTreeLeaf(String treeLeaf) {
        this.treeLeaf = treeLeaf;
    }

    public Short getTreeLevel() {
        return treeLevel;
    }

    public void setTreeLevel(Short treeLevel) {
        this.treeLevel = treeLevel;
    }

    public String getTreeNames() {
        return treeNames;
    }

    public void setTreeNames(String treeNames) {
        this.treeNames = treeNames;
    }

    public String getViewCode() {
        return viewCode;
    }

    public void setViewCode(String viewCode) {
        this.viewCode = viewCode;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOfficeType() {
        return officeType;
    }

    public void setOfficeType(String officeType) {
        this.officeType = officeType;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCorpCode() {
        return corpCode;
    }

    public void setCorpCode(String corpCode) {
        this.corpCode = corpCode;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getExtendS1() {
        return extendS1;
    }

    public void setExtendS1(String extendS1) {
        this.extendS1 = extendS1;
    }

    public String getExtendS2() {
        return extendS2;
    }

    public void setExtendS2(String extendS2) {
        this.extendS2 = extendS2;
    }

    public String getExtendS3() {
        return extendS3;
    }

    public void setExtendS3(String extendS3) {
        this.extendS3 = extendS3;
    }

    public String getExtendS4() {
        return extendS4;
    }

    public void setExtendS4(String extendS4) {
        this.extendS4 = extendS4;
    }

    public String getExtendS5() {
        return extendS5;
    }

    public void setExtendS5(String extendS5) {
        this.extendS5 = extendS5;
    }

    public String getExtendS6() {
        return extendS6;
    }

    public void setExtendS6(String extendS6) {
        this.extendS6 = extendS6;
    }

    public String getExtendS7() {
        return extendS7;
    }

    public void setExtendS7(String extendS7) {
        this.extendS7 = extendS7;
    }

    public String getExtendS8() {
        return extendS8;
    }

    public void setExtendS8(String extendS8) {
        this.extendS8 = extendS8;
    }

    public BigDecimal getExtendI1() {
        return extendI1;
    }

    public void setExtendI1(BigDecimal extendI1) {
        this.extendI1 = extendI1;
    }

    public BigDecimal getExtendI2() {
        return extendI2;
    }

    public void setExtendI2(BigDecimal extendI2) {
        this.extendI2 = extendI2;
    }

    public BigDecimal getExtendI3() {
        return extendI3;
    }

    public void setExtendI3(BigDecimal extendI3) {
        this.extendI3 = extendI3;
    }

    public BigDecimal getExtendI4() {
        return extendI4;
    }

    public void setExtendI4(BigDecimal extendI4) {
        this.extendI4 = extendI4;
    }

    public BigDecimal getExtendF1() {
        return extendF1;
    }

    public void setExtendF1(BigDecimal extendF1) {
        this.extendF1 = extendF1;
    }

    public BigDecimal getExtendF2() {
        return extendF2;
    }

    public void setExtendF2(BigDecimal extendF2) {
        this.extendF2 = extendF2;
    }

    public BigDecimal getExtendF3() {
        return extendF3;
    }

    public void setExtendF3(BigDecimal extendF3) {
        this.extendF3 = extendF3;
    }

    public BigDecimal getExtendF4() {
        return extendF4;
    }

    public void setExtendF4(BigDecimal extendF4) {
        this.extendF4 = extendF4;
    }

    public Date getExtendD1() {
        return extendD1;
    }

    public void setExtendD1(Date extendD1) {
        this.extendD1 = extendD1;
    }

    public Date getExtendD2() {
        return extendD2;
    }

    public void setExtendD2(Date extendD2) {
        this.extendD2 = extendD2;
    }

    public Date getExtendD3() {
        return extendD3;
    }

    public void setExtendD3(Date extendD3) {
        this.extendD3 = extendD3;
    }

    public Date getExtendD4() {
        return extendD4;
    }

    public void setExtendD4(Date extendD4) {
        this.extendD4 = extendD4;
    }
}