package com.eat.gateway.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * cl_operation_step_args
 * @author 
 */
public class ClOperationStepArgs implements Serializable {
    private String argId;

    private String argType;

    private String argVal;

    private String stepId;

    private Integer argOrder;

    private Date createDate;

    private String createBy;

    private String updateBy;

    private Date updateDate;

    private String remarks;

    private String status;

    private static final long serialVersionUID = 1L;

    public String getArgId() {
        return argId;
    }

    public void setArgId(String argId) {
        this.argId = argId;
    }

    public String getArgType() {
        return argType;
    }

    public void setArgType(String argType) {
        this.argType = argType;
    }

    public String getArgVal() {
        return argVal;
    }

    public void setArgVal(String argVal) {
        this.argVal = argVal;
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public Integer getArgOrder() {
        return argOrder;
    }

    public void setArgOrder(Integer argOrder) {
        this.argOrder = argOrder;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}