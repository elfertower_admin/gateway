package com.eat.gateway.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * js_monitor_notice
 * @author 
 */
public class JsMonitorNotice implements Serializable {
    /**
     * 主键
     */
    private String noticeId;

    /**
     * 上报信息
     */
    private Integer upReport;

    /**
     * 提醒次数
     */
    private Integer noticeTimes;

    /**
     * 工单编号
     */
    private String billCode;

    /**
     * 内容
     */
    private String content;

    /**
     * 机构编码
     */
    private String officeCode;

    /**
     * 接收人
     */
    private String sendeeCode;

    /**
     * 是否发送
     */
    private String isSend;

    /**
     * 发送时间
     */
    private Date sendTime;

    /**
     * 通知类型
0 接单通知
1 回单通知
2 发电通知
     */
    private String noticeType;

    /**
     * 是否发电工单
     */
    private String isPower;

    private Date createDate;

    private String createBy;

    private String updateBy;

    private Date updateDate;

    private String remarks;

    private String status;

    private String loginCode;

    private String userName;

    private static final long serialVersionUID = 1L;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public Integer getUpReport() {
        return upReport;
    }

    public void setUpReport(Integer upReport) {
        this.upReport = upReport;
    }

    public Integer getNoticeTimes() {
        return noticeTimes;
    }

    public void setNoticeTimes(Integer noticeTimes) {
        this.noticeTimes = noticeTimes;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getSendeeCode() {
        return sendeeCode;
    }

    public void setSendeeCode(String sendeeCode) {
        this.sendeeCode = sendeeCode;
    }

    public String getIsSend() {
        return isSend;
    }

    public void setIsSend(String isSend) {
        this.isSend = isSend;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public String getIsPower() {
        return isPower;
    }

    public void setIsPower(String isPower) {
        this.isPower = isPower;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}