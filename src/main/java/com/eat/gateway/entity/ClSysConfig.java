package com.eat.gateway.entity;

import java.io.Serializable;

/**
 * cl_sys_config
 * @author 
 */
public class ClSysConfig implements Serializable {
    private String browserName;

    private String browserHome;

    private String browserDriver;

    private String browserDownloadPath;

    private static final long serialVersionUID = 1L;

    public String getBrowserName() {
        return browserName;
    }

    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    public String getBrowserHome() {
        return browserHome;
    }

    public void setBrowserHome(String browserHome) {
        this.browserHome = browserHome;
    }

    public String getBrowserDriver() {
        return browserDriver;
    }

    public void setBrowserDriver(String browserDriver) {
        this.browserDriver = browserDriver;
    }

    public String getBrowserDownloadPath() {
        return browserDownloadPath;
    }

    public void setBrowserDownloadPath(String browserDownloadPath) {
        this.browserDownloadPath = browserDownloadPath;
    }
}